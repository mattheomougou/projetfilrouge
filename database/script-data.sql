
insert into Domaine values ('Informatique');
insert into Domaine values ('Management');
insert into Domaine values ('Finances');
insert into Domaine values ('Ressources Humaines');

insert into Theme values ('R�seaux');
insert into Theme values ('Syst�mes d�exploitation');
insert into Theme values ('Gestion de projets');
insert into Theme values ('Langages de d�veloppement');
insert into Theme values ('Langages du WEB');
insert into Theme values ('Bases de Donn�es');
insert into Theme values ('JAVA');
insert into Theme values ('C#');
insert into Theme values ('C++');
insert into Theme values ('JAVA POO');
insert into Theme values ('JAVA AVANCE');
insert into theme values ('PHP');
insert into theme values ('HTML');
insert into theme values ('JAVA SCRIPT');
insert into theme values ('CSS');
insert into theme values('SQL');
insert into theme values('Oracle');
insert into theme values('PostgreSQL');
insert into theme values('MySQL');
insert into theme values('Microsoft Azure');
insert into theme values('SGBDR');
insert into theme values('Big Data');
insert into theme values('NoSQL');
insert into theme values('MongoDB');
insert into theme values('Python');
insert into theme values('Intelligence Artificielle');
insert into theme values('Excel');
insert into theme values('Word');
insert into theme values('PowerPoint');
insert into theme values('Office');
insert into theme values('.Net');
insert into theme values('Linux');
insert into theme values('Unix');
insert into theme values('Windows');
insert into theme values('HTML5');
insert into theme values('CSS3');
insert into theme values('Angular');
insert into theme values('Angular JS');
insert into theme values('ReactJS');
insert into theme values('BootStrap');
insert into theme values('jQuery');
insert into theme values('Node.js');
insert into theme values('PHP 7');
insert into theme values('Symfony7');
insert into theme values('Wordpress');
insert into theme values('SOA');
insert into theme values('UML');
insert into theme values('ESB');
insert into theme values('XML');
insert into theme values('REST');
insert into theme values('JAX-RS');
insert into theme values('Apache');
insert into theme values('Tomcat');
insert into theme values('Deep Learning');
insert into theme values('Cloud computing');

-- select * from theme;
-- select * from theme_lien;
insert into Theme_lien values (1,1);
insert into Theme_lien values (2,2);
insert into Theme_lien values (3,3);
insert into Theme_lien values (4,4);
insert into Theme_lien values (5,5);
insert into Theme_lien values (6,6);
insert into Theme_lien values (4,7);
insert into Theme_lien values (4,8);
insert into Theme_lien values (4,9);
insert into Theme_lien values (7,10);
insert into Theme_lien values (7,11);
insert into Theme_lien values (4,12);
insert into Theme_lien values (5,13);
insert into Theme_lien values (5,14);
insert into Theme_lien values (8,15);
insert into Theme_lien values (6,16);
insert into Theme_lien values (6,17);
insert into Theme_lien values (6,18);
insert into Theme_lien values (6,19);
insert into Theme_lien values (6,20);
insert into Theme_lien values (6,21);
insert into Theme_lien values (55,22);
insert into Theme_lien values (6,23);
insert into Theme_lien values (6,24);
insert into Theme_lien values (4,25);


insert into Module values ('LES AM�LIORATIONS DU LANGAGE',5,'Les annotations r�p�tables.
La r�flexion sur param�tres.
Exercice : cr�er des annotations r�p�tables et les utiliser.',7);
insert into Module values ('LES STREAMS',2,'D�finition et utilisation des streams
Les classes de java.util.stream
Utilisation des streams avec les lambdas expressions
Les traitements parall�les avec les streams
Exercice : r�aliser des traitements avec les streams, en utilisant les lambdas expressions et autres pr�dicats',7);
insert into Module values ('JAVAFX',3,'Le th�me Modena.
Encapsulation de Swing dans JavaFX',7);

insert into Module values ('LES CONCEPTS OBJET',1,'Programmation objet, les r�utilisables
Principe de l''encapsulation
Attributs et m�thodes
Accesseurs
Diff�rence entre objet et classe
Instanciation',7);
insert into Module values ('INTRODUCTION � JAVA',3,'Philosophie de conception sous-jacente � Java
Les diff�rentes �ditions
Pr�sentation JSE, du jdk',7 );
insert into Module values ('SYNTAXE JAVA',1,'Les r�gles d''�critures
Pr�sentation des types primitifs, des objets et des types abstraits
D�claration des variables
Principaux op�rateurs sur les types primitifs
Pr�sentation des r�gles de priorit� entre les op�rateurs
Structures de contr�le : r�gles de d�finition d''une s�quence d''instructions Java',7);
insert into Module values ('ACC�S AUX SGBD',2,'Objectif de JDBC
Les types de drivers
Les architectures applicatives
Les classes et interfaces en jeu
Connexion
La gestion des transactions et l''isolation transactionnelle
Interrogation et mise � jour',7);


insert into Formation values (1,'Formation Java 8 - Les nouveaut�s - Ma�triser les nouvelles fonctionnalit�s', 'Si dans sa version 8, Java apporte de r�elles nouveaut�s (lambdas, Streams, Collectors, JavaFX, Nashorn) qui vont probablement n�cessiter une reprise des applications existantes pour les exploiter, une kyrielle d�am�liorations directement exploitables par les d�veloppeurs Java leur simplifieront � n�en pas douter leur quotidien. En effet, de nombreuses am�liorations apport�es notamment aux APIs Java, aux classes mais aussi au niveau de la syntaxe rend cette derni�re version plus "pratique" dans de multiples domaines tels que la manipulation de dates, de chaines de caract�res ou encore pour les tableaux. Les participants � cette formation de 2 jours passeront en revue toutes les nouvelles possibilit�s offertes pour tirer parti des avanc�es de Java 8.' ,'Conna�tre les �volutions du langage Java
Savoir tirer partir des �volutions et utiliser les expressions Lambda
Ma�triser les nouvelles possibilit�s offertes
Savoir g�rer la s�curit� et comprendre comment optimiser les acc�s r�seaux ','Savoir d�velopper en Java', 50,5000);

insert into Module_Formation values (1,1);
insert into Module_Formation values (1,2);
insert into Module_Formation values (1,3);


insert into Session values
(null,null,1,'inter','Session1','01/01/2022','10/03/2022',5000,'Lieu1','Rennes',35200),
(null,null,1,'inter','Session2','01/03/2022','10/06/2022',5000,'Lieu2','Nantes',44000),
(null,null,1,'inter','Session3','01/05/2022','10/07/2022',5000,'Lieu1','Rennes',35200);




insert into Module values('WEB INTELLIGENCE',2,'Description des concepts de Web Intelligence
',5),

('SAP FIORI BI LAUNCHPAD ET WEB INTELLIGENCE'
,2,'Description et introduction de l�interface SAP Fiori BI Launchpad.
Recherche de contenu.
Interaction avec les documents en mode lecture.
Personnalisation de l�interface SAP Fiori BI Launchpad.
',5),
('LES DOCUMENTS WEB INTELLIGENCE AVEC DES REQU�TES',2,
'Cr�ation de requ�tes. Cr�ation de document Web Intelligence. 
 Modification d�une requ�te dans un document Web Intelligence
',5),
('CONCEPTION D�UN DOCUMENT WEB INTELLIGENCE',2,'
El�ments de l�interface utilisateur de l�application Web Intelligence.
Affichage des donn�es en tableau.
Utilisation des tableaux.
Pr�sentation des donn�es dans des cellules libres.
Pr�sentation des donn�es dans des graphiques',5),
('EXPLORATION DU DOCUMENT WEB INTELLIGENCE
',2,'Construire un document avec des fonctionnalit�s d�exploration (drill-in, drill-up)',5);


insert into Formation values (1,'Formation SAP BusinessObjects Web Intelligence 4.3 - Niveau 1', 
'De plus en plus pris�s des entreprises, les logiciels dot�s d''une interface Web sont dor�navant privil�gi�s par rapport aux solutions traditionnelles. L''utilisation de telles solutions peut parfois d�router les utilisateurs qui n''ont plus leurs rep�res habituels. Il est donc n�cessaire de les accompagner dans le changement des outils. Cette formation permet � n''importe quel utilisateur prenant en main Web Intelligence d''�tre aussi performant qu''avec les outils de pr�c�dentes g�n�rations.' ,
'Pouvoir d�crire les concepts de Web Intelligence
�tre capable d''interagir avec des documents en mode lecture
Savoir r�cup�rer des donn�es en cr�ant des requ�tes � l''aide d''univers
Pouvoir restreindre les donn�es avec des filtres de requ�te
Apprendre � afficher les donn�es dans des tableaux et des graphiques
Comprendre comment am�liorer les documents pour une analyse plus facile','Il est recommand� de conna�tre l''environnement web et un tableur tel qu''Excel', 50,1450);

insert into Module_Formation values (2,8),
(2,9),(2,10),(2,11),(2,12);


insert into Module values('MULTIPLES SOURCES DE DONN�ES
',2,'Synchronisation des donn�es avec plusieurs sources de donn�es
Cr�ation d�un document avec plusieurs requ�tes
Synchronisation des donn�es avec des dimensions fusionn�es
Interaction avec Excel
Utilisation d�un document Web Intelligence comme source de donn�es
',5),
('TECHNIQUES DE REQU�TES AVANC�ES
',2,'Impl�mentation de requ�tes combin�es
Impl�mentation de sous-requ�tes
Cr�ation d�une requ�te bas�e sur une autre requ�te
Changer de sources de donn�es
',5),
('CONTEXTES DE CALCUL
',2,'Description des contextes de calcul
Red�finition des contextes de calcul (Input, output)
Impl�mentation des op�rateurs de contexte In / Where
Impl�mentation de l�op�rateur de contexte ForEach
Impl�mentation de l�op�rateur de contexte ForAll
Impl�mentation des mots cl�s �tendus
',5),

('LOGIQUE IF ET GROUPEMENT DE DONN�ES
',2,'Groupement de donn�es
Ex�cution de la fonction If() pour modifier le comportement de calcul
',5);


insert into Formation values (1,'Formation SAP BusinessObjects Web Intelligence 4.3 - Niveau 2', 
'La solution Web Intelligence propos�e par SAP BusinessObjects permet d''offrir aux utilisateurs un 
acc�s en "libre-service" aux donn�es qui les int�ressent. Mais elle int�gre �galement un certain nombre
de fonctionnalit�s qui peuvent leur �tre particuli�rement utiles pour cr�er des tableaux de bord et des
rapports adapt�s � leur m�tier sp�cifique. Ils le seront d''autant plus que seront ma�tris�s le recours 
� des formules de calcul, la cr�ation de requ�tes complexes et l''utilisation des contextes de calcul. Autant de comp�tences qu''acquerront les participants au cours de cette formation.',
'�tre capable de mettre en oeuvre la bonne m�thodologie et les meilleures pratiques de construction de document
Savoir utiliser des fonctionnalit�s avanc�es lors de la gestion des �l�ments de rapport
�tre en mesure de cr�er des filtres de requ�te �labor�s
Savoir cr�er des calculs complexes en utilisant des fonctions Web Intelligence',
'Avoir suivi la formation "SAP BusinessObjects Web Intelligence 4.3 - Niveau 1" (BO253)', 50,1450);


insert into Module_Formation values (3,13),
(3,14),(3,15),(3,16);


insert into Module values('LE SYST�ME DE GESTION DE BASES DE DONN�ES
',2,'Besoin de normalisation
Mod�le conceptuel : description du quoi ? (finalit� de l�entreprise)
Mod�le logique ou organisationnel : description de qui fait quoi ?
Mod�le physique ou op�rationnel : description de comment faire ?
Les m�thodes de conceptions : m�thodes fonctionnelles, m�thodes orient�es objets, m�thodes syst�miques
',6),
('TYPES DE SGBD',2,'Les SGBD relationnels
Les SGBD objets : architecture fonctionnelle type
Place de XML/XSL
Bilan SGBD et XML',6),
('ADMINISTRATION DES SGBD',2,'Un besoin fondamental : optimiser, pr�voir et anticiper, corriger, s�curiser, mettre � disposition, superviser, ...
La s�curit� des donn�es : confidentialit�, persistance, disponibilit�, sauvegarde et restauration, recomposition, int�grit�, restructuration
R�plication ou r�partition ? D�finitions, avantages et inconv�nients de chaque approche',6);



insert into Formation values (1,'Formation L''essentiel des bases de donn�es', 
'Indispensable � toute entreprise manipulant un nombre important de donn�es, un syst�me de gestion de 
base de donn�es (SGBD) permet de centraliser et s�curiser les donn�es. Qu�il soit issu du monde Open Source ou propos� par un �diteur reconnu, un SGBD est un logiciel qui 
permet de stocker et de manipuler des donn�es sous une forme structur�e. A l''issue de ce s�minaire, 
les participants auront acquis une vision claire de ce qu''est un SGBD et connaitront les principales 
solutions du march�, leurs forces et leurs faiblesses. Ils comprendront �galement l''int�r�t de 
mod�liser correctement une base de donn�es pour garantir l''int�grit� et les performances, d�couvriront
enfin la puissance du langage SQL pour manipuler les donn�es.',
'Disposer d''une vision claire de ce qu''est un SGBD
Comprendre l''int�r�t de mod�liser correctement une base de donn�es pour garantir l''int�grit� et les performances
D�couvrir la puissance du langage SQL pour manipuler les donn�es
Identifier les principaux acteurs du march� ainsi que les forces et faiblesses de leurs solutions','Aucun',50,1370);

insert into Module_Formation values (4,17),
(4,18),(4,19);

insert into Formation values (1,'Formation Introduction aux bases de donn�es SQL Server', 
'En am�liorant les performances de son syst�me de gestion de bases de donn�es et en le rendant encore plus s�r,
Microsoft vient clairement concurrencer Oracle sur ses terres avec SQL Server. Pr�sent�e de mani�re progressive,
claire et synth�tique, cette formation constitue une introduction d�une part aux concepts g�n�raux des bases
de donn�es relationnelles et d�autre part aux sp�cificit�s de SQL Server. Elle s�adresse aussi bien aux
non-sp�cialistes souhaitant se familiariser avec les concepts fondamentaux qu�� ceux qui se destinent � un 
cursus plus pouss�.',
'D�couvrir les concepts cl�s de la base de donn�es dans le contexte de SQL Server
Comprendre les techniques de mod�lisation de donn�es
Comprendre l�incidence de la conception de la base de donn�es sur les performances','Connaissance g�n�rale en informatique
',50,1895);

insert into Module_Formation values (5,17),
(5,18),(5,19);





insert into Module values('SAUVEGARDES ET ARCHIVAGE
',1,'Les diff�rentes m�thodes et outils : sauvegarde SQL, syst�me de fichiers, archivage continu
pgdump : principe, exemple de sauvegarde et restauration des donn�es avec pSQL
pgdumpall : sauvegarde de toutes les bases d''une instance
Archivage continu avec WAL
Principe, configuration de l''archivage WAL
',6),
('HAUTE DISPONIBILIT�',1,'Diff�rentes m�thodes
Principe des serveurs warm et hot standby
Utilisation des flux WAL
Mise en oeuvre du transfert de journaux et de la r�plication en continu (streaming replication)
',6),
('OPTIMISATION',1,'Outils de supervision de l''activit� de la base de donn�es
Configuration des statistiques
Param�tres : track_activities, track_count, track_functions, track_io_timing
Contr�le des verrous : pg_locks
',6);

insert into Formation values (1,'Formation PostgreSQL - Administration avanc�e', 
'PostgreSQL est un syst�me de gestion de bases de donn�es relationnel robuste et puissant, 
aux fonctionnalit�s riches et avanc�es, capable de manipuler en toute fiabilit� de gros volumes de donn�es, 
m�mes dans des situations critiques. On peut �riger PostgreSQL au rang de brique Open Source "incontournable".
PostgreSQL repose sur un mod�le orient� objet, une technologie qui ouvre des horizons beaucoup plus larges qu''un dispositif relationnel classique. A l''issue de la formation, les participants seront capables de mettre en oeuvre les fonctionnalit�s avanc�es de l''administration des bases de donn�es PostgreSQL.',
'Savoir configurer les sauvegardes et l''archivage
Savoir r�pondre aux contraintes de haute disponibilit�
�tre capable de mettre en oeuvre la r�plication
','Avoir suivi la formation "PostgreSQL - Mise en oeuvre et administration" (IXU50) ou avoir les connaissances �quivalentes
',50,1895);

insert into Module_Formation values (6,20),
(6,21),(6,22);



INSERT INTO Session VALUES
(null,null,3,'inter','Session1','01/04/2022','11/04/2022',1450,'P.O. Box 753, 7535 Aliquet. Av.','Bastia','36498'),
(null,null,3,'inter','Session2','15/03/2022','25/03/2022',1450,'Ap #769-1129 Ut, St.','Bastia','76327'),
(null,null,3,'inter','Session3','01/05/2022','10/07/2022',1450,'9406 Adipiscing St.','Brive-la-Gaillarde','10463');

  INSERT INTO Session VALUES
(null,null,4,'inter','Session1','02/04/2022','13/04/2022',1370,'P.O. Box 495, 9408 Ut, Street','Charleville-M�zi�res','46542'),
(null,null,4,'inter','Session2','15/05/2022','25/05/2022',1370,'P.O. Box 943, 1895 Odio. St.','Vannes','81267'),
(null,null,4,'inter','Session3','01/07/2022','11/07/2022',1370,'9406 Adipiscing St.','Brive-la-Gaillarde','10463');

  INSERT INTO Session VALUES
(null,null,5,'inter','Session1','08/04/2022','18/04/2022',1895,'P.O. Box 753, 7535 Aliquet. Av.','Bastia','36498');

   INSERT INTO Session VALUES
(null,null,6,'inter','Session1','08/04/2022','18/04/2022',1895,'Ap #676-3833 At Avenue','Mulhouse','64824'),
(null,null,6,'inter','Session2','15/03/2022','25/03/2022',1895,'Ap #650-3231 Turpis. Road','Talence','59366'),
(null,null,6,'inter','Session3','01/05/2022','10/07/2022',1895,'9406 Adipiscing St.','Brive-la-Gaillarde','10463');


insert into Utilisateur values ('Leyinda','Wylliams','wylliamsleyinda@gmail.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0787693381,'11 avenue rhin et danube','Pau',64000,1,'2021-04-03');
insert into Utilisateur values ('Moreau','Anne','annemoreau@gmail.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0727625381,'1 Quai de Bacalan','Bordeaux',33300,0,'2019-04-21');
insert into Utilisateur values ('Bassi','claudia','claudiabassi@gmail.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0545125963,'12 rue republique', 'Paris',75000,0,'2020-06-14');
insert into Utilisateur values ('BOTZANOVSKI','Thomas','thomasbotzanovski@gmail.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0745125942,'3 rue Henri POINCARE', 'Marseille',13000,0,'2021-04-05');
insert into Utilisateur values ('ANGER','Catherine','catherineanger@gmail.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0745125942,'7 Rue Saint-Denis', 'Nantes',44000,0,'2022-02-20');
insert into Utilisateur values ('TATEOSSIAN','Gaelle','gaelletateossion@it-training.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0745125942,'21 Rue du Chapitre', 'Rennes',35000,0,'2022-07-03');
insert into Utilisateur values ('VECIANI','Eric','ericveciani@it-training.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0625175952,'17 Rue Laplace',  'Paris',75005,0,'2021-08-18');
insert into Utilisateur values ('Charpentier','Alain','alaincharpentier@gmail.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0625175952,'6 Rue Bossuet','Lyon',69006,0,'2019-02-07');
insert into Utilisateur values ('Dumont','Chloe','chloedumont@gmail.com',CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(123456 as varchar)),2),0625175952,'21 Av. Jean Janvier','Rennes',35000,0,'2021-05-14');
--Employes
insert into Employe values ('Responsable Catalogue',5000,0,'2021-04-03',1);
insert into Employe values ('Responsable enregistrement des formations',2500,0,'2019-04-21',2);
insert into Employe values ('Responsable logistique',3000,0,'2020-06-14',3);
insert into Employe values ('Responsable installation des salles',2800,0,'2021-04-05',4);
insert into Employe values ('Responsable planning des intervenants externes',3500,0,'2022-02-20',5);
insert into Employe values ('Responsable des formations inter-entreprises',3500,1,'2022-07-03',6);
insert into Employe values ('Responsable Commercial',4000,0,'2021-08-18',7);

  
update utilisateur
set administrateur =1
where id =1;

update formation set niveau_exige=11 where id in(1,2);
update formation set niveau_exige=10 where id in(3,5);
update formation set niveau_exige=8 where id in(4);
update formation set niveau_exige=12 where id in(6);