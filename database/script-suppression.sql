DECLARE @DatabaseName nvarchar(50)
SET @DatabaseName = N'BDD_PROJET_FIL_ROUGE'

DECLARE @SQL varchar(max)

SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
FROM MASTER..SysProcesses
WHERE DBId = DB_ID(@DatabaseName) AND SPId <> @@SPId

--SELECT @SQL 
EXEC(@SQL)

DROP DATABASE BDD_PROJET_FIL_ROUGE;