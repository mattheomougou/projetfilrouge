CREATE DATABASE BDD_PROJET_FIL_ROUGE;

USE BDD_PROJET_FIL_ROUGE;

CREATE TABLE Utilisateur(
	id int IDENTITY CONSTRAINT PK_Utilisateur PRIMARY KEY,
	nom varchar(250) not null,
	prenom varchar(250) not null,
	mail varchar(250) not null CONSTRAINT CK_mail_utilisateur CHECK (mail LIKE'%@%') CONSTRAINT UN_mail_utilisateur UNIQUE,
	mot_de_passe varchar(250) not null,
	telephone varchar(30) not null,
	adresse varchar(200),
	ville varchar(200),
	code_postal varchar(10),
	administrateur bit not null CONSTRAINT DF_administrateur_utilisateur DEFAULT 0,
	date_inscription date CONSTRAINT DF_date_inscription_utilisateur DEFAULT getDate()

);

CREATE TABLE Formateur(
	id int IDENTITY CONSTRAINT PK_formateur PRIMARY KEY,
	date_embauche date CONSTRAINT DF_date_embauche_formateur DEFAULT getDate(),
	id_utilisateur int not null,
	CONSTRAINT FK_Formateur_U FOREIGN KEY(id_utilisateur) REFERENCES utilisateur(id)

	);


CREATE TABLE Employe(
	id int IDENTITY CONSTRAINT PK_employe PRIMARY KEY,
	fonction varchar(100) not null,
	salaire numeric(8,2) not null,
	responsable_planning bit not null,
	date_embauche date CONSTRAINT DF_date_embauche_employe DEFAULT getDate(),
	id_utilisateur int not null,
	CONSTRAINT FK_Employe_U FOREIGN KEY(id_utilisateur) REFERENCES utilisateur(id)

);
CREATE TABLE Stagiaire(
	id int IDENTITY CONSTRAINT PK_stagiaire PRIMARY KEY,
	entreprise varchar(150),
	id_utilisateur int not null,
	CONSTRAINT FK_Stagiaire_U FOREIGN KEY(id_utilisateur) REFERENCES utilisateur(id),

);



CREATE TABLE Domaine (
	id int IDENTITY CONSTRAINT PK_domaine PRIMARY KEY,
	nom varchar(200) not null,
);

CREATE TABLE Theme (
	id int IDENTITY CONSTRAINT PK_theme PRIMARY KEY,
	nom varchar(200) not null,
);

CREATE TABLE Theme_lien(
	id_parent int not null,
	id_theme int not null,
	CONSTRAINT FK_Theme_TL FOREIGN KEY(id_theme) REFERENCES theme(id),
);


CREATE TABLE Formation (
	id int IDENTITY CONSTRAINT PK_formation PRIMARY KEY,
	id_domaine int not null,
	titre varchar(200) not null,
	description varchar(4000),
	objectifs varchar(4000),
	prerequis varchar(4000),
	niveau_exige numeric(4,2) not null,
	prix numeric(10,2),
	CONSTRAINT FK_Domaine_Formation FOREIGN KEY(id_domaine) REFERENCES domaine(id),
	);


	CREATE TABLE Module(
	id int IDENTITY CONSTRAINT PK_module PRIMARY KEY,
	titre varchar(100) not null,
	duree int not null,
	description varchar(4000)not null,
	id_theme int not null,
	CONSTRAINT FK_Theme_Module FOREIGN KEY(id_theme) REFERENCES theme(id)
);

CREATE TABLE Module_Formation(
	id int IDENTITY CONSTRAINT PK_moduleFormation PRIMARY KEY,
	id_formation int not null,
	id_module int not null,
	CONSTRAINT FK_Formation_MF FOREIGN KEY(id_formation) REFERENCES formation(id),
	CONSTRAINT FK_Module_MF FOREIGN KEY(id_module) REFERENCES module(id),
);

CREATE TABLE Session(
	id int IDENTITY CONSTRAINT PK_session PRIMARY KEY,
	id_responsable int not null,
	id_employe int not null,
	id_formation int not null,
	type_session varchar(200) CONSTRAINT CK_type_session CHECK (type_session LIKE 'intra' OR type_session LIKE 'inter'),
	titre varchar(100),
	date_debut date CONSTRAINT DF_date_debut_session DEFAULT getDate(),
	date_fin date not null,
	prix numeric(8,2) not null,
	adresse varchar(200) not null,
	ville varchar(200) not null,
	code_postal varchar(10) not null,
	CONSTRAINT FK_Employe_Session FOREIGN KEY(id_employe) REFERENCES employe(id),
	CONSTRAINT FK_Formation_Session FOREIGN KEY(id_formation) REFERENCES formation(id),
	CONSTRAINT FK_Responsable_Session FOREIGN KEY(id_responsable) REFERENCES utilisateur(id)	
);

ALTER TABLE Session ALTER COLUMN id_responsable INT NULL;
ALTER TABLE Session ALTER COLUMN id_employe INT NULL;



CREATE TABLE Test(
id int IDENTITY CONSTRAINT PK_test PRIMARY KEY,
note int not null,
date_debut date CONSTRAINT DF_date_creation_test DEFAULT getDate(),
id_formation int not null,
id_utilisateur int not null,
CONSTRAINT FK_formation_Test FOREIGN KEY(id_formation) REFERENCES formation(id),
CONSTRAINT FK_Test_Utilisateur  FOREIGN KEY(id_utilisateur)  REFERENCES Utilisateur(id)
);


CREATE TABLE ModuleSession(
	id int IDENTITY CONSTRAINT PK_MS PRIMARY KEY,
	titre varchar(100),
	id_formateur int not null,
	date_debut date CONSTRAINT DF_date_debut_module_session DEFAULT getDate(),
	date_fin date not null,
	adresse varchar(200) not null,
	ville varchar(200) not null,
	code_postal varchar(10) not null,
	CONSTRAINT FK_formateur_MS FOREIGN KEY(id_formateur) REFERENCES formateur(id),
);

CREATE TABLE Evaluation(
    id_Evaluation int IDENTITY CONSTRAINT PK_evaluation PRIMARY KEY,
    id_session int,
    id_stagiaire int,
    note_accueil int CONSTRAINT CK_note_accueil CHECK(note_accueil<=5 AND note_accueil >0),
    note_contenu int CONSTRAINT CK_note_contenu CHECK(note_contenu<=5 AND note_contenu >0),
    note_disponibilite int CONSTRAINT CK_note_disponibilite CHECK(note_disponibilite<=5 AND note_disponibilite >0),
    note_maitrise_domaine int CONSTRAINT CK_note_maitrise_domaine CHECK(note_maitrise_domaine<=5 AND note_maitrise_domaine >0), 
    note_pedagogie int CONSTRAINT CK_note_pedagogie CHECK(note_pedagogie<=5 AND note_pedagogie >0),
    note_reponse_aux_questions int CONSTRAINT CK_note_repose_aux_questions CHECK(note_reponse_aux_questions<=5 AND note_reponse_aux_questions >0),
    note_technique_animation int CONSTRAINT CK_note_technique_animation CHECK(note_technique_animation<=5 AND note_technique_animation >0),
    note_qualite_environnement int CONSTRAINT CK_note_qualite_environnement CHECK(note_qualite_environnement<=5 AND note_qualite_environnement >0),
    note_formateur int CONSTRAINT CK_note_formateur CHECK(note_formateur<=5 AND note_formateur >0),
    satisfaction_stagiaire int,
    recommandation_stagiaire bit,
    projet_formation bit,
    commentaires_projet_formation varchar(2000),
    CONSTRAINT FK_session_evaluation FOREIGN KEY(id_session) REFERENCES modulesession(id),
    CONSTRAINT FK_stagiaire_evaluation FOREIGN KEY(id_stagiaire) REFERENCES stagiaire(id),

);


CREATE TABLE Stagiaire_Session(
id int IDENTITY CONSTRAINT PK_stagiaire_session PRIMARY KEY,
id_session int not null,
id_stagiaire int not null,
CONSTRAINT FK_Session_SS FOREIGN KEY(id_session) REFERENCES Session(id),
CONSTRAINT FK_stagiaire_SS FOREIGN KEY(id_stagiaire) REFERENCES stagiaire(id)
);


CREATE TABLE Contact(
	id_contact int IDENTITY CONSTRAINT PK_contact PRIMARY KEY,

	objet varchar(1000) not null,
	civilite varchar(250) not null,
	nom varchar(250) not null,
	prenom varchar(250) not null,
	telephone varchar(250) not null,
	email varchar(250) not null CONSTRAINT CK_email_utilisateur CHECK (email LIKE'%@%') CONSTRAINT UN_email_contact UNIQUE,
	fonction varchar(250),
	societe varchar(250),
	adresse varchar(250) not null,
	code_postal varchar(250) not null,
	ville varchar(250) not null,
	message varchar(4000) not null,
	reponse varchar(250) not null  
);