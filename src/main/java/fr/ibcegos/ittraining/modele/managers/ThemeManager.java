package fr.ibcegos.ittraining.modele.managers;

import java.util.List;

import fr.ibcegos.ittraining.modele.dao.ThemeDAO;
import fr.ibcegos.ittraining.modele.objets.Theme;

public class ThemeManager {

	public Theme getThemeById(int id) {
		ThemeDAO themeDAO = new ThemeDAO();
		Theme theme = themeDAO.findThemeById(id);
		return theme;
	}
	
	public Theme findThemeByName(String themeName) {
		ThemeDAO themeDAO = new ThemeDAO();
		Theme theme = themeDAO.findThemeByName(themeName);
		return theme;
	}

	public List<Theme> findAllThemes() {
		ThemeDAO themeDAO = new ThemeDAO();
		List<Theme> themes = themeDAO.findAllThemes();
		return themes;
	}


	public void enregistrer(Theme theme) {
		ThemeDAO themeDAO = new ThemeDAO();
		themeDAO.enregistrer(theme);

	}

}
