package fr.ibcegos.ittraining.modele.managers;

import java.util.List;

import fr.ibcegos.ittraining.modele.dao.DomaineDAO;
import fr.ibcegos.ittraining.modele.objets.Domaine;

public class DomaineManager {


	public Domaine findDomaineById(int domaineId) {
		DomaineDAO domaineDAO = new DomaineDAO();
		Domaine domaine = domaineDAO.findDomaineById(domaineId);
		return domaine;
	}
	
	public Domaine findDomaineByName(String domaineName) {
		DomaineDAO domaineDAO = new DomaineDAO();
		Domaine domaine = domaineDAO.findDomaineByName(domaineName);
		return domaine;
	}

	public List<Domaine> findAllDomaine() {
		DomaineDAO domaineDAO = new DomaineDAO();
		List<Domaine> domaines = domaineDAO.findAllDomaine();
		return domaines;
	}

	

	public void enregistrer(Domaine domaine) {
		DomaineDAO domaineDAO = new DomaineDAO();
		domaineDAO.enregistrer(domaine);

	}

}
