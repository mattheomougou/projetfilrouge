package fr.ibcegos.ittraining.modele.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.ittraining.modele.dao.EmployeDAO;
import fr.ibcegos.ittraining.modele.dao.UtilisateurDAO;
import fr.ibcegos.ittraining.modele.objets.Employe;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class EmployeManager {
	
	// cl� : champs test� - valeur : message
	Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verifEmploye( double salaire) {
		verifSalaire(salaire);
		return erreurs;
	}
	
	
	private void verifSalaire(double salaire) {
		if(salaire <= 1300) { 
			erreurs.put("salaire", "Le salaire de l'employ� doit �tre sup�rieur � 1300");
			return;
		}
		
	}
	

	public void enregistrer(Employe employe) {
		UtilisateurDAO utilisateurDAO =new UtilisateurDAO();
		utilisateurDAO.enregistrer(new Utilisateur(employe.getNom(), employe.getPrenom(), employe.getMail(), employe.getMotDePasse(),
				employe.getTelephone(), employe.getAdresse(), employe.getVille(), employe.getCodePostal()));
		Utilisateur utilisateur =utilisateurDAO.findByMail(employe.getMail());
		EmployeDAO employeDAO = new EmployeDAO();
		employeDAO.enregistrer(employe,utilisateur.getId());
	}

	public Employe findByID(int id) {
		EmployeDAO employeDAO = new EmployeDAO();
		return employeDAO.findByID(id);
		
	}

	public Object findByMail(String mail) {
		EmployeDAO employeDAO = new EmployeDAO();
		return employeDAO.findByMail(mail);
	}

	public void supprimerByID(int id) {
		EmployeDAO employeDAO = new EmployeDAO();
		employeDAO.supprimerByID(id);
		
	}

	public void supprimerByMail(String mail) {
		EmployeDAO employeDAO = new EmployeDAO();
		employeDAO.supprimerByMail(mail);
		
	}

	public List<Employe> findAll() {
		EmployeDAO employeDAO = new EmployeDAO();	
		return employeDAO.findAll();
	}


	public int getNombreEmploye() {
		EmployeDAO employeDAO = new EmployeDAO();	
		return employeDAO.getNombreEmploye();
	}

	
	
	
}
