package fr.ibcegos.ittraining.modele.managers;

import java.util.List;

import fr.ibcegos.ittraining.modele.dao.SalleDAO;
import fr.ibcegos.ittraining.modele.dao.ThemeDAO;
import fr.ibcegos.ittraining.modele.objets.Salle;
import fr.ibcegos.ittraining.modele.objets.Theme;

public class SalleManager {

	public Salle getSalle(int id) {
		SalleDAO salleDAO = new SalleDAO();
		Salle salle = salleDAO.findSalle(id);
		return salle;
	}
	
	public Salle findSalleByName(String nom) {
		SalleDAO salleDAO = new SalleDAO();
		Salle salle = salleDAO.findSalleByName(nom);
		return salle;
	}

	public List<Salle> findAll() {
		SalleDAO salleDAO = new SalleDAO();
		List<Salle> salles = salleDAO.findAll();
		return salles;
	}

}
