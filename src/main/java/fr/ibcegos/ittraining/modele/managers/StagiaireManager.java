package fr.ibcegos.ittraining.modele.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.ittraining.modele.dao.FormateurDAO;
import fr.ibcegos.ittraining.modele.dao.StagiaireDAO;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;

public class StagiaireManager {

	public void inscrireStagiaireAUneSession(int idUser, int idSession, String entreprise) {
		StagiaireDAO stagiaireDAO = new StagiaireDAO();
		Stagiaire stagiaire = stagiaireDAO.findByUserID(idUser);
		if (stagiaire == null) {
			stagiaire = new Stagiaire();
			stagiaire.setEntreprise(entreprise);
			stagiaire = stagiaireDAO.enregistrer(stagiaire, idUser);
		}
		if (stagiaire != null) {
			stagiaireDAO.inscrireASession(stagiaire.getId(), idSession);
		}
	}

	public void enregistrerAdmin(Stagiaire stagiaire, int idUser) {
		StagiaireDAO stagiaireDAO = new StagiaireDAO();
		stagiaireDAO.enregistrer(stagiaire, idUser);
	}

	public List<Stagiaire> findAll() {
		StagiaireDAO stagiaireDAO = new StagiaireDAO();
		return stagiaireDAO.findAll();
	}

	public void supprimerByID(int id) {
		StagiaireDAO stagiaireDAO = new StagiaireDAO();
		stagiaireDAO.supprimerByID(id);

	}

	public Stagiaire findByID(int id) {
		StagiaireDAO stagiaireDAO = new StagiaireDAO();
		return stagiaireDAO.findByID(id);
	}

	public void modifier(Stagiaire stagiaire, int idUser) {
		StagiaireDAO stagiaireDAO = new StagiaireDAO();
		stagiaireDAO.modifier(stagiaire, idUser);
		
	}

	public int getNombreStagiaire() {
		StagiaireDAO stagiaireDAO = new StagiaireDAO();
		return stagiaireDAO.getNombreStagiaire();
	}

}
