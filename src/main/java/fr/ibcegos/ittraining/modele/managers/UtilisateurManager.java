package fr.ibcegos.ittraining.modele.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.ittraining.modele.dao.UtilisateurDAO;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class UtilisateurManager {
	
	// cl� : champs test� - valeur : message
	Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verif(String nom, String prenom,String mail,String motDePasse, String confirmMotDePasse,String telephone) {
		verifNom(nom);
		verifPrenom(prenom);
		verifMail(mail);
		verifMotDePasse(motDePasse, confirmMotDePasse);
		verifTelephone(telephone);
		return erreurs;
	}
	
	public Map<String,String> verifier(String nom, String prenom,String telephone) {
		verifNom(nom);
		verifPrenom(prenom);
		verifTelephone(telephone);
		return erreurs;
	}
	
	public void verifNom(String nom) {
		if(nom.length() <= 2) { 
			erreurs.put("nom", "Le nom doit faire au moins 3 caract�res");
		}
	}
	
	public void verifPrenom(String prenom) {
		if(prenom.length() <= 2) { 
			erreurs.put("prenom", "Le pr�nom doit faire au moins 3 caract�res");
		}
	}
	
	private void verifTelephone(String telephone) {
		if(telephone.length() < 10 || telephone.length()>10 ) { 
			erreurs.put("telephone", "Le num�ro de t�l�phone doit faire 10 caract�res");
			return;
		}
		if(!telephone.matches("[+-]?\\d*(\\.\\d+)?")) {
			erreurs.put("telephone", "Le num�ro de t�l�phone doit contenir uniquement des chiffres");
		}
		
	}
	
	public void verifMail(String mail) {
		if(!(mail.trim().endsWith(".fr") || mail.trim().endsWith(".com") || mail.trim().endsWith(".bzh"))) {
			erreurs.put("mail", "L'email doit terminer par .bzh, .com ou .fr");
			return;
		}
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		Utilisateur utilisateur = utilisateurDAO.findByMail(mail);
		if(utilisateur!= null) {
			erreurs.put("mail", "Le mail est d�j� utilis�");
		}
	}
	
	public void verifMotDePasse(String motDePasse, String confirmMotDePasse){
		if(!(motDePasse.length() > 3)) {
			erreurs.put("motDePasse", "Les mot de passe doit faire au moins 4 caract�res");
		}
		if(!motDePasse.equals(confirmMotDePasse)) {
			erreurs.put("confirmMotDePasse", "Les mots de passes ne sont pas identiques");
		}
	}

	public Utilisateur enregistrer(Utilisateur utilisateur) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		return utilisateurDAO.enregistrer(utilisateur);
	}

	public Utilisateur findByMailAndMotDePasse(String mail, String motDePasse) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		Utilisateur utilisateur = utilisateurDAO.findByMailAndMotDePasse(mail, motDePasse);
		return utilisateur;
	}



	public Utilisateur modifier(Utilisateur utilisateur) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		return utilisateurDAO.modifier(utilisateur);
	}




	
	
}
