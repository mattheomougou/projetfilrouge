package fr.ibcegos.ittraining.modele.managers;

import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.dao.ModuleDAO;
import fr.ibcegos.ittraining.modele.dao.ModuleSessionDAO;
import fr.ibcegos.ittraining.modele.objets.Module;
import fr.ibcegos.ittraining.modele.objets.ModuleSession;

public class ModuleSessionManager {

	public List<ModuleSession> getModulesSession(List<Integer> ids) {
		List<ModuleSession> modulesSession = new ArrayList<ModuleSession>();
		for (int id : ids) {
			ModuleSession moduleSession = this.getModuleSession(id);
			if (moduleSession != null) {
				modulesSession.add(moduleSession);
			}
		}

		return modulesSession;
	}

	public ModuleSession getModuleSession(int id) {
		ModuleSessionDAO moduleSessionDAO = new ModuleSessionDAO();
		return moduleSessionDAO.getModuleSession(id);
	}

	public void enregistrer(ModuleSession moduleSession) {
		ModuleSessionDAO moduleSessionDAO = new ModuleSessionDAO();
		moduleSessionDAO.enregistrer(moduleSession);

	}

	public List<ModuleSession> findAllModulesSession(int sessionId) {
		ModuleSessionDAO moduleSessionDAO = new ModuleSessionDAO();
		List<ModuleSession> modules = moduleSessionDAO.findAllModulesSession(sessionId);
		return modules;
	}

}
