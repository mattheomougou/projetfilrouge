package fr.ibcegos.ittraining.modele.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.ittraining.modele.dao.FormateurDAO;
import fr.ibcegos.ittraining.modele.dao.UtilisateurDAO;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class FormateurManager {

	// cl� : champs test� - valeur : message
	Map<String, String> erreurs = new HashMap<>();

	public void enregistrer(Formateur formateur) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		utilisateurDAO.enregistrer(new Utilisateur(formateur.getNom(), formateur.getPrenom(), formateur.getMail(),
				formateur.getMotDePasse(), formateur.getTelephone(), formateur.getAdresse(), formateur.getVille(),
				formateur.getCodePostal()));
		Utilisateur utilisateur = utilisateurDAO.findByMail(formateur.getMail());
		FormateurDAO formateurDAO = new FormateurDAO();
		formateurDAO.enregistrer(formateur, utilisateur.getId());
	}

	public List<Formateur> findAll() {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.findAll();
	}

	public void supprimerByID(int id) {
		FormateurDAO formateurDAO = new FormateurDAO();
		formateurDAO.supprimerByID(id);

	}

	public Formateur findByID(int id) {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.findByID(id);
	}

	public int getNombreFormateur() {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.getNombreFormateur();
	}

}
