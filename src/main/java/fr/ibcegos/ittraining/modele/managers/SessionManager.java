package fr.ibcegos.ittraining.modele.managers;

import java.util.List;
import java.util.Map;

import fr.ibcegos.ittraining.modele.dao.SessionDAO;
import fr.ibcegos.ittraining.modele.objets.InfoSessionInscrit;
import fr.ibcegos.ittraining.modele.objets.Session;

public class SessionManager {

	public Session findSessionById(int sessionId) {
		SessionDAO sessionDAO = new SessionDAO();
		Session session = sessionDAO.getSession(sessionId);
		return session;
	}

	public List<Session> findAllSession() {
		SessionDAO sessionDAO = new SessionDAO();
		List<Session> sessions = sessionDAO.findAllSessions();
		return sessions;
	}

	public void enregistrer(Session session) {
		SessionDAO sessionDAO = new SessionDAO();
		sessionDAO.enregistrer(session);
	}


	public Map<Integer, Integer> findAllInscrit() {
		SessionDAO sessionDAO = new SessionDAO();
		return sessionDAO.findAllInscrit();
	}

	public List<InfoSessionInscrit> findAllSessionOfStagiaire(int id) {
		SessionDAO sessionDAO = new SessionDAO();
		return sessionDAO.findAllSessionOfStagiaire(id);
	}

}
