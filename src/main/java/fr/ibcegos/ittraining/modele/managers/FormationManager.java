package fr.ibcegos.ittraining.modele.managers;

import java.util.List;

import fr.ibcegos.ittraining.modele.dao.FormateurDAO;
import fr.ibcegos.ittraining.modele.dao.FormationDAO;
import fr.ibcegos.ittraining.modele.objets.Formation;
import fr.ibcegos.ittraining.modele.objets.Module;

public class FormationManager {

	public void enregistrer(Formation formation) {
		FormationDAO formationDAO = new FormationDAO();
		formationDAO.enregistrer(formation);

	}

	public List<Formation> findAll() {
		FormationDAO formationDAO = new FormationDAO();
		List<Formation> formations = formationDAO.findAllFormations();
		return formations;
	}

	public Formation findById(int formationId) {
		FormationDAO formationDAO = new FormationDAO();
		Formation formation = formationDAO.findById(formationId);
		return formation;
	}

	
	public List<Formation> searchFormations(String motCle){
		FormationDAO formationDAO = new FormationDAO();
		List<Formation> formations = formationDAO.searchFormations(motCle);
		return formations;
	}
	
	public int getNiveauExige(int formationId) {
		FormationDAO formationDAO = new FormationDAO();
		return formationDAO.getNiveauExige(formationId);
	}
	
	public void supprimerByID(int id) {
		FormationDAO dao = new FormationDAO();
		dao.supprimerByID(id);
		
	}


	public int calculerDureeFormation(int id) {
		FormationDAO formationDAO = new FormationDAO();
		int dureeFormation = formationDAO.CalculerDureeFormation(id);
		return dureeFormation;
	}

	public int getNombreFormation() {
		FormationDAO formationDAO = new FormationDAO();
		return formationDAO.getNombreFormation();
	}
}
