package fr.ibcegos.ittraining.modele.managers;

import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.dao.ModuleDAO;
import fr.ibcegos.ittraining.modele.objets.Module;

public class ModuleManager {

	public List<Module> getModules(List<Integer> ids) {
		List<Module> modules = new ArrayList<Module>();
		for (int id : ids) {
			Module module = this.getModule(id);
			if (module != null) {
				modules.add(module);
			}
		}

		return modules;
	}

	public Module getModule(int id) {
		ModuleDAO moduleDAO = new ModuleDAO();
		return moduleDAO.getModule(id);
	}

	public List<Module> findModulesByNames(String[] selectedModulesNames) {
		ModuleDAO moduleDAO = new ModuleDAO();
		List<Module> modules = moduleDAO.findModulesByNames(selectedModulesNames);
		return modules;
	}

	public void enregistrer(Module module) {
		ModuleDAO moduleDAO = new ModuleDAO();
		moduleDAO.enregistrer(module);

	}

	public List<Module> findAllModules() {
		ModuleDAO moduleDAO = new ModuleDAO();
		List<Module> modules = moduleDAO.findModules();
		return modules;
	}

}
