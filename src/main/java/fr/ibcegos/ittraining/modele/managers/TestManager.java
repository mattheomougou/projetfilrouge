package fr.ibcegos.ittraining.modele.managers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import fr.ibcegos.ittraining.modele.dao.TestDAO;
import fr.ibcegos.ittraining.modele.objets.TestInscription;
import jakarta.servlet.http.HttpServletRequest;

public class TestManager {
	
		Map<Integer,String> resultatsAttendu = new HashMap<>();
		
		public TestManager() {
			resultatsAttendu.put(1, "C");
			resultatsAttendu.put(2, "B");
			resultatsAttendu.put(3, "B");
			resultatsAttendu.put(4, "A");
			resultatsAttendu.put(5, "B");
			resultatsAttendu.put(6, "B");
			resultatsAttendu.put(7, "C");
			resultatsAttendu.put(8, "C");
			resultatsAttendu.put(9, "A");
			resultatsAttendu.put(10, "B");
			resultatsAttendu.put(11, "C");
			resultatsAttendu.put(12, "B");
			resultatsAttendu.put(13, "A");
			resultatsAttendu.put(14, "C");
			resultatsAttendu.put(15, "A");
			resultatsAttendu.put(16, "A");
			resultatsAttendu.put(17, "B");
			resultatsAttendu.put(18, "C");
			resultatsAttendu.put(19, "A");
			resultatsAttendu.put(20, "B");
			
		}
		
		public int getNoteStagiaire(Map<Integer,String> answerStagiaire) {
			int note=0;
			for (int i=1;i<=resultatsAttendu.size();i++) {
				if(resultatsAttendu.get(i).equals(answerStagiaire.get(i))) {
					note++;
				}	
			}
			return note;
		}

		public void enregistrer(TestInscription test) {
			TestDAO testDAO = new TestDAO();
			testDAO.enregistrer(test);
		}

		public TestInscription findTestByUtilisateurAndFormation(int idUtilisateur, int idFormateur) {
			TestDAO testDAO = new TestDAO();
			return testDAO.findTestByUtilisateurAndFormation(idUtilisateur,idFormateur);
			
		}
	
		public void verifNote(HttpServletRequest request, int noteStagiaire, int niveauExige, LocalDate dateTest) {
			if(noteStagiaire>=niveauExige) {
				request.getSession().setAttribute("test", "testValide");
			}else {
				DateTimeFormatter fmt2 = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
				if(dateTest.plusDays(60).isAfter(LocalDate.now())) {
					request.getSession().setAttribute("test", "testRate");
					request.getSession().setAttribute("dateDuTest", dateTest.format(fmt2));
				}else {
					request.getSession().setAttribute("test", "testNonPasse");
				}
			}
		}
	
	

	
	}

	
	


	

