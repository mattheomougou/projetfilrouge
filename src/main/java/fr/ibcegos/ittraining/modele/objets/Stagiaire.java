package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;

public class Stagiaire extends Utilisateur {

	private int id;
	private String entreprise;

	
	public Stagiaire(String nom, String prenom, String mail, String motDePasse, String telephone, String adresse,
			String ville, String codePostal) {
		super(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
	}

	public Stagiaire(String nom, String prenom, String mail, String motDePasse, String telephone, String adresse,
			String ville, String codePostal, String entreprise) {
		super(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
		this.entreprise = entreprise;
	}

	public Stagiaire(String nom, String mail, String motDePasse) {
		super(nom, mail, motDePasse);

	}

	public Stagiaire() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(String entreprise) {
		this.entreprise = entreprise;
	}

	@Override
	public String toString() {
		return "Stagiaire [getId()=" + getId() + ", getEntreprise()=" + getEntreprise() + "]";
	}

}
