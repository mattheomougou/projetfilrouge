package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;

public class TestInscription {
	private int id; // car on enregistre un utilisateur en BDD
	private int note;
	private LocalDate dateDePassageDuTest;
	private int idFormation;
	private int idUtilisateur;
	
	public TestInscription() {
	}

	public TestInscription(int note, int idFormation, int idUtilisateur) {
		super();
		this.note = note;
		this.dateDePassageDuTest=LocalDate.now();
		this.idFormation = idFormation;
		this.idUtilisateur = idUtilisateur;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public LocalDate getDateDePassageDuTest() {
		return dateDePassageDuTest;
	}

	public void setDateDePassageDuTest(LocalDate dateDePassageDuTest) {
		this.dateDePassageDuTest = dateDePassageDuTest;
	}

	public int getIdFormation() {
		return idFormation;
	}

	public void setIdFormation(int idFormation) {
		this.idFormation = idFormation;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	@Override
	public String toString() {
		return "TestInscription [id=" + id + ", note=" + note + ", dateDePassageDuTest=" + dateDePassageDuTest
				+ ", idFormation=" + idFormation + ", idUtilisateur=" + idUtilisateur + "]";
	}
	
	
	
	
	

}
