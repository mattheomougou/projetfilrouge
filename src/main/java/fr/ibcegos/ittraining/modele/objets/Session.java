package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;
import java.util.List;

public class Session {
	private int id;
	private String titre;
	private String type;
	private Double prix;
	private LocalDate dateDebut;
	private LocalDate dateFin;
	private String adresse;
	private String ville;
	private String codePostal;
	private Formation formation;
	private List<ModuleSession> modulesSession;

	public Session() {
	}

	public Session(String titre, String type, Double prix, LocalDate dateDebut, LocalDate dateFin, String adresse,
			String ville, String codePostal, Formation formation) {
		super();
		this.titre = titre;
		this.type = type;
		this.prix = prix;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.adresse = adresse;
		this.ville = ville;
		this.codePostal = codePostal;
		this.formation = formation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	
	public boolean isStarted() {
		LocalDate today = LocalDate.now();
		return today.isAfter(dateDebut) ;
	}

	@Override
	public String toString() {
		return "Session [getId()=" + getId() + ", getTitre()=" + getTitre() + ", getType()=" + getType()
				+ ", getPrix()=" + getPrix() + ", getDateDebut()=" + getDateDebut() + ", getDateFin()=" + getDateFin()
				+ ", getAdresse()=" + getAdresse() + ", getVille()=" + getVille() + ", getCodePostal()="
				+ getCodePostal() + "]";
	}

}
