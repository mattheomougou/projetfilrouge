package fr.ibcegos.ittraining.modele.objets;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Formation {
	private int id;
	private String titre;
	private int niveauExige;
	private Domaine domaine;
	private String description;
	private String prerequis;
	private String objectifs;
	private List<Module> modules;
	private List<Session> sessions = new ArrayList();
	private double prix;
	private Utilisateur responsableFormation;

	public Formation() {
	}

	public Formation(String titre, int niveauExige, Domaine domaine, String description, String prerequis,
			String objectifs, List<Module> modules, double prix, Utilisateur responsableFormation) {
		super();
		this.titre = titre;
		this.niveauExige = niveauExige;
		this.domaine = domaine;
		this.description = description;
		this.prerequis = prerequis;
		this.objectifs = objectifs;
		this.modules = modules;
		this.prix = prix;
		this.responsableFormation = responsableFormation;
	}

	public Formation(String titre, int niveauExige, Domaine domaine, String description, String prerequis,
			String objectifs, List<Module> modules, List<Session> sessions, double prix,
			Utilisateur responsableFormation) {
		super();
		this.titre = titre;
		this.niveauExige = niveauExige;
		this.domaine = domaine;
		this.description = description;
		this.prerequis = prerequis;
		this.objectifs = objectifs;
		this.modules = modules;
		this.sessions = sessions;
		this.prix = prix;
		this.responsableFormation = responsableFormation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getNiveauExige() {
		return niveauExige;
	}

	public void setNiveauExige(int niveauExige) {
		this.niveauExige = niveauExige;
	}

	public Domaine getDomaine() {
		return domaine;
	}

	public void setDomaine(Domaine domaine) {
		this.domaine = domaine;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrerequis() {
		return prerequis;
	}

	public void setPrerequis(String prerequis) {
		this.prerequis = prerequis;
	}

	public String getObjectifs() {
		return objectifs;
	}

	public void setObjectifs(String objectifs) {
		this.objectifs = objectifs;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Session> getSessions() {
		return sessions;
	}

	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public Utilisateur getResponsableFormation() {
		return responsableFormation;
	}

	public void setResponsableFormation(Utilisateur responsableFormation) {
		this.responsableFormation = responsableFormation;
	}

	public List<Session> getProchainesSessions() {
		return this.sessions.stream().filter(s -> !s.isStarted()).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return "Formation [getId()=" + getId() + ", getTitre()=" + getTitre() + ", getNiveauExige()=" + getNiveauExige()
				+ ", getDomaine()=" + getDomaine() + ", getDescription()=" + getDescription() + ", getPrerequis()="
				+ getPrerequis() + ", getObjectifs()=" + getObjectifs() + ", getModules()=" + getModules()
				+ ", getSessions()=" + getSessions() + ", getPrix()=" + getPrix() + ", getResponsableFormation()="
				+ getResponsableFormation() + "]";
	}

}
