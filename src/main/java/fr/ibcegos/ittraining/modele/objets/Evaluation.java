package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;


public class Evaluation {
	private int id_Evaluation;
	private int id_session;
	private int id_stagiaire;
	private boolean recommandation; 
	private int accueil;		 
	private int environnement; 
	private int contenu; 
	private int satisfaction; 
	private int formateur; 
	private int disponibilité; 
	private int maîtriseDomaine; 
	private int pedagogie; 
	private int reponsesQuestion; 
	private int animation; 
	private boolean isProjetFormation;
	private String commentaires_projet_formation;
	
	public Evaluation() {
		// TODO Auto-generated constructor stub
	}

	public Evaluation(boolean recommandation, int accueil, int environnement, int contenu, int satisfaction,
			int formateur, int disponibilité, int maîtriseDomaine, int pedagogie, int reponsesQuestion, int animation,
			boolean isProjetFormation, String commentaires_projet_formation) {
		super();
		this.recommandation = recommandation;
		this.accueil = accueil;
		this.environnement = environnement;
		this.contenu = contenu;
		this.satisfaction = satisfaction;
		this.formateur = formateur;
		this.disponibilité = disponibilité;
		this.maîtriseDomaine = maîtriseDomaine;
		this.pedagogie = pedagogie;
		this.reponsesQuestion = reponsesQuestion;
		this.animation = animation;
		this.isProjetFormation = isProjetFormation;
		this.commentaires_projet_formation = commentaires_projet_formation;
	}

	public int getId_Evaluation() {
		return id_Evaluation;
	}

	public void setId_Evaluation(int id_Evaluation) {
		this.id_Evaluation = id_Evaluation;
	}

	public int getId_session() {
		return id_session;
	}

	public void setId_session(int id_session) {
		this.id_session = id_session;
	}

	public int getId_stagiaire() {
		return id_stagiaire;
	}

	public void setId_stagiaire(int id_stagiaire) {
		this.id_stagiaire = id_stagiaire;
	}

	public boolean isRecommandation() {
		return recommandation;
	}

	public void setRecommandation(boolean recommandation) {
		this.recommandation = recommandation;
	}

	public int getAccueil() {
		return accueil;
	}

	public void setAccueil(int accueil) {
		this.accueil = accueil;
	}

	public int getEnvironnement() {
		return environnement;
	}

	public void setEnvironnement(int environnement) {
		this.environnement = environnement;
	}

	public int getContenu() {
		return contenu;
	}

	public void setContenu(int contenu) {
		this.contenu = contenu;
	}

	public int getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(int satisfaction) {
		this.satisfaction = satisfaction;
	}

	public int getFormateur() {
		return formateur;
	}

	public void setFormateur(int formateur) {
		this.formateur = formateur;
	}

	public int getDisponibilité() {
		return disponibilité;
	}

	public void setDisponibilité(int disponibilité) {
		this.disponibilité = disponibilité;
	}

	public int getMaîtriseDomaine() {
		return maîtriseDomaine;
	}

	public void setMaîtriseDomaine(int maîtriseDomaine) {
		this.maîtriseDomaine = maîtriseDomaine;
	}

	public int getPedagogie() {
		return pedagogie;
	}

	public void setPedagogie(int pedagogie) {
		this.pedagogie = pedagogie;
	}

	public int getReponsesQuestion() {
		return reponsesQuestion;
	}

	public void setReponsesQuestion(int reponsesQuestion) {
		this.reponsesQuestion = reponsesQuestion;
	}

	public int getAnimation() {
		return animation;
	}

	public void setAnimation(int animation) {
		this.animation = animation;
	}

	public boolean isProjetFormation() {
		return isProjetFormation;
	}

	public void setProjetFormation(boolean isProjetFormation) {
		this.isProjetFormation = isProjetFormation;
	}

	public String getCommentaires_projet_formation() {
		return commentaires_projet_formation;
	}

	public void setCommentaires_projet_formation(String commentaires_projet_formation) {
		this.commentaires_projet_formation = commentaires_projet_formation;
	}

	@Override
	public String toString() {
		return "Evaluation [id_Evaluation=" + id_Evaluation + ", id_session=" + id_session + ", id_stagiaire="
				+ id_stagiaire + ", recommandation=" + recommandation + ", accueil=" + accueil + ", environnement="
				+ environnement + ", contenu=" + contenu + ", satisfaction=" + satisfaction + ", formateur=" + formateur
				+ ", disponibilité=" + disponibilité + ", maîtriseDomaine=" + maîtriseDomaine + ", pedagogie="
				+ pedagogie + ", reponsesQuestion=" + reponsesQuestion + ", animation=" + animation
				+ ", isProjetFormation=" + isProjetFormation + ", commentaires_projet_formation="
				+ commentaires_projet_formation + "]";
	}
	

	
	}
	

