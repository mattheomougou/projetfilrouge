package fr.ibcegos.ittraining.modele.objets;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Panier {

	private List<Session> sessions = new ArrayList<Session>();

	public List<Session> getSessions() {
		return sessions;
	}

	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}

	public void ajouterSession(Session session) {
		boolean exist = false;
		for (Session s : sessions) {
			if (session != null && s != null && Objects.equals(s.getId(), session.getId())) {
				exist = true;
				break;
			}
		}
		if (!exist) {
			this.sessions.add(session);
		}
	}

	public void supprimerSession(int sessionId) {

		sessions.removeIf(s -> s != null && Objects.equals(s.getId(), sessionId));

	}

	public double getTotal() {
		double prix = 0;
		for (Session session : sessions) {
			if (session.getFormation() != null) {
				prix = prix + session.getFormation().getPrix();
			}
		}
		return prix;
	}
}
