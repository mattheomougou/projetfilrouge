package fr.ibcegos.ittraining.modele.objets;

/**
 * @author ib
 *
 */
public class Module {
    private int id;
    private String titre;
    private Theme theme;
    private int dureeParJour;
    private String description;
    
	public Module() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Module(String titre, Theme theme, int dureeParJour, String description) {
		super();
		this.titre = titre;
		this.theme = theme;
		this.dureeParJour = dureeParJour;
		this.description = description;
	}
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public Theme getTheme() {
		return theme;
	}
	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	public int getDureeParJour() {
		return dureeParJour;
	}
	public void setDureeParJour(int dureeParJour) {
		this.dureeParJour = dureeParJour;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Module [getId()=" + getId() + ", getTitre()=" + getTitre() + ", getTheme()=" + getTheme()
				+ ", getDureeParJour()=" + getDureeParJour() + ", getDescription()=" + getDescription() + "]";
	}

   
	
}
