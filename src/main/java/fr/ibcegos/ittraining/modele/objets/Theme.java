package fr.ibcegos.ittraining.modele.objets;

import java.util.ArrayList;
import java.util.List;

public class Theme {
	private int id;
	private String nom;
	private List<Theme>childrens;
	
	public Theme() {
		this.childrens=new ArrayList<Theme>(); 
		}
	
	public Theme(String nom) {
		super();
		this.nom = nom;
	}
	
	public Theme(String nom, List<Theme> childrens) {
		super();
		this.nom = nom;
		this.childrens = childrens;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Theme> getChildrens() {
		return childrens;
	}

	public void setChildrens(List<Theme> childrens) {
		this.childrens = childrens;
	}


}
