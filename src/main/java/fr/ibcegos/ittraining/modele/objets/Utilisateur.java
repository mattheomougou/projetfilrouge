package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;

public class Utilisateur {

	private int id; // car on enregistre un utilisateur en BDD
	private String nom;
	private String prenom;
	private String mail;
	private String motDePasse;
	private String telephone;
	private String adresse;
	private String ville;
	private String codePostal;
	private LocalDate dateInscription;
	private boolean administrateur;

	public Utilisateur() {
		// TODO Auto-generated constructor stub
	}

	public Utilisateur(String nom, String mail, String motDePasse) {
		this.nom = nom;
		this.mail = mail;
		this.motDePasse = motDePasse;
		this.administrateur = false;
		this.dateInscription = LocalDate.now();
	}

	public Utilisateur(String nom, String prenom, String mail, String motDePasse, String telephone, String adresse,
			String ville, String codePostal) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.motDePasse = motDePasse;
		this.telephone = telephone;
		this.adresse = adresse;
		this.ville = ville;
		this.codePostal = codePostal;
		this.administrateur = false;
		this.dateInscription = LocalDate.now();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public LocalDate getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(LocalDate dateInscription) {
		this.dateInscription = dateInscription;
	}

	public boolean isAdministrateur() {
		return administrateur;
	}

	public void setAdministrateur(boolean administrateur) {
		this.administrateur = administrateur;
	}

	@Override
	public String toString() {
		return "Utilisateur [getId()=" + getId() + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom()
				+ ", getMail()=" + getMail() + ", getMotDePasse()=" + getMotDePasse() + ", getTelephone()="
				+ getTelephone() + ", getAdresse()=" + getAdresse() + ", getVille()=" + getVille()
				+ ", getCodePostal()=" + getCodePostal() + ", getDateInscription()=" + getDateInscription()
				+ ", isAdministrateur()=" + isAdministrateur() + "]";
	}

}
