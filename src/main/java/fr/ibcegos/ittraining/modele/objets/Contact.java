package fr.ibcegos.ittraining.modele.objets;

public class Contact {
	private int id_contact;
	private String objet;
	private String civilite;
	private String nom;
	private String prenom;
	private String telephone;
	private String email;
	private String fonction;
	private String societe;
	private String adresse;
	private String codePostal;
	private String ville;
	private String message;
	private String reponse;
	
	public Contact() {
		
	}

	public Contact(String objet, String civilite, String nom, String prenom, String telephone, String email,
			String fonction, String societe, String adresse, String codePostal, String ville, String message,
			String reponse) {
		super();
		this.objet = objet;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.email = email;
		this.fonction = fonction;
		this.societe = societe;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.message = message;
		this.reponse = reponse;
	}



	

	public int getId_contact() {
		return id_contact;
	}

	public void setId_contact(int id_contact) {
		this.id_contact = id_contact;
	}

	public String getObjet() {
		return objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getSociete() {
		return societe;
	}

	public void setSociete(String societe) {
		this.societe = societe;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	@Override
	public String toString() {
		return "Contact [id_contact=" + id_contact + ", objet=" + objet + ", civilite=" + civilite + ", nom=" + nom
				+ ", prenom=" + prenom + ", telephone=" + telephone + ", email=" + email + ", fonction=" + fonction
				+ ", societe=" + societe + ", adresse=" + adresse + ", codePostal=" + codePostal + ", ville=" + ville
				+ ", message=" + message + ", reponse=" + reponse + "]";
	}

	
	
	
}
