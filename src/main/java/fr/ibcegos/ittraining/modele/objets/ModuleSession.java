package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;

public class ModuleSession {
	private int id;
	private LocalDate dateDebut;
	private LocalDate dateFin; 
	private Salle salle;
	private Formateur formateur;
	private Module module;
	
		
	public ModuleSession() {
		}


	public ModuleSession(LocalDate dateDebut, LocalDate dateFin, Salle salle, Formateur formateur, Module module) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.salle = salle;
		this.formateur = formateur;
		this.module = module;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public LocalDate getDateDebut() {
		return dateDebut;
	}


	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}


	public LocalDate getDateFin() {
		return dateFin;
	}


	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}


	public Salle getSalle() {
		return salle;
	}


	public void setSalle(Salle salle) {
		this.salle = salle;
	}


	public Formateur getFormateur() {
		return formateur;
	}


	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}


	public Module getModule() {
		return module;
	}


	public void setModule(Module module) {
		this.module = module;
	}


	@Override
	public String toString() {
		return "ModuleSession [getId()=" + getId() + ", getDateDebut()=" + getDateDebut() + ", getDateFin()="
				+ getDateFin() + ", getSalle()=" + getSalle() + ", getFormateur()=" + getFormateur() + ", getModule()="
				+ getModule() + "]";
	}


	
	
	
}
