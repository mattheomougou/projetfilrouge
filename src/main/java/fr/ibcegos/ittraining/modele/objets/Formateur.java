package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;

public class Formateur extends Utilisateur {
	private int id;
	private LocalDate dateEmbauche;
	
	public Formateur() {
		super();
	}
	
	
	
	public Formateur(String nom, String prenom, String mail, String motDePasse, String telephone, String adresse,
			String ville, String codePostal) {
		super(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
		this.dateEmbauche=LocalDate.now();
	}
	
	public Formateur(String nom, String prenom, String mail, String motDePasse, String telephone, String adresse,
			String ville, String codePostal, LocalDate dateEmbauche) {
		super(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
		this.dateEmbauche=dateEmbauche;
	}



	public Formateur(String nom, String mail, String motDePasse) {
		super(nom, mail, motDePasse);
		// TODO Auto-generated constructor stub
	}



	@Override
	public String toString() {
		return "Formateur [id=" + id + ", dateEmbauche=" + dateEmbauche + "]";
	}

	public LocalDate getDateEmbauche() {
		return dateEmbauche;
	}

	public void setDateEmbauche(LocalDate dateEmbauche) {
		this.dateEmbauche = dateEmbauche;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	
	
}
