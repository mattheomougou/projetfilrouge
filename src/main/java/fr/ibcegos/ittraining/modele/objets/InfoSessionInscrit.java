package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;

public class InfoSessionInscrit {
	private int idUtilisateur;
	private int idStagiaire;
	private LocalDate dateDebut;
	private LocalDate dateFin;
	private String titre;
	
	public InfoSessionInscrit() {

	}

	public InfoSessionInscrit(int idUtilisateur, int idStagiaire, LocalDate dateDebut, LocalDate dateFin,
			String titre) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.idStagiaire = idStagiaire;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.titre = titre;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public int getIdStagiaire() {
		return idStagiaire;
	}

	public void setIdStagiaire(int idStagiaire) {
		this.idStagiaire = idStagiaire;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	@Override
	public String toString() {
		return "InfoSessionInscrit [idUtilisateur=" + idUtilisateur + ", idStagiaire=" + idStagiaire + ", dateDebut="
				+ dateDebut + ", dateFin=" + dateFin + ", titre=" + titre + "]";
	}
	
	
	
	
	
	

}
