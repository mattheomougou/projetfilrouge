package fr.ibcegos.ittraining.modele.objets;

public class Salle {

	private int id;
	private String nom;
	private String adresse;
	private String codePostal;
	private String ville;

	public Salle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Salle(String nom, String adresse, String codePostal, String ville) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	@Override
	public String toString() {
		return "Salle [getId()=" + getId() + ", getNom()=" + getNom() + ", getAdresse()=" + getAdresse()
				+ ", getCodePostal()=" + getCodePostal() + ", getVille()=" + getVille() + "]";
	}

}
