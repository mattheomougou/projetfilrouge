package fr.ibcegos.ittraining.modele.objets;

import java.time.LocalDate;


public class Employe extends Utilisateur {
	
	private int id; // car on enregistre un utilisateur en BDD
	private String fonction;
	private double salaire;
	private LocalDate dateEmbauche;
	private boolean responsablePlanning;
	
	
	public Employe() {
		super();
	}
	
	

	public Employe(String nom, String prenom, String mail, String motDePasse, String telephone, String adresse,
			String ville, String codePostal, String fonction, double salaire, boolean responsablePlanning) {
		super(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
		this.fonction=fonction;
		this.salaire=salaire;
		this.dateEmbauche=LocalDate.now();
		this.responsablePlanning=responsablePlanning;
		
	}
	
	public Employe(String nom, String prenom, String mail, String motDePasse, String telephone, String adresse,
			String ville, String codePostal, String fonction, double salaire, boolean responsablePlanning,LocalDate dateEmbauche) {
		super(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
		this.fonction=fonction;
		this.salaire=salaire;
		this.dateEmbauche=dateEmbauche;
		this.responsablePlanning=responsablePlanning;
		
	}



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFonction() {
		return fonction;
	}


	public void setFonction(String fonction) {
		this.fonction = fonction;
	}


	public double getSalaire() {
		return salaire;
	}


	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}


	@Override
	public String toString() {
		return "Employe [id=" + id + ", fonction=" + fonction + ", salaire=" + salaire + ", dateEmbauche="
				+ dateEmbauche + ", responsablePlanning=" + responsablePlanning + ", getNom()=" + getNom()
				+ ", getPrenom()=" + getPrenom() + ", getMail()=" + getMail() + ", getMotDePasse()=" + getMotDePasse()
				+ ", getTelephone()=" + getTelephone() + ", getAdresse()=" + getAdresse() + ", getVille()=" + getVille()
				+ ", getCodePostal()=" + getCodePostal() + ", getDateInscription()=" + getDateInscription()
				+ ", isAdministrateur()=" + isAdministrateur() + "]";
	}


	public LocalDate getDateEmbauche() {
		return dateEmbauche;
	}


	public void setDateEmbauche(LocalDate dateEmbauche) {
		this.dateEmbauche = dateEmbauche;
	}


	public boolean isResponsablePlanning() {
		return responsablePlanning;
	}


	public void setResponsablePlanning(boolean responsablePlanning) {
		this.responsablePlanning = responsablePlanning;
	}


	
	



}
