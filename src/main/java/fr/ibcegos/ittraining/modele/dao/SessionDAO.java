package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.ittraining.modele.objets.Formation;
import fr.ibcegos.ittraining.modele.objets.InfoSessionInscrit;
import fr.ibcegos.ittraining.modele.objets.Session;

public class SessionDAO {

	private FormationDAO formationDao = new FormationDAO();

	public Session getSession(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Session WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rsToSession(rs);
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void enregistrer(Session session) {
		try {

			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO Session(titre, type_session, prix, date_debut, date_fin, adresse, ville, code_postal , id_formation) "
					+ "VALUES(?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, session.getTitre());
			pstmt.setString(2, session.getType());
			pstmt.setDouble(3, session.getPrix());
			pstmt.setDate(4, Date.valueOf(session.getDateDebut()));
			pstmt.setDate(5, Date.valueOf(session.getDateFin()));

			pstmt.setString(6, session.getAdresse());
			pstmt.setString(7, session.getVille());
			pstmt.setString(8, session.getCodePostal());

			pstmt.setInt(9, session.getFormation().getId());
			pstmt.executeUpdate();

			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<Session> findAllSessions() {
		List<Session> sessions = new ArrayList<>();

		Connection cnx = PoolConnexion.getConnexion();
		String query = "SELECT * from Session";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Session session = rsToSession(rs);
				sessions.add(session);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sessions;
	}

	private Session rsToSession(ResultSet rs) {
		try {
			Session session = new Session();
			session.setId(rs.getInt("id"));
			session.setAdresse(rs.getString("adresse"));
			session.setCodePostal(rs.getString("code_postal"));
			session.setVille(rs.getString("ville"));
			session.setDateDebut(LocalDate.parse(rs.getString("date_debut")));
			session.setDateFin(LocalDate.parse(rs.getString("date_fin")));
			int formationId = rs.getInt("id_formation");
			Formation formation = formationDao.findById(formationId);
			session.setFormation(formation);
			return session;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}



	public Map<Integer, Integer> findAllInscrit() {
		Map<Integer, Integer> nombreInscritSession = new HashMap<>();

		Connection cnx = PoolConnexion.getConnexion();
		String query = "select count(distinct id_stagiaire) as nombre_inscrit, id_session \r\n"
				+ "from Stagiaire_Session\r\n" + "group by id_session ";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				int idSession = rs.getInt("id_session");
				int nombreInscrit = rs.getInt("nombre_inscrit");
				nombreInscritSession.put(idSession, nombreInscrit);
				// System.out.println("nombreInscritSession"+nombreInscritSession);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nombreInscritSession;
	}

	public List<InfoSessionInscrit> findAllSessionOfStagiaire(int idUser) {
		List<InfoSessionInscrit> infoSessionInscrits = new ArrayList<>();

		Connection cnx = PoolConnexion.getConnexion();
		String query = "select s.id as id_stagiaire, u.id as id_utilisateur,se.date_debut,se.date_fin,f.titre from Stagiaire_Session ss\r\n"
				+ "join stagiaire s on ss.id_stagiaire=s.id\r\n"
				+ "Join utilisateur u on u.id=s.id_utilisateur\r\n"
				+ "join session se on se.id=ss.id_session\r\n"
				+ "join formation f on se.id_formation = f.id\r\n"
				+ "where u.id=? ";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setInt(1, idUser);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				InfoSessionInscrit infoSessionInscrit = rsToInfoSessionInscrit(rs);
				infoSessionInscrits.add(infoSessionInscrit);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return infoSessionInscrits;
	}
	
	private InfoSessionInscrit rsToInfoSessionInscrit(ResultSet rs) {
		try {
			InfoSessionInscrit infoSessionInscrit = new InfoSessionInscrit();
			infoSessionInscrit.setIdStagiaire(rs.getInt("id_stagiaire"));
			infoSessionInscrit.setIdUtilisateur(rs.getInt("id_utilisateur"));
			infoSessionInscrit.setDateDebut(LocalDate.parse(rs.getString("date_debut")));
			infoSessionInscrit.setDateFin(LocalDate.parse(rs.getString("date_fin")));
			infoSessionInscrit.setTitre(rs.getString("titre"));
			return infoSessionInscrit;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
