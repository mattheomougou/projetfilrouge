package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.TestInscription;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class TestDAO {

	public void enregistrer(TestInscription test) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib
		// du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			// 3 - Ex�cuter la requete
			String query = "INSERT INTO Test(note,date_debut,id_formation ,id_utilisateur) "
					+ "VALUES(?,?,?,?)";

			PreparedStatement pstmt = connexion.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, test.getNote());
			pstmt.setDate(2, Date.valueOf(test.getDateDePassageDuTest()));
			pstmt.setInt(3, test.getIdFormation());
			pstmt.setInt(4, test.getIdUtilisateur());
			// pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		
		} catch (Exception e) {
			e.printStackTrace();
			
		}

	}



	public TestInscription findTestByUtilisateurAndFormation(int idUtilisateur, int idFormateur) {
		TestInscription test = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM test WHERE id_utilisateur = ? and id_formation=?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, idUtilisateur);
			pstmt.setInt(2,idFormateur);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while (rs.next()) {
				test = remplirTest(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return test;
	}

	private TestInscription remplirTest(ResultSet rs) throws SQLException {
		TestInscription test = new TestInscription();
		test.setId(rs.getInt("id"));
		test.setNote(rs.getInt("note"));
		test.setDateDePassageDuTest(rs.getDate("date_debut").toLocalDate());
		test.setIdFormation(rs.getInt("id_formation"));
		test.setIdUtilisateur(rs.getInt("id_utilisateur"));

		return test;
	}

}
