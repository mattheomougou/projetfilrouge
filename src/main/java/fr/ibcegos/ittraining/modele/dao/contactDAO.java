package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Contact;
import fr.ibcegos.ittraining.modele.objets.Evaluation;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class contactDAO {
	
	public static void enregistrer(Contact contact) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			
			String query = "INSERT INTO Contact( objet,civilite,nom, \r\n"
					+ "prenom,telephone,email,fonction,\r\n"
					+ "societe,adresse,code_postal,ville,message,reponse) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			// 3 - Ex�cuter la requete
			
			PreparedStatement pstmt = connexion.prepareStatement(query);
			//pstmt.setInt(1, evaluation.getId_Session());
			//pstmt.setInt(1, evaluation.getId_Utilisateur());
			pstmt.setString(1, contact.getObjet());
			pstmt.setString(2, contact.getCivilite());
			pstmt.setString(3, contact.getNom());
			pstmt.setString(4, contact.getPrenom());
			pstmt.setString(5, contact.getTelephone());
			pstmt.setString(6, contact.getEmail());
			pstmt.setString(7, contact.getFonction());
			pstmt.setString(8, contact.getSociete());
			pstmt.setString(9, contact.getAdresse());
			pstmt.setString(10, contact.getCodePostal());
			pstmt.setString(11, contact.getVille());
			pstmt.setString(12, contact.getMessage());
			pstmt.setString(13, contact.getReponse());
			//pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	}
	
	/*public Utilisateur findByMailAndMotDePasse(String mail, String motDePasse) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Utilisateur WHERE mail = ? and "
					+ "mot_de_passe = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2,motDePasse);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}


	private Utilisateur remplirUtilisateur(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt("id"));
		utilisateur.setNom(rs.getString("nom"));
		utilisateur.setMail(rs.getString("mail"));
		utilisateur.setMotDePasse(rs.getString("mot_de_passe")); 
		utilisateur.setDateInscription(rs.getDate("date_inscription").toLocalDate());					
		return utilisateur;
	}


}*/
