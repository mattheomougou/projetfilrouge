package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Evaluation;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class EvaluationDAO {
	
	public void enregistrer(Evaluation evaluation) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			// 3 - Ex�cuter la requete
			String query = "INSERT INTO Contact( note_accueil,note_contenu,note_disponibilite, \r\n"
					+ "note_maitrise_domaine,note_pedagogie,note_reponse_aux_questions,note_technique_animation,\r\n"
					+ "note_qualite_environnement,note_formateur,satisfaction_stagiaire,recommandation_stagiaire,projet_formation,commentaires_projet_formation) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			//pstmt.setInt(1, evaluation.getId_Session());
			//pstmt.setInt(1, evaluation.getId_Utilisateur());
			pstmt.setInt(1, evaluation.getAccueil());
			pstmt.setInt(2, evaluation.getContenu());
			pstmt.setInt(3, evaluation.getDisponibilit�());
			pstmt.setInt(4, evaluation.getMa�triseDomaine());
			pstmt.setInt(5, evaluation.getPedagogie());
			pstmt.setInt(6, evaluation.getReponsesQuestion());
			pstmt.setInt(7, evaluation.getAnimation());
			pstmt.setInt(8, evaluation.getEnvironnement());
			pstmt.setInt(9, evaluation.getFormateur());
			pstmt.setInt(10, evaluation.getSatisfaction());
			pstmt.setBoolean(11, evaluation.isRecommandation());
			pstmt.setBoolean(12, evaluation.isProjetFormation());
			pstmt.setString(13, evaluation.getCommentaires_projet_formation());
			//pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	}
	
	/*public Utilisateur findByMailAndMotDePasse(String mail, String motDePasse) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Utilisateur WHERE mail = ? and "
					+ "mot_de_passe = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2,motDePasse);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}


	private Utilisateur remplirUtilisateur(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt("id"));
		utilisateur.setNom(rs.getString("nom"));
		utilisateur.setMail(rs.getString("mail"));
		utilisateur.setMotDePasse(rs.getString("mot_de_passe")); 
		utilisateur.setDateInscription(rs.getDate("date_inscription").toLocalDate());					
		return utilisateur;
	}


}*/
