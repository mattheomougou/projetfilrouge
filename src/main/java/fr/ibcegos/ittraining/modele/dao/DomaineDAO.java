package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Domaine;

public class DomaineDAO {

	public static Domaine findDomaineById(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Domaine WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				Domaine domaine = new Domaine();
				domaine.setId(rs.getInt("id"));
				domaine.setNom(rs.getString("nom"));
				return domaine;
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Domaine findDomaineByName(String domaineName) {
		Domaine domaine = new Domaine();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Domaine WHERE nom = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, domaineName);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				domaine.setId(rs.getInt("id"));
				domaine.setNom(rs.getString("nom"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domaine;
	}



	public List<Domaine> findAllDomaine() {
		List<Domaine> domaines = new ArrayList<>();
		// 1 - Se connecter � la BDD
		Connection cnx = PoolConnexion.getConnexion();
		// 2 - Fabrique la requete
		String query = "SELECT * FROM Domaine";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Domaine domaine = new Domaine();
				domaine.setId(rs.getInt("id"));
				domaine.setNom(rs.getString("nom"));
				domaines.add(domaine);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return domaines;
	}

	public void enregistrer(Domaine domaine) {
		// TODO Auto-generated method stub

	}

}
