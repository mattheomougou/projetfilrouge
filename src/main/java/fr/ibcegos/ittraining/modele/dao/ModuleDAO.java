package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Domaine;
import fr.ibcegos.ittraining.modele.objets.Module;
import fr.ibcegos.ittraining.modele.objets.Theme;

public class ModuleDAO {

	public Module getModule(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Module WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rsToModule(rs);
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void enregistrer(Module module) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO Module(titre,duree ,description, id_theme) " + "VALUES(?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, module.getTitre());
			pstmt.setInt(2, module.getDureeParJour());
			pstmt.setString(3, module.getDescription());
			pstmt.setInt(4, module.getTheme().getId());

			pstmt.executeUpdate();
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<Module> findModules() {
		List<Module> modules = new ArrayList<>();

		Connection cnx = PoolConnexion.getConnexion();
		String query = "SELECT * from Module";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Module module = rsToModule(rs);
				modules.add(module);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return modules;
	}

	private Module rsToModule(ResultSet rs) {
		try {
			Module module = new Module();
			module.setId(rs.getInt("id"));
			module.setTitre(rs.getString("titre"));
			module.setDureeParJour(rs.getInt("duree"));
			module.setDescription(rs.getString("description"));
			ThemeDAO themeDAO = new ThemeDAO();
			Theme theme = themeDAO.findThemeById(rs.getInt("id_theme"));
			module.setTheme(theme);
			return module;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Module> findAllModulesOfFormation(int formationId) {
		List<Module> modules = null;

		return modules;
	}

	public List<Module> findAllModulesOfSession(int sessionId) {
		List<Module> modules = null;

		return modules;
	}

	public List<Module> findModulesByNames(String[] selectedModulesNames) {
		// TODO Auto-generated method stub
		return null;
	}

}
