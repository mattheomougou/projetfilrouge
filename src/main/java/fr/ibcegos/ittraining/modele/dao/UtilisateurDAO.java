package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.ibcegos.ittraining.modele.objets.Stagiaire;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class UtilisateurDAO {

	public Utilisateur enregistrer(Utilisateur utilisateur) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib
		// du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			// 3 - Ex�cuter la requete
			String query = "INSERT INTO Utilisateur(nom,prenom,mail ,mot_de_passe,telephone,adresse,ville,code_postal, date_inscription,administrateur) "
					+ "VALUES(?,?,?,CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(? as varchar)),2),?,?,?,?,?,?)";

			PreparedStatement pstmt = connexion.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, utilisateur.getNom());
			pstmt.setString(2, utilisateur.getPrenom());
			pstmt.setString(3, utilisateur.getMail());
			pstmt.setString(4, utilisateur.getMotDePasse());
			pstmt.setString(5, utilisateur.getTelephone());
			pstmt.setString(6, utilisateur.getAdresse());
			pstmt.setString(7, utilisateur.getVille());
			pstmt.setString(8, utilisateur.getCodePostal());
			pstmt.setDate(9, Date.valueOf(utilisateur.getDateInscription()));
			pstmt.setBoolean(10, utilisateur.isAdministrateur());
			// pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				int last_inserted_id = rs.getInt(1);
				utilisateur.setId(last_inserted_id);
			}
			connexion.close();
			return utilisateur;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public Utilisateur findByMailAndMotDePasse(String mail, String motDePasse) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Utilisateur WHERE mail = ? and "
					+ "mot_de_passe = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2, motDePasse);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels

			while (rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	private Utilisateur remplirUtilisateur(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt("id"));
		utilisateur.setNom(rs.getString("nom"));
		utilisateur.setPrenom(rs.getString("prenom"));
		utilisateur.setMail(rs.getString("mail"));
		utilisateur.setMotDePasse(rs.getString("mot_de_passe"));
		utilisateur.setTelephone(rs.getString("telephone"));
		utilisateur.setAdresse(rs.getString("adresse"));
		utilisateur.setVille(rs.getString("ville"));
		utilisateur.setCodePostal(rs.getString("code_postal"));
		utilisateur.setDateInscription(rs.getDate("date_inscription").toLocalDate());
		utilisateur.setAdministrateur(rs.getBoolean("administrateur"));

		return utilisateur;
	}

	public Utilisateur findByNom(String nom) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Utilisateur WHERE nom = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, nom);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while (rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	public Utilisateur findByMail(String mail) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Utilisateur WHERE mail = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while (rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}
	
	
	public Utilisateur find(int id) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Utilisateur WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while (rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	public void supprimerByID(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();

			// 2 - Fabriquer la requete
			String query = "DELETE Utilisateur WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);

			// 3 - Ex�cuter la requete
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Utilisateur modifier(Utilisateur utilisateur) {
		Utilisateur utilisateurModifie= new Utilisateur();
				try {
					Connection connexion = PoolConnexion.getConnexion();
					String query = "UPDATE utilisateur SET nom = ?, prenom =?, telephone= ?,adresse=?,ville=?,code_postal=? WHERE id= ?";
					PreparedStatement pstmt = connexion.prepareStatement(query);
					pstmt.setString(1, utilisateur.getNom());
					pstmt.setString(2, utilisateur.getPrenom());
					pstmt.setString(3, utilisateur.getTelephone());
					pstmt.setString(4, utilisateur.getAdresse());
					pstmt.setString(5, utilisateur.getVille());
					pstmt.setString(6, utilisateur.getCodePostal());
					System.out.println("l'id de user modifi�"+utilisateur.getId());
					pstmt.setInt(7, utilisateur.getId());
					pstmt.executeUpdate();
					utilisateurModifie = find(utilisateur.getId());
					System.out.println("utilisateurModifier"+utilisateurModifie);
					return utilisateurModifie;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}		

	}

}
