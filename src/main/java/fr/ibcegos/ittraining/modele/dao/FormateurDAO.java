package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Employe;
import fr.ibcegos.ittraining.modele.objets.Formateur;


public class FormateurDAO {
	
	public void enregistrer(Formateur formateur, int id) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			// 3 - Ex�cuter la requete
			String query = "INSERT INTO Formateur(date_embauche, id_utilisateur) "
					+ "VALUES(?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setDate(1, Date.valueOf(formateur.getDateEmbauche()));
			pstmt.setInt(2, id);
			//pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	


	private Formateur remplirFormateur(ResultSet rs) throws SQLException {
		Formateur formateur = new Formateur();
		formateur.setId(rs.getInt("id"));
		formateur.setNom(rs.getString("nom"));
		formateur.setMail(rs.getString("mail"));
		formateur.setAdresse(rs.getString("adresse"));
		formateur.setCodePostal(rs.getString("code_postal"));
		formateur.setDateEmbauche(rs.getDate("date_embauche").toLocalDate());	
		formateur.setVille(rs.getString("ville"));
		formateur.setTelephone(rs.getString("telephone"));
		formateur.setPrenom(rs.getString("prenom"));
		formateur.setAdministrateur(rs.getBoolean("administrateur"));
		formateur.setDateInscription(rs.getDate("date_inscription").toLocalDate());	
		return formateur;
	}
	
	public Formateur findByMail(String mail) {
		Formateur formateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Formateur f JOIN Utilisateur u On u.id=f.id_utilisateur WHERE mail = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				formateur = remplirFormateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formateur;
	}




	public List<Formateur> findAll() {
		List<Formateur> formateurs = new ArrayList<>();;
		
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Formateur f JOIN Utilisateur u On u.id=f.id_utilisateur";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				Formateur formateur  = remplirFormateur(rs);
				formateurs.add(formateur);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formateurs;
	}




	public void supprimerByID(int id) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		int idUtilisateur=0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query1="select id_utilisateur from formateur where id = ?";
			PreparedStatement pstmt1 = connexion.prepareStatement(query1);
			pstmt1.setInt(1, id);
			
			// 3 - Ex�cuter la requete
			ResultSet rs=pstmt1.executeQuery();
			while(rs.next()) {
				idUtilisateur=rs.getInt("id_utilisateur");
				
			}
			String query2 = "DELETE Formateur WHERE id = ?";
			PreparedStatement pstmt2 = connexion.prepareStatement(query2);
			pstmt2.setInt(1, id);
			pstmt2.executeUpdate();
			utilisateurDAO.supprimerByID(idUtilisateur);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}




	public Formateur findByID(int id) {
		Formateur formateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Formateur f JOIN Utilisateur u On u.id=f.id_utilisateur WHERE f.id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				formateur = remplirFormateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formateur;
	}




	public int getNombreFormateur() {
		int nombreFormateur =0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "Select count(*) as nombre from Formateur";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				nombreFormateur=rs.getInt("nombre");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nombreFormateur;
	}


}
