package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Employe;

public class EmployeDAO {
	
	public void enregistrer(Employe employe, int id) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			// 3 - Ex�cuter la requete
			String query = "INSERT INTO Employe(fonction,salaire,responsable_planning,date_embauche,id_utilisateur) "
					+ "VALUES(?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, employe.getFonction());
			pstmt.setDouble(2, employe.getSalaire());
			pstmt.setBoolean(3, employe.isResponsablePlanning());
			pstmt.setDate(4,Date.valueOf(employe.getDateEmbauche()));
			pstmt.setInt(5, id);
			//pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	


	private Employe remplirEmploye(ResultSet rs) throws SQLException {
		Employe employe;
		employe= new Employe();
		employe.setId(rs.getInt("id"));
		employe.setNom(rs.getString("nom"));
		employe.setMail(rs.getString("mail"));
		employe.setFonction(rs.getString("fonction")); 
		employe.setSalaire(rs.getDouble("salaire")); 
		employe.setAdresse(rs.getString("adresse"));
		employe.setCodePostal(rs.getString("code_postal"));
		employe.setResponsablePlanning(rs.getBoolean("responsable_planning"));
		employe.setDateEmbauche(rs.getDate("date_embauche").toLocalDate());	
		employe.setVille(rs.getString("ville"));
		employe.setTelephone(rs.getString("telephone"));
		employe.setPrenom(rs.getString("prenom"));
		employe.setAdministrateur(rs.getBoolean("administrateur"));
		employe.setDateInscription(rs.getDate("date_inscription").toLocalDate());	
		return employe;
	}
	
	public Employe findByMail(String mail) {
		Employe employe = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Employe WHERE mail = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				employe = remplirEmploye(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employe;
	}




	public Employe findByID(int id) {
		Employe employe = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Employe e Join Utilisateur u On u.id=e.id_utilisateur WHERE e.id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				employe = remplirEmploye(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employe;
	}




	public void supprimerByID(int id) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		int idUtilisateur=0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query1="select id_utilisateur from employe where id = ?";
			PreparedStatement pstmt1 = connexion.prepareStatement(query1);
			pstmt1.setInt(1, id);
			
			// 3 - Ex�cuter la requete
			ResultSet rs=pstmt1.executeQuery();
			while(rs.next()) {
				idUtilisateur=rs.getInt("id_utilisateur");
				
			}
			String query2 = "DELETE Employe WHERE id = ?";
			PreparedStatement pstmt2 = connexion.prepareStatement(query2);
			pstmt2.setInt(1, id);
			pstmt2.executeUpdate();
			utilisateurDAO.supprimerByID(idUtilisateur);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




	public void supprimerByMail(String mail) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "DELETE Employe WHERE mail = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			// 3 - Ex�cuter la requete
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}




	
	public List<Employe> findAll() {
		List<Employe> employes = new ArrayList<>();;
		
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Employe e JOIN Utilisateur u On u.id=e.id_utilisateur";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				Employe employe  = remplirEmploye(rs);
				employes.add(employe);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employes;
	}




	public int getNombreEmploye() {
		int nombreEmploye =0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "Select count(*) as nombre from Employe";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				nombreEmploye=rs.getInt("nombre");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nombreEmploye;
	}


}
