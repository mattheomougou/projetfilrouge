package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.ModuleSession;
import fr.ibcegos.ittraining.modele.objets.Salle;
import fr.ibcegos.ittraining.modele.objets.Module;
public class ModuleSessionDAO {


public void enregistrer(ModuleSession moduleSession) {
		try {

			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO ModuleSession(date_debut, date_fin, id_salle, id_module , id_formation) "
					+ "VALUES(?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setDate(1, Date.valueOf(moduleSession.getDateDebut()));
			pstmt.setDate(2, Date.valueOf(moduleSession.getDateFin()));
			pstmt.setInt(3, moduleSession.getSalle().getId());
			pstmt.setInt(4, moduleSession.getModule().getId());
			pstmt.setInt(5, moduleSession.getFormateur().getId());
			
			pstmt.executeUpdate();
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	

	private ModuleSession rsToModuleSession(ResultSet rs) {
		try {
			ModuleSession moduleSession = new ModuleSession();
			moduleSession.setId(rs.getInt("id"));
			moduleSession.setDateDebut(LocalDate.parse(rs.getString("date_debut")));
			moduleSession.setDateFin(LocalDate.parse(rs.getString("date_fin")));
			
			FormateurDAO formateurDAO = new FormateurDAO();
			Formateur formateur = formateurDAO.findByID(rs.getInt("id_formateur"));
			moduleSession.setFormateur(formateur);
			
			ModuleDAO moduleDAO = new ModuleDAO();
			Module module= moduleDAO.getModule(rs.getInt("id_module"));
			moduleSession.setModule(module);
			
			SalleDAO salleDAO = new SalleDAO();
			Salle salle= salleDAO.findSalle(rs.getInt("id_salle"));
			moduleSession.setSalle(salle);
			
			return moduleSession;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<ModuleSession> findAllModulesSession(int sessionId) {
		List<ModuleSession> modulesSession = new ArrayList<>();

		Connection cnx = PoolConnexion.getConnexion();
		String query = "SELECT * from ModuleSession  where id_session =? ";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				ModuleSession moduleSession = rsToModuleSession(rs);
				modulesSession.add(moduleSession);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return modulesSession;
	}

	public ModuleSession getModuleSession(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Session WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rsToModuleSession(rs);
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
}
