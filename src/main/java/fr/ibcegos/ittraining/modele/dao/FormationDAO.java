package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.EmployeManager;
import fr.ibcegos.ittraining.modele.managers.ThemeManager;
import fr.ibcegos.ittraining.modele.objets.Domaine;
import fr.ibcegos.ittraining.modele.objets.Employe;
import fr.ibcegos.ittraining.modele.objets.Formation;
import fr.ibcegos.ittraining.modele.objets.Module;
import fr.ibcegos.ittraining.modele.objets.Session;
import fr.ibcegos.ittraining.modele.objets.Theme;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;

public class FormationDAO {

	public void enregistrer(Formation formation) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib
		// du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO Formation(titre,description,prerequis,objectifs,niveau_exige,prix,id_domaine) "
					+ "VALUES(?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			// Formation( List<Module> modules, double prix, Utilisateur
			// responsableFormation) {

			pstmt.setString(1, formation.getTitre());
			pstmt.setString(2, formation.getDescription());
			pstmt.setString(3, formation.getPrerequis());
			pstmt.setString(4, formation.getObjectifs());
			pstmt.setInt(5, formation.getNiveauExige());
			pstmt.setDouble(6, formation.getPrix());
			if (formation.getDomaine() != null) {
				pstmt.setInt(7, formation.getDomaine().getId());
			}
//			if (formation.getResponsableFormation() != null) {
//				pstmt.setInt(8, formation.getResponsableFormation().getId());
//			}
			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				int last_inserted_id = rs.getInt(1);

				List<Module> modules = formation.getModules();

				for (Module module : modules) {
					String query2 = "INSERT INTO Module_Formation(id_formation,id_module)" + "VALUES(?,?)";
					PreparedStatement pstmt2 = connexion.prepareStatement(query2);

					pstmt2.setInt(1, last_inserted_id);
					pstmt2.setInt(2, module.getId());

					pstmt2.executeUpdate();
				}

			}

			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<Formation> findAllFormations() {
		List<Formation> formations = new ArrayList<>();
		List<Module> modules = new ArrayList<>();
		List<Session> sessions = new ArrayList<>();
		Connection cnx = PoolConnexion.getConnexion();
		EmployeManager employeManager = new EmployeManager();
		String query = "select * from Formation";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Formation formation = new Formation();
				formation = remplirFormation(rs);

				modules = findAllModulesOfFormation(rs.getInt("id"));
				formation.setModules(modules);

				sessions = findAllSessionsOfFormation(rs.getInt("id"));
				formation.setSessions(sessions);

				/*
				 * Utilisateur responsable = new Employe(); if (responsable != null) {
				 * responsable = employeManager.findByID(rs.getInt("id_responsable"));
				 * formation.setResponsableFormation(responsable); }
				 */
				formations.add(formation);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formations;
	}

	private Formation remplirFormation(ResultSet rs) throws SQLException {
		Formation formation = new Formation();
		formation.setId(rs.getInt("id"));
		formation.setTitre(rs.getString("titre"));
		formation.setDescription(rs.getString("description"));
		formation.setObjectifs(rs.getString("objectifs"));
		formation.setPrerequis(rs.getString("prerequis"));
		formation.setPrix(rs.getDouble("prix"));
		formation.setNiveauExige(rs.getInt("niveau_exige"));
		Domaine domaine = DomaineDAO.findDomaineById(rs.getInt("id_domaine"));
		formation.setDomaine(domaine);
		return formation;
	}

	private List<Module> findAllModulesOfFormation(int formationID) {
		List<Module> modules = new ArrayList<>();

		Connection cnx = PoolConnexion.getConnexion();

		String query = "SELECT * ,m.id as id_module FROM Module_Formation mf INNER JOIN Module m ON m.id = mf.id_module  where mf.id_formation = ?";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setInt(1, formationID);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Module module = new Module();
				module.setId(rs.getInt("id_module"));
				module.setTitre(rs.getString("titre"));
				module.setDescription(rs.getString("description"));

				ThemeManager themeManager = new ThemeManager();
				Theme theme = themeManager.getThemeById(rs.getInt("id_theme"));
				module.setTheme(theme);
				modules.add(module);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return modules;
	}

	private List<Session> findAllSessionsOfFormation(int formationID) {
		List<Session> sessions = new ArrayList<>();

		Connection cnx = PoolConnexion.getConnexion();

		String query = "SELECT * ,s.id as id_session,s.titre as titre_session,s.prix as prix_session FROM Formation f INNER JOIN Session s ON f.id = s.id_formation  where s.id_formation = ?;";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setInt(1, formationID);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Session session = new Session();
				session.setId(rs.getInt("id_session"));
				session.setTitre(rs.getString("titre_session"));
				session.setType(rs.getString("type_session"));
				session.setDateDebut(LocalDate.parse(rs.getString("date_debut")));
				session.setDateFin(LocalDate.parse(rs.getString("date_fin")));
				session.setPrix(rs.getDouble("prix_session"));
				session.setAdresse(rs.getString("adresse"));
				session.setVille(rs.getString("ville"));
				session.setCodePostal(rs.getString("code_postal"));
				sessions.add(session);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sessions;
	}

	public Formation findById(int formationId) {
		Formation formation = null;
		List<Module> modules = new ArrayList<>();
		List<Session> sessions = new ArrayList<>();
		Connection cnx = PoolConnexion.getConnexion();

		String query = "select * from Formation where id = ?";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setInt(1, formationId);

			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				formation = remplirFormation(rs);

				modules = findAllModulesOfFormation(rs.getInt("id"));
				formation.setModules(modules);

				sessions = findAllSessionsOfFormation(rs.getInt("id"));
				formation.setSessions(sessions);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formation;
	}

	public void supprimerByID(int id) {
		Connection connexion = null ;
		try {

			connexion = PoolConnexion.getConnexion();
			connexion.setAutoCommit(false);
			// 2 - Fabriquer la requete
			String query = "DELETE FROM Module_Formation WHERE id_formation= ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();

			
			String query2 = "DELETE FROM session WHERE id_formation=?";
		    pstmt = connexion.prepareStatement(query2);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			
			String query3 = "DELETE Formation WHERE id = ?";
		    pstmt = connexion.prepareStatement(query3);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			
			
			
			connexion.commit();

			System.out.println("Done!");

		} catch (Exception e) {
			//throw new RuntimeException(e);
			System.out.println(e.getMessage());
			try {
				connexion.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		} finally {

			if (connexion != null) {
					try {
						connexion.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}

		}

	}


	public List<Formation> searchFormations(String motCle) {
		List<Formation> formations = new ArrayList<>();
		List<Module> modules = new ArrayList<>();
		List<Session> sessions = new ArrayList<>();
		Connection cnx = PoolConnexion.getConnexion();

		String query = "SELECT * FROM Formation WHERE titre LIKE ?";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setString(1, "%" + motCle + "%");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Formation formation = new Formation();
				formation = remplirFormation(rs);

				modules = findAllModulesOfFormation(rs.getInt("id"));
				formation.setModules(modules);

				sessions = findAllSessionsOfFormation(rs.getInt("id"));
				formation.setSessions(sessions);

				formations.add(formation);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formations;
	}

	public int CalculerDureeFormation(int id) {
		int dureeFormation = 0;
		List<Module> modules = new ArrayList<>();
		modules = findAllModulesOfFormation(id);
		for (int i = 0; i < modules.size(); i++) {
			dureeFormation = (modules.get(i).getDureeParJour()) + dureeFormation;
		}
		return dureeFormation;
	}

	public int getNiveauExige(int formationId) {
		int niveauExige = 0;
		Connection cnx = PoolConnexion.getConnexion();

		String query = "select niveau_exige from Formation where id = ?";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setInt(1, formationId);

			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				niveauExige = rs.getInt("niveau_exige");

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return niveauExige;
	}

	public int getNombreFormation() {
		int nombreFormation=0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "Select count(*) as nombre from Formation";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				nombreFormation=rs.getInt("nombre");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nombreFormation;
	}

}
