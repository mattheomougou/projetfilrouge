package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Employe;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Module;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;

public class StagiaireDAO {

	public void inscrireASession(int idStagiaire, int idSession) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query2 = "INSERT INTO Stagiaire_Session(id_stagiaire,id_session)" + "VALUES(?,?)";
			PreparedStatement pstmt2 = connexion.prepareStatement(query2);

			pstmt2.setInt(1, idStagiaire);
			pstmt2.setInt(2, idSession);

			pstmt2.executeUpdate();
			
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Stagiaire remplirStagiaire(ResultSet rs) throws SQLException {
		Stagiaire stagiaire = new Stagiaire();
		stagiaire.setId(rs.getInt("id"));
		stagiaire.setNom(rs.getString("nom"));
		stagiaire.setPrenom(rs.getString("prenom"));
		stagiaire.setMail(rs.getString("mail"));
		stagiaire.setTelephone(rs.getString("telephone"));
		stagiaire.setAdresse(rs.getString("adresse"));
		stagiaire.setVille(rs.getString("ville"));
		stagiaire.setCodePostal(rs.getString("code_postal"));
		stagiaire.setDateInscription(rs.getDate("date_inscription").toLocalDate());	
		stagiaire.setEntreprise(rs.getString("entreprise"));
		return stagiaire;
	}

	public List<Stagiaire> findAll() {
		List<Stagiaire> stagiaires = new ArrayList<>();
		;

		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Stagiaire s JOIN Utilisateur u On u.id=s.id_utilisateur";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while (rs.next()) {
				Stagiaire stagiaire = remplirStagiaire(rs);
				stagiaires.add(stagiaire);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stagiaires;
	}

	public void supprimerByID(int id) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		int idUtilisateur=0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query="select id_utilisateur from stagiaire where id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			
			// 3 - Ex�cuter la requete
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				idUtilisateur=rs.getInt("id_utilisateur");
				
			}
			// 2 - Fabriquer la requete
			String query1 =" DELETE Stagiaire_Session where id_stagiaire=?" ;
			PreparedStatement pstmt1 = connexion.prepareStatement(query1);
			pstmt1.setInt(1, id);
			pstmt1.executeUpdate();
			String query2 = "DELETE Stagiaire WHERE id = ?";
			PreparedStatement pstmt2 = connexion.prepareStatement(query2);
			pstmt2.setInt(1, id);
			// 3 - Ex�cuter la requete
			pstmt2.executeUpdate();
			utilisateurDAO.supprimerByID(idUtilisateur);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Stagiaire findByUserID(int id) {
		Stagiaire stagiaire = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Stagiaire s JOIN Utilisateur u On u.id=s.id_utilisateur WHERE u.id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while (rs.next()) {
				stagiaire = remplirStagiaire(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stagiaire;
	}

	public Stagiaire findByID(int id) {
		Stagiaire stagiaire = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Stagiaire s JOIN Utilisateur u On u.id=s.id_utilisateur WHERE s.id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while (rs.next()) {
				stagiaire = remplirStagiaire(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stagiaire;
	}

	public Stagiaire enregistrer(Stagiaire stagiaire, int idUser) {

		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO Stagiaire(entreprise , id_utilisateur ) " + "VALUES(?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, stagiaire.getEntreprise());
			pstmt.setInt(2, idUser);

			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				int last_inserted_id = rs.getInt(1);
				stagiaire.setId(last_inserted_id);
			}
			connexion.close();
			return stagiaire;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Stagiaire modifier(Stagiaire stagiaire, int idUser) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = " UPDATE Stagiaire SET entreprise = ? WHERE id_utilisateur= ?" ;
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, stagiaire.getEntreprise());
			pstmt.setInt(2, idUser);

			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				int last_inserted_id = rs.getInt(1);
				stagiaire.setId(last_inserted_id);
			}
			connexion.close();
			return stagiaire;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	public int getNombreStagiaire() {
		int nombreStagiaire =0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "Select count(*) as nombre from Stagiaire";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				nombreStagiaire=rs.getInt("nombre");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nombreStagiaire;
	}

}
