package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Theme;

public class ThemeDAO {

	public Theme findThemeByName(String themeName) {
		Theme theme = new Theme();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Theme WHERE nom = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, themeName);
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next()) {
				theme.setId(rs.getInt("id"));
				theme.setNom(rs.getString("nom"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return theme;
	}

	public Theme findThemeById(int themeId) {
		Theme theme = new Theme();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Theme WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, themeId);
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next()) {
				theme.setId(rs.getInt("id"));
				theme.setNom(rs.getString("nom"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return theme;
	}
	
	
	public List<Theme> findAllThemes() {
		List<Theme> themes = new ArrayList<>();
		// 1 - Se connecter � la BDD
		Connection cnx = PoolConnexion.getConnexion();
		// 2 - Fabrique la requete
		String query = "SELECT t.id as id_theme, t.nom as nom_theme, tl.id_parent as id_parent FROM Theme t JOIN Theme_lien tl ON t.id = tl.id_theme";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Theme theme = new Theme();
				theme.setId(rs.getInt("id_theme"));
				theme.setNom(rs.getString("nom_theme"));
				themes.add(theme);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return themes;
	}

	
	public List<Theme> findAllChildrensByIdTheme(int themeId) {
		List<Theme> themeChildrens = new ArrayList<>();
		Connection cnx = PoolConnexion.getConnexion();
		String query = "SELECT t.id as id_theme, t.nom as nom_theme, tl.id_parent as id_parent FROM Theme t JOIN Theme_lien tl ON t.id = tl.id_theme WHERE id_parent=?";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			
			pstmt.setInt(1, themeId);
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next()) {
				Theme theme = new Theme();
				theme.setId(rs.getInt(themeId));
				theme.setNom(rs.getString("nom"));
				themeChildrens.add(theme);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return themeChildrens;
	}

	public void enregistrer(Theme theme) {
		// TODO Auto-generated method stub
		
	}

	
}
