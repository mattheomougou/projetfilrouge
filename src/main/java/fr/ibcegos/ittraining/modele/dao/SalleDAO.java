package fr.ibcegos.ittraining.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.objets.Salle;

public class SalleDAO {
	
	
	public List<Salle> findAll() {
		List<Salle> salles = new ArrayList<>();
		Salle salle = new Salle();

		// 1 - Se connecter � la BDD
		Connection cnx = PoolConnexion.getConnexion();
		// 2 - Fabrique la requete
		String query = "SELECT * FROM Salle";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				remplirSalle(salle, rs);
				salles.add(salle);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return salles;
	}

	


	public Salle findSalle(int id) {
		Salle salle = new Salle();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Salle WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next()) {
				remplirSalle(salle, rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return salle;
	}




	private void remplirSalle(Salle salle, ResultSet rs) throws SQLException {
		salle.setId(rs.getInt("id"));
		salle.setNom(rs.getString("nom"));
		salle.setAdresse(rs.getString("adresse"));
		salle.setCodePostal(rs.getString("code_postal"));
		salle.setVille(rs.getString("ville"));
	}




	public Salle findSalleByName(String nom) {
		Salle salle = new Salle();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Salle WHERE nom = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, nom);
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next()) {
				remplirSalle(salle, rs);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return salle;
	}

	
}
