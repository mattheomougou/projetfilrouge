package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.SessionManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.objets.Panier;
import fr.ibcegos.ittraining.modele.objets.Session;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/panier")
public class MonPanier extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/panier.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		String sessionId = request.getParameter("sessionId");
		if ("VALIDER".equals(action)) {

			Panier panier = (Panier) request.getSession().getAttribute("panier");
			Utilisateur utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");

			String entreprise = (String) request.getSession().getAttribute("entreprise");

			StagiaireManager stagiaireManager = new StagiaireManager();
			for (Session session : panier.getSessions()) {
				stagiaireManager.inscrireStagiaireAUneSession(utilisateurConnecte.getId(), session.getId(), entreprise);
			}
			request.getSession().setAttribute("panier", new Panier());

			this.getServletContext().getRequestDispatcher("/WEB-INF/confirmation.jsp").forward(request, response);
		} else if ("SUPPRIMER".equals(action) && sessionId != null) {
			Panier panier = (Panier) request.getSession().getAttribute("panier");
			panier.supprimerSession(Integer.valueOf(sessionId));
			request.getSession().setAttribute("panier", panier);
			this.getServletContext().getRequestDispatcher("/WEB-INF/panier.jsp").forward(request, response);
		} else {
			this.doGet(request, response);
		}
	}

}
