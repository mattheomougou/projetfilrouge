package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.EmployeManager;
import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.objets.Employe;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/gestionFormateur")
public class GestionFormateur extends HttpServlet {
	
	FormateurManager formateurManager = new FormateurManager();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Formateur> formateurs =formateurManager.findAll();
		request.setAttribute("formateurs", formateurs);
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionFormateur.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// r�cup�ration de donn�es
		int id = Integer.valueOf(request.getParameter("idFormateur"));
				
		formateurManager.supprimerByID(id);
		request.setAttribute("messageSucces","Le formateur a bien �t� supprim�");
		List<Formateur> formateurs =formateurManager.findAll();
		request.setAttribute("formateurs", formateurs);
				
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionFormateur.jsp").forward(request,response);
		
	}

}