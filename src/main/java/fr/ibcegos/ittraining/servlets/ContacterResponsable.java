package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.ContactManager;
import fr.ibcegos.ittraining.modele.objets.Contact;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/contact")
public class ContacterResponsable extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/contacterResponsable.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// r�cup�ration de donn�es
		String objet = request.getParameter("objet");
		String civilite = request.getParameter("civilite");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		String email = request.getParameter("email");
		String fonction = request.getParameter("fonction");
		String societe = request.getParameter("societe");
		String adresse = request.getParameter("adresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String message = request.getParameter("message");
		String reponse =String.valueOf( request.getParameter("reponse"));
		
		// si pas d'erreur alors on enregistre en BDD
		
		Contact contact = new Contact(objet,civilite,nom,prenom,telephone,email,fonction,societe,adresse,codePostal,ville,message,reponse);
		ContactManager contactManager = new ContactManager();
		contactManager.enregistrer(contact);
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
		.forward(request, response);
	}

}
