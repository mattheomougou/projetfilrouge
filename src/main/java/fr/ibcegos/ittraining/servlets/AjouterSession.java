package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.SessionManager;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Formation;
import fr.ibcegos.ittraining.modele.objets.Session;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/ajouterSession")
public class AjouterSession extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		FormationManager formationManager = new FormationManager();
		List<Formation> formations = formationManager.findAll();
		request.setAttribute("formations", formations);

		FormateurManager formateurManager = new FormateurManager();
		List<Formateur> formateurs = formateurManager.findAll();
		request.setAttribute("formateurs", formateurs);

		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterSession.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int formationId = Integer.valueOf(request.getParameter("formation"));

		String titre = request.getParameter("titre");
		String type = request.getParameter("type").toLowerCase();
		

		String adresse = request.getParameter("adresse");
		String ville = request.getParameter("ville");
		String codePostal = request.getParameter("codePostal");

		LocalDate dateDebut = LocalDate.parse(request.getParameter("dateDebut"));
		LocalDate dateFin = LocalDate.parse(request.getParameter("dateFin"));

		FormationManager formationManager = new FormationManager();
		Formation formation = formationManager.findById(formationId);

		Session session = new Session(titre, type, formation.getPrix(), dateDebut, dateFin, adresse, ville, codePostal, formation);
		session.setFormation(formation);

		SessionManager sessionManager = new SessionManager();
		sessionManager.enregistrer(session);

		request.setAttribute("messageSuccess", "La session a bien �t� planifi�e");
		doGet(request, response);

	}

	private List<Integer> stringsToInts(String[] ids) {
		List<Integer> list = new ArrayList<Integer>();

		if (ids != null) {
			for (String string : ids) {
				list.add(Integer.valueOf(string));
			}
		}
		return list;
	}

}
