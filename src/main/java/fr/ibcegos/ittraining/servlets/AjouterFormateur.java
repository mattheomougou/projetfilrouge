package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.managers.UtilisateurManager;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/ajouterFormateur")
public class AjouterFormateur extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterFormateur.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// r�cup�ration de donn�es
		LocalDate dateEmbauche =null;
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String motDePasse = request.getParameter("motDePasse");
		String confirmMotDePasse = request.getParameter("confirmMotDePasse");
		String telephone=request.getParameter("telephone");
		String adresse= request.getParameter("adresse");
		String codePostal= request.getParameter("codePostal");
		String ville= request.getParameter("ville");
		
		if(!request.getParameter("dateEmbauche").isEmpty()){
			dateEmbauche= Date.valueOf(request.getParameter("dateEmbauche")).toLocalDate();
		}
		
		
	
		
		// redirection
			FormateurManager formateurManager = new FormateurManager();
			UtilisateurManager utilisateurManager = new UtilisateurManager();
			Formateur formateur;
			Map<String,String> erreurs = utilisateurManager.verif(nom, prenom, mail,motDePasse,confirmMotDePasse,telephone);
			
			if(erreurs.isEmpty()) {
			// si pas d'erreur alors on enregistre en BDD
			if(dateEmbauche == null) {
				formateur = new Formateur(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
			}else {
				formateur = new Formateur(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal, dateEmbauche);
				
			}
			formateurManager.enregistrer(formateur);
			request.setAttribute("messageSucces", "Le formateur a bien �t� ajout�");
			this.getServletContext().getRequestDispatcher("/WEB-INF/administration.jsp")
			.forward(request, response);
			}else {
				request.setAttribute("erreurs", erreurs);
				this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterFormateur.jsp").forward(request, response);
				
			}
	

	}

}
