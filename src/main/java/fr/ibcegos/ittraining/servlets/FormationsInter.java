package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.objets.Formation;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/formationsInter")
public class FormationsInter extends HttpServlet {
	
	FormationManager formationManager = new FormationManager();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Formation> formations = formationManager.findAll();
		request.setAttribute("formations", formations);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/formationsInter.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
