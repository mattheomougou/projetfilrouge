package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/gestionStagiaire")
public class GestionStagiaire extends HttpServlet {

	StagiaireManager stagiaireManager = new StagiaireManager();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Stagiaire> stagiaires = stagiaireManager.findAll();
		request.setAttribute("stagiaires", stagiaires);
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionStagiaire.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// r�cup�ration de donn�es
		int id = Integer.valueOf(request.getParameter("idStagiaire"));
		stagiaireManager.supprimerByID(id);
		request.setAttribute("messageSucces", "Le formateur a bien �t� supprim�");
		List<Stagiaire> stagiaires = stagiaireManager.findAll();
		request.setAttribute("stagiaires", stagiaires);

		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionStagiaire.jsp").forward(request, response);

	}

}