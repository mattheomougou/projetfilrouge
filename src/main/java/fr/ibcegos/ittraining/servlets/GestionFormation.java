package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.SessionManager;
import fr.ibcegos.ittraining.modele.objets.Formation;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/gestionFormation")
public class GestionFormation extends HttpServlet {

	FormationManager formationManager = new FormationManager();
	SessionManager sessionManager = new SessionManager();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Formation> formations = formationManager.findAll();
		Map<Integer,Integer> nombreInscritSession = sessionManager.findAllInscrit();
		//System.out.println(nombreInscritSession);
		request.setAttribute("nombreInscritSession", nombreInscritSession);
		request.setAttribute("formations", formations);
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionFormations.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// r�cup�ration de donn�es
		int id = Integer.valueOf(request.getParameter("idFormation"));

		try {
			formationManager.supprimerByID(id);
			request.setAttribute("messageSuccess", "La formation a bien �t� supprim�e");
		} catch (Exception ex) {
			request.setAttribute("messageError", "La formation n'a pas pu �tre supprim�e. V�rifiez qu'elle n'a aucune session encours");
		}
		List<Formation> formations = formationManager.findAll();
		request.setAttribute("formations", formations);
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionFormations.jsp").forward(request, response);

	}

}