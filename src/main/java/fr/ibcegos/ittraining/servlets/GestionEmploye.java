package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.EmployeManager;
import fr.ibcegos.ittraining.modele.objets.Employe;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/gestionEmploye")
public class GestionEmploye extends HttpServlet {
	
	EmployeManager employeManager = new EmployeManager();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Employe> employes =employeManager.findAll();
		request.setAttribute("employes", employes);
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionEmploye.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// r�cup�ration de donn�es
		int id = Integer.valueOf(request.getParameter("idEmploye"));
		employeManager.supprimerByID(id);
		request.setAttribute("messageSucces","L'employ� a bien �t� supprim�");
		List<Employe> employes =employeManager.findAll();
		request.setAttribute("employes", employes);
				
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionEmploye.jsp").forward(request,response);
		
	}

}