package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.SessionManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.objets.InfoSessionInscrit;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/connect/espaceClient")
public class EspaceClient extends HttpServlet {
	
	StagiaireManager stagiaireManager = new StagiaireManager();
	SessionManager sessionManager = new SessionManager();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur utilisateur=(Utilisateur)(request.getSession().getAttribute("utilisateur"));
		int id =utilisateur.getId();
		SessionManager sessionManager = new SessionManager();
		List <InfoSessionInscrit> sessions = sessionManager.findAllSessionOfStagiaire(id);
		request.getSession().setAttribute("sessions", sessions);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/espaceClient.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		// r�cup�ration de donn�es
		int id = Integer.valueOf(request.getParameter("idUser"));
		Stagiaire stagiaire = stagiaireManager.findByID(id);
		request.setAttribute("stagaiare", stagiaire);	
		*/
		this.getServletContext().getRequestDispatcher("/WEB-INF/espaceClient.jsp").forward(request,response);
		
	}

}