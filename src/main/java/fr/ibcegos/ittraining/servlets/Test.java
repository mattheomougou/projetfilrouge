package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.TestManager;
import fr.ibcegos.ittraining.modele.objets.TestInscription;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/test")
public class Test extends HttpServlet {


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().setAttribute("previousURL", request.getHeader("referer"));
		request.getSession().setAttribute("formationID", Integer.valueOf(request.getParameter("formationID")));
		this.getServletContext().getRequestDispatcher("/WEB-INF/test.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<Integer,String> reponseStagiaire =new HashMap<Integer, String>();
		reponseStagiaire.put(1, request.getParameter("un"));
		reponseStagiaire.put(2, request.getParameter("deux"));
		reponseStagiaire.put(3, request.getParameter("trois"));
		reponseStagiaire.put(4, request.getParameter("quatre"));
		reponseStagiaire.put(5, request.getParameter("cinq"));
		reponseStagiaire.put(6, request.getParameter("six"));
		reponseStagiaire.put(7, request.getParameter("sept"));
		reponseStagiaire.put(8, request.getParameter("huit"));
		reponseStagiaire.put(9, request.getParameter("neuf"));
		reponseStagiaire.put(10, request.getParameter("dix"));
		reponseStagiaire.put(11, request.getParameter("onze"));
		reponseStagiaire.put(12, request.getParameter("douze"));
		reponseStagiaire.put(13, request.getParameter("treize"));
		reponseStagiaire.put(14, request.getParameter("quatorze"));
		reponseStagiaire.put(15, request.getParameter("quinze"));
		reponseStagiaire.put(16, request.getParameter("seize"));
		reponseStagiaire.put(17, request.getParameter("dix_sept"));
		reponseStagiaire.put(18, request.getParameter("dix_huit"));
		reponseStagiaire.put(19, request.getParameter("dix_neuf"));
		reponseStagiaire.put(20, request.getParameter("vingt"));
		TestManager testManager = new TestManager();
		FormationManager formationManager= new FormationManager();
		int noteStagiaire =testManager.getNoteStagiaire(reponseStagiaire);
		int idFormation = (int)(request.getSession().getAttribute("formationID"));
		int idUtilisateur = ((Utilisateur)(request.getSession().getAttribute("utilisateur"))).getId();
		TestInscription test= new TestInscription(noteStagiaire,idFormation,idUtilisateur);
		testManager.enregistrer(test);
		int niveauExige =formationManager.getNiveauExige((Integer)(request.getSession().getAttribute("formationID")));
		request.getSession().removeAttribute("formationID");
		
		testManager.verifNote(request, noteStagiaire, niveauExige, test.getDateDePassageDuTest());
		
		String nexturl = (String) request.getSession().getAttribute("nextURL");
		if (nexturl != null) {
			request.getSession().removeAttribute("nextURL");
			this.getServletContext().getRequestDispatcher(nexturl).forward(request, response);
		} else {
			response.sendRedirect((String) request.getSession().getAttribute("previousURL"));
		}
	}

}
