package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.ModuleManager;
import fr.ibcegos.ittraining.modele.objets.Formation;
import fr.ibcegos.ittraining.modele.objets.Module;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/chercherFormations")
public class ChercherFormations extends HttpServlet {

	FormationManager formationManager = new FormationManager();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String q = request.getParameter("q");

		List<Formation> formations = formationManager.searchFormations(q);
		request.setAttribute("formations", formations);

		this.getServletContext().getRequestDispatcher("/WEB-INF/listerFormations.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		this.doGet(request, response);
	}

}