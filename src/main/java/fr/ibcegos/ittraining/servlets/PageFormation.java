package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;


import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.objets.Formation;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/pageFormation")
public class PageFormation extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 int formationId= Integer.valueOf(request.getParameter("id"));
		 FormationManager formationManager = new FormationManager();
		 Formation formation = formationManager.findById(formationId);
		 request.getSession().setAttribute("formation", formation);
		 this.getServletContext().getRequestDispatcher("/WEB-INF/pageFormation.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/pageFormation.jsp")
		.forward(request, response);
	}
	
}