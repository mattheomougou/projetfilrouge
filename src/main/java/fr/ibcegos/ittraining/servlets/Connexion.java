package fr.ibcegos.ittraining.servlets;

import java.io.IOException;

import fr.ibcegos.ittraining.modele.managers.EmployeManager;
import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.managers.UtilisateurManager;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/connexion")
public class Connexion extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getSession().setAttribute("previousURL", request.getHeader("referer"));
		this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EmployeManager employeManager = new EmployeManager();
		StagiaireManager stagiaireManager = new StagiaireManager();
		FormateurManager formateurManager = new FormateurManager();
		FormationManager formationManager = new FormationManager();
		// récupération des paramètres
		String mail = request.getParameter("mail");
		String motDePasse = request.getParameter("motDePasse");

		// appel au manager
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		Utilisateur utilisateur = utilisateurManager.findByMailAndMotDePasse(mail, motDePasse);

		// redirection en fonction du résultat

		if (utilisateur == null) {
			request.setAttribute("messageErreur", "Email ou mot de passe invalide");
			this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
		} else {
			// on stocke l'attribut dans la session
			request.getSession().setAttribute("utilisateur", utilisateur);
			if(utilisateur.isAdministrateur()) {
				request.getSession().setAttribute("administrateur", true);
				int nombreEmploye =employeManager.getNombreEmploye();
				int nombreStagiaire =stagiaireManager.getNombreStagiaire();
				int nombreFormateur =formateurManager.getNombreFormateur();
				int nombreFormation =formationManager.getNombreFormation();
				request.getSession().setAttribute("nombreEmploye", nombreEmploye);
				request.getSession().setAttribute("nombreStagiaire", nombreStagiaire);
				request.getSession().setAttribute("nombreFormateur", nombreFormateur);
				request.getSession().setAttribute("nombreFormation", nombreFormation);
				
				this.getServletContext().getRequestDispatcher("/WEB-INF/administration.jsp")
				.forward(request, response);
			}else {
				String nexturl = (String) request.getSession().getAttribute("nextURL");
				String previousurl = (String) request.getSession().getAttribute("previousURL");
				if (previousurl != null) {
					response.sendRedirect((String) request.getSession().getAttribute("previousURL"));
				} else if(nexturl != null){
					request.getSession().removeAttribute("nextURL");
					this.getServletContext().getRequestDispatcher(nexturl).forward(request, response);	
				}else {
					this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
					.forward(request, response);
				}
			}		
		}
	}

}
