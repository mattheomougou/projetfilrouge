package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.ModuleManager;
import fr.ibcegos.ittraining.modele.managers.ThemeManager;
import fr.ibcegos.ittraining.modele.objets.Module;
import fr.ibcegos.ittraining.modele.objets.Theme;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/ajouterModule")
public class AjouterModule extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ThemeManager themeManager = new ThemeManager();
		List<Theme> themes = themeManager.findAllThemes();
		request.setAttribute("themes", themes);
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterModule.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		

		String titre = request.getParameter("titre");
		int duree = Integer.valueOf(request.getParameter("duree"));
		String description = request.getParameter("description");
		String themeId = request.getParameter("theme");
		Theme theme = getTheme(themeId);

		Module module = new Module(titre, theme, duree, description);

		ModuleManager moduleManager = new ModuleManager();
		moduleManager.enregistrer(module);
		request.setAttribute("success", true);
		doGet(request, response);

	}

	private Theme getTheme(String themeId) {
		try {
			ThemeManager themeManager = new ThemeManager();
			int id = Integer.valueOf(themeId);
			Theme theme = themeManager.getThemeById(id);
			return theme;
		} catch (NumberFormatException ex) {
			return null;
		}
	}
}
