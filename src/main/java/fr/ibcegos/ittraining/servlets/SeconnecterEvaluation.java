package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.EvaluationManager;
import fr.ibcegos.ittraining.modele.objets.Evaluation;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/connexionEvaluation")
public class SeconnecterEvaluation extends HttpServlet {
	
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/connexionEvaluation.jsp")
		.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// r�cup�ration de donn�es de connection
		String numeroSession = request.getParameter("numeroSession");
		LocalDate dateSession = LocalDate.parse(request.getParameter("dateSession"));
        
	//titit
		//request.setAttribute("messageSucces", "Votre �valuation a bien �t� enregistr�e");
			this.getServletContext().getRequestDispatcher("/WEB-INF/evaluation.jsp")
			.forward(request, response);
		/*} else {
			request.setAttribute("erreurs", erreurs);
			this.getServletContext().getRequestDispatcher("/WEB-INF/evaluation.jsp")
			.forward(request, response);*/
		
		}
	}


