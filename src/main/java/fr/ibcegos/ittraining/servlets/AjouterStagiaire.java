package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.managers.UtilisateurManager;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/ajouterStagiaire")
public class AjouterStagiaire extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterStagiaire.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// r�cup�ration de donn�es
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String motDePasse = request.getParameter("motDePasse");
		String confirmMotDePasse = request.getParameter("confirmMotDePasse");
		String telephone = request.getParameter("telephone");
		String adresse = request.getParameter("adresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String entreprise = request.getParameter("entreprise");

		// redirection
		StagiaireManager stagiaireManager = new StagiaireManager();
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		Stagiaire stagiaire;
		Map<String, String> erreurs = utilisateurManager.verif(nom, prenom, mail, motDePasse, confirmMotDePasse,
				telephone);

		if (erreurs.isEmpty()) {
			// si pas d'erreur alors on enregistre en BDD
			if (entreprise == null) {
				stagiaire = new Stagiaire(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal);
			} else {
				stagiaire = new Stagiaire(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal,
						entreprise);

			}
			Utilisateur utilisateur = utilisateurManager.enregistrer(stagiaire);
			stagiaireManager.enregistrerAdmin(stagiaire, utilisateur.getId());
			request.setAttribute("messageSuccess", "Le stagiiare a bien �t� ajout�");
			this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterStagiaire.jsp").forward(request, response);
		} else {
			request.setAttribute("messageErrors", erreurs);
			this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterStagiaire.jsp").forward(request, response);

		}
	}
}
