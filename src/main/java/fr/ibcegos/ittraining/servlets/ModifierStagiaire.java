package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.managers.UtilisateurManager;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/modifierStagiaire")
public class ModifierStagiaire extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/modifierStagiaire.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// r�cup�ration de donn�es
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String motDePasse = request.getParameter("motDePasse");
		String telephone = request.getParameter("telephone");
		String adresse = request.getParameter("adresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String entreprise = request.getParameter("entreprise");
		StagiaireManager stagiaireManager = new StagiaireManager();
		UtilisateurManager utilisateurManager = new UtilisateurManager();

		Map<String, String> erreurs = utilisateurManager.verifier(nom, prenom, telephone);
		Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
		
		if (erreurs.isEmpty()) {
			utilisateur.setNom(nom);
			utilisateur.setPrenom(prenom);
			utilisateur.setTelephone(telephone);
			utilisateur.setAdresse(adresse);
			utilisateur.setCodePostal(codePostal);
			utilisateur.setVille(ville);
						
			utilisateur = utilisateurManager.modifier(utilisateur);
			System.out.println(" apr�s modification " + utilisateur);
			// stagiaireManager.modifier(stagiaire, utilisateur.getId());
			request.setAttribute("messageSuccess", "Les modifications ont bien �t� ajout�");
			this.getServletContext().getRequestDispatcher("/WEB-INF/espaceClientInfos.jsp").forward(request, response);
		} else {
			request.setAttribute("messageErrors", erreurs);
			this.getServletContext().getRequestDispatcher("/WEB-INF/modifierStagiaire.jsp").forward(request, response);

		}

		System.out.println("Errors" + erreurs);
	}
}
