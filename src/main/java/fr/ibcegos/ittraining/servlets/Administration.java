package fr.ibcegos.ittraining.servlets;

import java.io.IOException;

import fr.ibcegos.ittraining.modele.managers.EmployeManager;
import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/administration")
public class Administration extends HttpServlet {
	EmployeManager employeManager = new EmployeManager();
	StagiaireManager stagiaireManager = new StagiaireManager();
	FormateurManager formateurManager = new FormateurManager();
	FormationManager formationManager = new FormationManager();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int nombreEmploye =employeManager.getNombreEmploye();
		int nombreStagiaire =stagiaireManager.getNombreStagiaire();
		int nombreFormateur =formateurManager.getNombreFormateur();
		int nombreFormation =formationManager.getNombreFormation();
		request.getSession().setAttribute("nombreEmploye", nombreEmploye);
		request.getSession().setAttribute("nombreStagiaire", nombreStagiaire);
		request.getSession().setAttribute("nombreFormateur", nombreFormateur);
		request.getSession().setAttribute("nombreFormation", nombreFormation);
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/administration.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}

