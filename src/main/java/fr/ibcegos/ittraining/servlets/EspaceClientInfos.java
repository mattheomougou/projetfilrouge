package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.SessionManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.objets.InfoSessionInscrit;
import fr.ibcegos.ittraining.modele.objets.Stagiaire;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/connect/espaceClientInfos")
public class EspaceClientInfos extends HttpServlet {
	
	StagiaireManager stagiaireManager = new StagiaireManager();
	SessionManager sessionManager = new SessionManager();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		this.getServletContext().getRequestDispatcher("/WEB-INF/espaceClientInfos.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int id = Integer.valueOf(request.getParameter("idUser"));
		Stagiaire stagiaire = stagiaireManager.findByID(id);
		request.setAttribute("stagaiare", stagiaire);	
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/espaceClientInfos.jsp").forward(request,response);
		
	}

}