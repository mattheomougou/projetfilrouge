package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.EvaluationManager;
import fr.ibcegos.ittraining.modele.objets.Evaluation;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/evaluation")
public class AjouterEvaluation extends HttpServlet {
	
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/connexionEvaluation.jsp")
		.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// récupération de données de connection
				
		boolean isRecommandation = Boolean.valueOf(request.getParameter("isRecommandation"));
		int accueil = Integer.valueOf(request.getParameter("accueil"));
		int environnement = Integer.valueOf(request.getParameter("environnement"));
		int contenu = Integer.valueOf(request.getParameter("contenu"));
		int satisfaction = Integer.valueOf(request.getParameter("satisfaction"));
		int formateur = Integer.valueOf(request.getParameter("formateur"));
		int disponibilité = Integer.valueOf(request.getParameter("disponibilité"));
		int maîtriseDomaine = Integer.valueOf(request.getParameter("maîtriseDomaine"));
		int pedagogie = Integer.valueOf(request.getParameter("pedagogie"));
		int reponsesQuestion = Integer.valueOf(request.getParameter("reponsesQuestion"));
		int animation = Integer.valueOf(request.getParameter("animation"));
		boolean isProjetFormation = Boolean.valueOf(request.getParameter("isProjetFormation"));
		String commentaires_projet_formation = request.getParameter("commentaires_projet_formation");
		
		// encapsulation de la verification dans un manager
		EvaluationManager evaluationManager = new EvaluationManager();
		Map<String,String> erreurs = evaluationManager.verif(isRecommandation,accueil,environnement,contenu,satisfaction,formateur,disponibilité,
		maîtriseDomaine,pedagogie,reponsesQuestion,animation, isProjetFormation,commentaires_projet_formation);
		
		// redirection
		//if(erreurs.isEmpty()) {
		// si pas d'erreur alors on enregistre en BDD
		Evaluation evaluation = new Evaluation(isRecommandation, accueil,environnement,
		contenu,satisfaction,formateur,disponibilité,maîtriseDomaine,
		pedagogie,reponsesQuestion,animation, isProjetFormation,commentaires_projet_formation);
		
		evaluationManager.enregistrer(evaluation);
			request.setAttribute("messageSucces", "Votre évaluation a bien été enregistrée");
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
			.forward(request, response);
		/*} else {
			request.setAttribute("erreurs", erreurs);
			this.getServletContext().getRequestDispatcher("/WEB-INF/evaluation.jsp")
			.forward(request, response);*/
		
		}
	}


