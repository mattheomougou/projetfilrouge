package fr.ibcegos.ittraining.servlets;

import java.io.IOException;

import fr.ibcegos.ittraining.modele.managers.FormateurManager;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/informationFormateur")
public class InformationFormateur extends HttpServlet {
	
	FormateurManager formateurManager = new FormateurManager();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionFormateur.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// r�cup�ration de donn�es
		int id = Integer.valueOf(request.getParameter("idFormateur"));
				
		
		Formateur formateur =formateurManager.findByID(id);
		request.setAttribute("formateur", formateur);	
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/informationFormateur.jsp").forward(request,response);
		
	}

}