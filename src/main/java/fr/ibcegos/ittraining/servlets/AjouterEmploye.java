package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Map;

import fr.ibcegos.ittraining.modele.managers.EmployeManager;
import fr.ibcegos.ittraining.modele.managers.UtilisateurManager;
import fr.ibcegos.ittraining.modele.objets.Employe;
import fr.ibcegos.ittraining.modele.objets.Formateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/ajouterEmploye")
public class AjouterEmploye extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterEmploye.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// r�cup�ration de donn�es
		LocalDate dateEmbauche=null;
		String nom = request.getParameter("nom");
		String prenom =request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String motDePasse = request.getParameter("motDePasse");
		String confirmMotDePasse = request.getParameter("confirmMotDePasse");
		String fonction = request.getParameter("fonction");
		double salaire = Double.valueOf(request.getParameter("salaire"));
		String adresse = request.getParameter("adresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String telephone = request.getParameter("telephone");
		boolean responsablePlanning = Boolean.valueOf(request.getParameter("responsablePlanning")).booleanValue();
		//System.out.println(responsablePlanning);
		if(!request.getParameter("dateEmbauche").isEmpty()){
			dateEmbauche = Date.valueOf(request.getParameter("dateEmbauche")).toLocalDate();
		}
		
	
		
		// redirection
			EmployeManager employeManager = new EmployeManager();
			UtilisateurManager utilisateurManager = new UtilisateurManager();
			Employe employe;
			Map<String,String> erreursUtilisateur = utilisateurManager.verif(nom, prenom, mail,motDePasse,confirmMotDePasse,telephone);
			Map<String, String> erreursEmploye=employeManager.verifEmploye(salaire);
			// si pas d'erreur alors on enregistre en BDD
			if(erreursUtilisateur.isEmpty() && erreursEmploye.isEmpty()) {
				if(dateEmbauche == null) {
					employe = new Employe(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal, fonction, salaire, responsablePlanning);
				}else {
					employe = new Employe(nom, prenom, mail, motDePasse, telephone, adresse, ville, codePostal, fonction, salaire, responsablePlanning, dateEmbauche);	
				}
			employeManager.enregistrer(employe);
			request.setAttribute("messageSucces", "L'employ� a bien �t� ajout�");
			this.getServletContext().getRequestDispatcher("/WEB-INF/administration.jsp")
			.forward(request, response);
			}else {
				request.setAttribute("erreursUtilisateur", erreursUtilisateur);
				request.setAttribute("erreursEmploye", erreursEmploye);
				this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterEmploye.jsp").forward(request, response);
			}
	

	}

}
