package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.EmployeManager;
import fr.ibcegos.ittraining.modele.objets.Employe;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/informationEmploye")
public class InformationEmploye extends HttpServlet {
	
	EmployeManager employeManager = new EmployeManager();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/gestionEmploye.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// r�cup�ration de donn�es
		int id = Integer.valueOf(request.getParameter("idEmploye"));
				
		EmployeManager employeManager = new EmployeManager();
		Employe employe =employeManager.findByID(id);
		request.setAttribute("employe", employe);	
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/informationEmploye.jsp").forward(request,response);
		
	}

}