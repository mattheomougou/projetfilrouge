package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.DomaineManager;
import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.ModuleManager;
import fr.ibcegos.ittraining.modele.objets.Domaine;
import fr.ibcegos.ittraining.modele.objets.Formation;
import fr.ibcegos.ittraining.modele.objets.Module;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/ajouterFormation")
public class AjouterFormation extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DomaineManager domaineManager = new DomaineManager();
		List<Domaine> domaines = domaineManager.findAllDomaine();
		request.setAttribute("domaines", domaines);

		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = moduleManager.findAllModules();
		request.setAttribute("modules", modules);

		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterFormation.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String titre = request.getParameter("titre");
		String description = request.getParameter("description");
		String objectifs = request.getParameter("objectifs");
		String prerequis = request.getParameter("prerequis");
		int niveauExige = Integer.valueOf(request.getParameter("niveauExige"));
		double prix = Double.valueOf(request.getParameter("prix"));
		int domaineId = Integer.valueOf(request.getParameter("domaine"));
		List<Integer> modulesIds = stringsToInts(request.getParameterValues("modules"));

		FormationManager formationManager = new FormationManager();
		Utilisateur responsableFormation = (Utilisateur) request.getSession().getAttribute("utilisateur");
		DomaineManager domaineManager = new DomaineManager();
		Domaine domaine = domaineManager.findDomaineById(domaineId);
		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = moduleManager.getModules(modulesIds);

		Formation formation = new Formation(titre, niveauExige, domaine, description, prerequis, objectifs, modules,
				prix, responsableFormation);
		formationManager.enregistrer(formation);

		doGet(request, response);

	}

	private List<Integer> stringsToInts(String[] ids) {
		List<Integer> list = new ArrayList<Integer>();

		if (ids != null) {
			for (String string : ids) {
				list.add(Integer.valueOf(string));
			}
		}
		return list;
	}

}