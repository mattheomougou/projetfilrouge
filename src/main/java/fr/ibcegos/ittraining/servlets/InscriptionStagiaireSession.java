package fr.ibcegos.ittraining.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.ittraining.modele.managers.FormationManager;
import fr.ibcegos.ittraining.modele.managers.SessionManager;
import fr.ibcegos.ittraining.modele.managers.StagiaireManager;
import fr.ibcegos.ittraining.modele.managers.TestManager;
import fr.ibcegos.ittraining.modele.objets.Formation;
import fr.ibcegos.ittraining.modele.objets.Panier;
import fr.ibcegos.ittraining.modele.objets.Session;
import fr.ibcegos.ittraining.modele.objets.TestInscription;
import fr.ibcegos.ittraining.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/inscriptionStagiaireSession")
public class InscriptionStagiaireSession extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Utilisateur utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");
		TestManager testManager = new TestManager();
		FormationManager formationManager = new FormationManager();
		if (utilisateurConnecte == null) {
			request.setAttribute("messageErreur", "merci de bien vouloir vous connecter!");

			request.getSession().setAttribute("nextURL", "/WEB-INF/inscriptionStagiaireSession.jsp");

			this.getServletContext().getRequestDispatcher("/connexion").forward(request, response);

		} else {

			Formation formation = (Formation) request.getSession().getAttribute("formation");
			TestInscription test = testManager.findTestByUtilisateurAndFormation(
					((Utilisateur) request.getSession().getAttribute("utilisateur")).getId(), formation.getId());
			int niveauExige = formationManager.getNiveauExige(formation.getId());
			if (test != null) {
				testManager.verifNote(request, test.getNote(), niveauExige, test.getDateDePassageDuTest());

			} else {
				request.getSession().setAttribute("test", "testNonPasse");
			}

			this.getServletContext().getRequestDispatcher("/WEB-INF/inscriptionStagiaireSession.jsp").forward(request,
					response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Utilisateur utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");

		// redirection en fonction du r�sultat

		String entreprise = request.getParameter("entreprise");
		request.getSession().setAttribute("entreprise", entreprise);
		int sessionId = Integer.valueOf(request.getParameter("sessionId"));

		StagiaireManager stagiaireManager = new StagiaireManager();
        
		request.setAttribute("messageSuccess",
				"Votre inscription � la session de cette formation a �t� effectu�e avec succ�s");
		SessionManager sessionManager = new SessionManager();
		Session session = sessionManager.findSessionById(sessionId);
		System.out.println("Session" + session);
		//request.getSession().setAttribute("session", sessionId);
		
		Panier panier = (Panier) request.getSession().getAttribute("panier");

		if (panier == null) {
			panier = new Panier();
		}

		panier.ajouterSession(session);

		request.getSession().setAttribute("panier", panier);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/panier.jsp").forward(request, response);

	}

	private List<Integer> stringsToInts(String[] ids) {
		List<Integer> list = new ArrayList<Integer>();
		if (ids != null) {
			for (String string : ids) {
				list.add(Integer.valueOf(string));
			}
		}
		return list;
	}

}