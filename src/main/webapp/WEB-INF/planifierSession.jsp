<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Plannifier une nouvelle session</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script src="https://kit.fontawesome.com/25ea7e5f39.js"
	crossorigin="anonymous"></script>

<link href="ajouterFormation.css" rel="stylesheet">
<link href="general.css" rel="stylesheet">


<title>Ajouter une session</title>
</head>
<body>
	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1 vh-100">
			<%@ include file="header.jspf"%>
			<div class="container main-content p-4">

				<div class="m-5">
					<h1>Plannifier une nouvelle Session</h1>
				</div>
				<div class="m-5">
					<%@ include file="messages.jspf"%>
				</div>

				<div class="m-5 col-md-6">
					<form action="ajouterSession?formationId=${param.formationId}"
						method="post">
						<fieldset>
							<legend>Informations de la Session</legend>


							<div class="mb-3 mt-2 ">

								<c:if test="${param.formationId != null}">
									<c:forEach items="${formations}" var="formation">
										<c:if test="${param.formationId == formation.id}">
											<input type="hidden" name="formation"
												value="${formation.id }">
											<div>Formation : ${formation.titre}</div>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${param.formationId == null}">
									<div>
										<label for="formation">la formation</label>
									</div>
									<div class="form-group">
										<select class="form-select" name="formation" id="formation">

											<option value="" disabled selected>Sélectionner une
												formation</option>
											<c:forEach items="${formations}" var="formation">
												<option name="choixFormation" value="${formation.id}">${formation.titre}</option>
											</c:forEach>
										</select>
									</div>
								</c:if>


							</div>


							<div class="mb-3">
								<label for="titre">Nom</label> <input name="titre"
									class="form-control" id="titre" placeholder="Nommer la session" />
							</div>

							<div class="mb-5">
								<label for="type">Type</label>

								<div class="form-check">
									<input class="form-check-input" type="radio" name="type"
										id="INTRA" value="INTRA"> <label
										class="form-check-label" for="INTRA">Intra </label>
								</div>


								<div class="form-check">
									<input class="form-check-input" type="radio" name="type"
										id="INTER" value="INTER" checked> <label
										class="form-check-label" for="INTER"> Inter </label>
								</div>


							</div>
						</fieldset>


						<fieldset>
							<legend>Lieu de la Session</legend>


							<div class="mb-3">
								<label for="adresse">Adresse</label> <input name="adresse"
									class="form-control" id="adresse"
									placeholder="Saisir l'adresse de cette session" />
							</div>
							<div class="mb-3">
								<label for="ville">Ville</label> <input name="ville"
									class="form-control" id="ville" placeholder="Saisir la ville" />
							</div>
							<div class="mb-5">
								<label for="codePostal">Code postal</label> <input
									name=codePostal class="form-control" id="codePostal"
									placeholder="Saisir le code postal" />
							</div>

						</fieldset>


						<fieldset>
							<legend>Dates de la Session</legend>
							<div class="mb-3">
								<label for="dateDebut">Date de Début </label> <input type="date"
									class="form-control" id="dateDebut" name="dateDebut">
							</div>

							<div class="mb-3">
								<label for="dateFin">Date de Fin </label> <input type="date"
									class="form-control" id="dateFin" name="dateFin">
							</div>

						</fieldset>

						<div class="mb-5">
							<button type="submit" class="btn btn-primary btn-lg btn-block"">Ajouter</button>
						</div>
					</form>
				</div>



			</div>
		</div>
	</div>



	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>
</body>

</html>