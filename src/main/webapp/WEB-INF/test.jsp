<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
<link href="test.css" rel="stylesheet">
<title>Test</title>

<%@ include file="header.jspf" %>      
</head>

<body>

<div class = "m-5">
<h1>Test de Prérequis à la Formation</h1>


<p> Nous allons maintenant vérifier vos prérequis avant de vous inscrire à la session
 demandée. Ce test est composé de 20 questions.<br> La durée totale est estimée à 30 minutes.
 Merci de prévoir 30 minutes consécutives pour faire ce test.</p>

<form action="test" method="post">

<h4>Java</h4>
<p>
	1. Lequel des éléments suivants n’est pas un concept POO en Java?
	<div><input type="radio"  id="" name="un" value="A"><label for="1">A. Héritage</label></div>
	<div><input type="radio"  id="" name="un" value="B"><label for="1"> B. Encapsulation</label></div>
	<div><input type="radio"  id="" name="un" value="C" checked><label for="1"> C. Compilation</label></div>
<p>
<p>
	2. Quel concept de Java est utilisé en combinant des méthodes et des attributs dans une classe?
	<div><input type="radio"  id="un" name="deux" value="A"><label for="2">A. Polymorphisme </label></div>
	<div><input type="radio"  id="" name="deux" value="B" checked><label for="2">B. Encapsulation </label></div>
	<div><input type="radio"  id="" name="deux" value="C"><label for="2">C. Héritage </label></div>
<p>
<p>
	3. Quand la surcharge de méthode est-elle déterminée?
	<div><input type="radio"  id="trois" name="trois" value="A" checked><label for="">A. Au moment de l’exécution </label></div>
	<div><input type="radio"  id="" name="trois" value="B" checked><label for="">B. Au moment de la compilation </label></div>
	<div><input type="radio"  id="" name="trois" value="C"><label for="3">C. Au moment du codage </label></div>
<p>
<p>
	4. Quel concept de Java est un moyen de convertir des objets du monde réel en termes de classe?
	<div><input selected type="radio"  id="quatre" name="quatre" value="A" checked><label for="" >A. Abstraction </label></div>
	<div><input type="radio"  id="quatre" name="quatre" value="B" ><label for="">B. Encapsulation </label></div>
	<div><input type="radio"  id="quatre" name="quatre" value="C"><label for="">C. Héritage </label></div>
<p>
<p>
	5. Quel mot clé est utilisé pour déclarer une interface en Java?
	<div><input type="radio"  id="" name="cinq" value="A"><label for="">A.	class </label></div>
	<div><input type="radio"  id="" name="cinq" value="B" checked><label for="">B.	interface </label></div>
	<div><input type="radio"  id="" name="cinq" value="C"><label for="">C.	implements </label></div>
<p>


<h4>HTML / CSS</h4>
<p>
	6. La première page d’un site Web s’appelle _____.
	<div><input type="radio"  id="" name="six" value="A"><label for=""> A.	Page de conception</label></div>
	<div><input type="radio"  id="" name="six" value="B" checked><label for=""> B.	Page d’accueil</label></div>
	<div><input type="radio"  id="" name="six" value="C"><label for=""> C.	Page principale </label></div>
<p>
<p>
	7. Quelle est la syntaxe correcte du code CSS suivant?
	<div><input type="radio"  id="" name="sept" value="A"><label for="" selected>A.	Body:color=black </label></div>
	<div><input type="radio"  id="" name="sept" value="B"><label for="">B.	{body:color=black(body} </label></div>
	<div><input type="radio"  id="" name="sept" value="C" checked><label for="">C.	Body {color: Black} </label></div>
<p>
<p>
	8. La balise Head est utilisée pour?
	<div><input type="radio"  id="" name="huit" value="A" checked><label for="" selected>A.	Écrire des styles CSS</label></div>
	<div><input type="radio"  id="" name="huit" value="B"><label for="">B.	Écrire du Javascript </label></div>
	<div><input type="radio"  id="" name="huit" value="C" checked><label for="">C.	Toutes les réponses sont vrais </label></div>
<p>
<p>
	9. CSS est un acronyme pour _____
	<div><input type="radio"  id="" name="neuf" value="A" checked><label for="" selected>A. Cascading Style Sheet </label></div>
	<div><input type="radio"  id="" name="neuf" value="B" ><label for="">B. Costume Style Sheet </label></div>
	<div><input type="radio"  id="" name="neuf" value="C"><label for="">C.	Cascading System Style </label></div>
<p>
<p>
	10. Quel attribut peut être ajouté à de nombreux éléments HTML / XHTML pour les identifier en tant que membre d’un groupe spécifique?
	<div><input type="radio"  id="" name="dix" value="A"><label for="">A.	Id </label></div>
	<div><input type="radio"  id="" name="dix" value="B" checked><label for="" selected>B.	class </label></div>
	<div><input type="radio"  id="" name="dix" value="C" ><label for="">C.	div </label></div>
<p>
<h4>Javascript</h4>
<p>
	11. Dans quel balise HTML plaçons-nous le code JavaScript?
	<div><input type="radio"  id="" name="onze" value="A"><label for="">A. La balise js </label></div>
	<div><input type="radio"  id="" name="onze" value="B"><label for="">B. La balise javascript </label></div>
	<div><input type="radio"  id="" name="onze" value="C" checked><label for="" selected>C. La balise script </label></div>
<p>
<p>
	12. Quel est le bon endroit pour insérer un code JavaScript?
	<div><input type="radio"  id="" name="douze" value="A"><label for=""selected>A. La section head </label></div>
	<div><input type="radio"  id="" name="douze" value="B"><label for="">B. La section body </label></div>
	<div><input type="radio"  id="" name="douze" value="C"><label for="">C. Aucune de ces réponses n’est vraie. </label></div>
<p>
<p>
	13. Comment créer une fonction en JavaScript?
	<div><input type="radio"  id="" name="treize" value="A"><label for="">A. function f() </label></div>
	<div><input type="radio"  id="" name="treize" value="B"><label for="">B. function = f() </label></div>
	<div><input type="radio"  id="" name="treize" value="C"><label for="" selected>C. function:f() </label></div>
<p>
<p>
	14. Quelle est la syntaxe correct de la boucle for?
	<div><input type="radio"  id="quatorze" name="quatorze" value="A"><label for="">A. for (i <= 10; i++) </label></div>
	<div><input type="radio"  id="" name="quatorze" value="B"><label for="">B. for (i = 0; i <= 10) </label></div>
	<div><input type="radio"  id="" name="quatorze" value="C"><label for=""selected>C. for (i = 0; i <= 10; i++) </label></div>
<p>
<p>
	15. JavaScript est identique à Java?
	<div><input type="radio"  id="quinze" name="quinze" value="A"><label for=""selected>A. Faux </label></div>
	<div><input type="radio"  id="" name="quinze" value="B"><label for="">B. Vrai </label></div>
	
<p>
<h4>Base de données</h4>
<p>
	16. Quelle commande est utilisée pour supprimer une table existante?
	<div><input type="radio"  id="seize" name="seize" value="A" ><label for="" selected>A. DROP TABLE </label></div>
	<div><input type="radio"  id="" name="seize" value="B"><label for="">B. DELETE </label></div>
	<div><input type="radio"  id="" name="seize" value="C"><label for="">C. Les deux </label></div>
<p>
<p>
	17. Duplication de la clé primaire est-elle autorisée dans SQL?
	<div><input type="radio"  id="dix_sept" name="dix_sept" value="A"=><label for="dix_sept">A. Oui </label></div>
	<div><input type="radio"  id="" name="dix_sept" value="B"><label for="dix_sept" selected>B. Non </label></div>
	<div><input type="radio"  id="" name="dix_sept" value="C"><label for="dix_sept">C. Dépend de SGBD </label></div>
<p>
<p>
	18. Quelle ligne suivants produira une erreur?
	<div><input type="radio"  id="dix_huit" name="dix_huit" value="A"><label for="dix_huit">A. SELECT * FROM personne WHERE personne_id=1; </label></div>
	<div><input type="radio"  id="" name="dix_huit" value="B"><label for="dix_huit">B. SELECT nom FROM personne; </label></div>
	<div><input type="radio"  id="" name="dix_huit" value="C"><label for="dix_huit" selected>C. Aucune de ces réponses </label></div>
<p>
<p>
	19. Quelle clause est utilisée pour déterminer quelle colonne a inclure dans les ensembles de requêtes?
	<div><input type="radio"  id="dix_neuf" name="dix_neuf" value="A"><label for="dix_neuf" selected>A. SELECT </label></div>
	<div><input type="radio"  id="" name="dix_neuf" value="B"><label for="dix_neuf">B. FROM </label></div>
	<div><input type="radio"  id="" name="dix_neuf" value="C"><label for="dix_neuf" >C. WHERE </label></div>
<p>
<p>
	20. Quelle clause est utilisée pour identifie la table ?
	<div><input type="radio"  id="vingt" name="vingt" value="A" ><label for="vingt">A.SELECT </label></div>
	<div><input type="radio"  id="" name="vingt" value="B"><label for="vingt" selected >B. FROM </label></div>
	<div><input type="radio"  id="" name="vingt" value="C"><label for="vingt">C. WHERE </label></div>
<p>

<br>

<button type="submit"  class="btn btn-primary">Vérifier</button>

</form>
</div>
<%@ include file="footer.jspf" %>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>