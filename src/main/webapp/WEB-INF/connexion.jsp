<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Connexion</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>

	<%@ include file="header.jspf"%>
	<div class="container">
		<div class="m-5">
			<h2>Connexion</h2>
			<div class="form-group col-md-6">
				<a href="${pageContext.request.contextPath}/inscription">Cr�er un
					compte</a>
			</div>
			

			<form action="connexion" method="post" class="mt-5">
				<div class="form-group col-md-6">
					<label for="mail">Mail :</label> <input class="form-control"
						type="email" id="mail" name="mail" value="${param.mail}"
						placeholder="Saisir votre mail"><br />
				</div>
				<div class="form-group col-md-6">
					<label for="motDePasse">Mot de passe :</label> <input
						class="form-control" type="password" id="motDePasse"
						name="motDePasse" placeholder="Saisir votre mot de passe">
				</div>
				<div class="form-group col-md-6">
					<div class="text-danger">${messageErreur}</div>
				</div>
				
				<div class="mt-4">
				</div>
				<div class="form-group col-md-6">
					<input class="btn btn-primary" type="submit" value="Se connecter" />
				</div>
			</form>

		</div>
	</div>
	<%@ include file="footer.jspf"%>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>
</body>
</html>