<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png"
	type="image/x-icon">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">

<title>Page d'une Formation</title>



<title>la page de la formation</title>
</head>
<body>
	<%@ include file="header.jspf"%>



	<div class="container p-4 mt-4">



		<div class="row">
			<div class="col-md-8 col-sm-12">
				<div class="mt-5 mb-5">
					<h1>${formation.titre}</h1>
				</div>

				<div class="p">
					<h4 class="text-secondary">Description</h4>
				</div>
				<div class="card">
					<div class="card-body bg-light p-4">
						<div>
							<p class="card-text">${formation.description}</p>
						</div>
					</div>
				</div>

				<div class="mt-4">
					<h4 class="text-secondary">Objectifs</h4>
				</div>
				<div class="card">
					<div class="card-body bg-light p-4">
						<div>
							<p class="card-text">${formation.objectifs}</p>
						</div>
					</div>
				</div>

				<div class="mt-4">
					<h4 class="text-secondary">Pré-requis</h4>
				</div>
				<div class="card">
					<div class="card-body bg-light  p-4">
						<div>
							<p class="card-text">${formation.prerequis}</p>
						</div>
					</div>
				</div>

				<div class="mt-4">
					<h4 class="text-secondary">Modules</h4>
				</div>
				<div class="card">
					<div class="card-body bg-light p-4">
						<ul>
							<c:forEach items="${formation.modules}" var="module">
								<li name="choixModule" value="${module.id}">Module :
									${module.titre}</li>
							</c:forEach>

						</ul>
					</div>
				</div>


			</div>

			<div class="col-md-4 col-sm-12 mt-5">

				<div class="mt-2">
					<div class="card">
						<div class="card-header">


							<ul class="nav nav-tabs card-header-tabs" id="myTab"
								role="tablist">
								<li class="nav-item" role="presentation">
									<button class="nav-link active" id="home-tab"
										data-bs-toggle="tab" data-bs-target="#inter" type="button"
										role="tab" aria-controls="inter" aria-selected="true">INTER</button>
								</li>
								<li class="nav-item" role="presentation">
									<button class="nav-link" id="profile-tab" data-bs-toggle="tab"
										data-bs-target="#intra" type="button" role="tab"
										aria-controls="intra" aria-selected="false">INTRA</button>
								</li>

							</ul>
						</div>
						<div class="card-body">

							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="inter"
									role="tabpanel" aria-labelledby="home-tab">
									<h5 class=" mt-3 card-title text-center">Informations</h5>

									<div class="mt-3">
										<table class="table">

											<tr>
												<td>Tarif :</td>
												<td>${formation.prix}€HT</td>

											</tr>
											<tr>
												<td>Durée :
												</th>
												<td>8 jours</td>

											</tr>

										</table>
									</div>


									<div class="d-flex mt-2">
										<c:if test="${not empty formation.prochainesSessions}">
											<a
												href="inscriptionStagiaireSession?formationId=${formation.id}"
												class="flex-grow-1 btn btn-primary btn-block">S'inscrire
												à cette formation</a>
										</c:if>
										<c:if test="${empty formation.prochainesSessions}">
											<div class="flex-grow-1 d-flex" data-bs-toggle="tooltip" data-bs-placement="bottom"
												title="L'inscription à cette formation est fermée pour le moment parce qu'aucune Session n'est programmée">
												<button disabled
													class="flex-grow-1 btn btn-primary btn-block">
													S'inscrire à cette formation</button>
											</div>
										</c:if>

									</div>

								</div>
								<div class="tab-pane fade" id="intra" role="tabpanel"
									aria-labelledby="profile-tab">

									<div class="d-flex mt-2">

										<a href="contact" target="_blank"
											class="flex-grow-1 btn btn-primary btn-block" name="">

											Contactez nous</a>
									</div>
								</div>
							</div>


						</div>
					</div>
					<div class="mt-4">
						<h4 class="text-secondary">Sessions à venir</h4>
					</div>
					<div class="">
						<c:if test="${not empty formation.prochainesSessions}">
							<div class="">
								<p class="card-text">Différentes sessions sont programmées</p>

								<ul class="list-group">
									<c:forEach items="${formation.prochainesSessions}"
										var="session">
										<li class="list-group-item">Session : <span
											class="text-primary">${session.dateDebut}</span> - <span
											class="text-primary">${session.dateFin}</span></li>

									</c:forEach>
								</ul>
							</div>
						</c:if>
						<c:if test="${empty formation.prochainesSessions}">
							<p>Aucune</p>
						</c:if>


					</div>
				</div>
			</div>

		</div>
	</div>





	<div class="mt-5"></div>
	<%@ include file="footer.jspf"%>
</body>
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
	integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
	crossorigin="anonymous"></script>

<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
	integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
	crossorigin="anonymous"></script>


</html>