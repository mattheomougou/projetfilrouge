<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
<%@ include file="header.jspf" %>      
</head>



<body>

      
 <div class="container">
<div class = "m-5">  
<h1> Evaluation de fin de formation</h1>

<form action="evaluation" method = post class = "mt-5">
        



<p> La formation  à laquelle vous participez s'achève.
Nous vous remercions de consacrer quelques minutes à ce questionnaire qui nous permettra de mesurer votre satisfaction quant à la prestation qui vous a été délivrée.</p>

	<h2>La formation</h2>
		<p>Recommanderiez-vous cette formation à un ami ou un collègue ?</p>
 
    <input  type="radio" id="oui" name="isRecommandation" value="true" required="required" onchange="verifier()" class="exemple"><label for="oui">Oui</label>
    <input type="radio" id="non" name="isRecommandation" value="false"><label for="non">Non</label>
  
  
	<p>Qualité de l’accueil chez IT-Training <br>(1 = médiocre - 5 = Excellent)

		<input type="radio"  id="accueil" name="accueil" value="1" required="required"><label for="accueil">1</label>
		<input type="radio"  id="accueil" name="accueil" value="2"><label for="accueil">2</label>
		<input type="radio"  id="accueil" name="accueil" value="3"><label for="accueil">3</label>
		<input type="radio"  id="accueil" name="accueil" value="4"><label for="accueil">4</label>
		<input type="radio"  id="accueil" name="accueil" value="5"><label for="accueil">5</label>	

    
	
		</p>
		<p>
        Qualité de l'environnement:

		<input type="radio"  id="environnement" name="environnement" value="1" required="required"><label for="environnement">1</label>
		<input type="radio"  id="environnement" name="environnement" value="2"><label for="environnement">2</label>
		<input type="radio"  id="environnement" name="environnement" value="3"><label for="environnement">3</label>
		<input type="radio"  id="environnement" name="environnement" value="4"><label for="environnement">4</label>
		<input type="radio"  id="environnement" name="environnement" value="5"><label for="environnement">5</label>
		
  	 	</p>
		
        <p>
        Contenu de la formation:

		<input type="radio"  id="contenu" name="contenu" value="1" ><label for="contenu">1</label>
		<input type="radio"  id="contenu" name="contenu" value="2"><label for="contenu">2</label>
		<input type="radio"  id="contenu" name="contenu" value="3"><label for="contenu">3</label>
		<input type="radio"  id="contenu" name="contenu" value="4"><label for="contenu">4</label>
		<input type="radio"  id="contenu" name="contenu" value="5"><label for="contenu">5</label>
		
  	 	</p>
  	 	<p>
        Êtes-vous satisfait(e) de la formation:

		<input type="radio"  id="satisfaction" name="satisfaction" value="1" required="required"><label for="satisfaction">1</label>
		<input type="radio"  id="satisfaction" name="satisfaction" value="2"><label for="satisfaction">2</label>
		<input type="radio"  id="satisfaction" name="satisfaction" value="3"><label for="satisfaction">3</label>
		<input type="radio"  id="satisfaction" name="satisfaction" value="4"><label for="satisfaction">4</label>
		<input type="radio"  id="satisfaction" name="satisfaction" value="5"><label for="satisfaction">5</label>
		
  	 	</p>
  	 	<h3>Le consultant-formateur</h3>
  	 		<p>
        Note formateur:

		<input type="radio"  id="formateur" name="formateur" value="1" required="required"><label for="formateur">1</label>
		<input type="radio"  id="formateur" name="formateur" value="2"><label for="formateur">2</label>
		<input type="radio"  id="formateur" name="formateur" value="3"><label for="formateur">3</label>
		<input type="radio"  id="formateur" name="formateur" value="4"><label for="formateur">4</label>
		<input type="radio"  id="formateur" name="formateur" value="5"><label for="formateur">5</label>
		
  	 	</p>
  	 	
  	 	<p>
        disponibilité:

		<input type="radio"  id="disponibilité" name="disponibilité" value="1" required="required"><label for="disponibilité">1</label>
		<input type="radio"  id="disponibilité" name="disponibilité" value="2"><label for="disponibilité">2</label>
		<input type="radio"  id="disponibilité" name="disponibilité" value="3"><label for="disponibilité">3</label>
		<input type="radio"  id="disponibilité" name="disponibilité" value="4"><label for="disponibilité">4</label>
		<input type="radio"  id="disponibilité" name="disponibilité" value="5"><label for="disponibilité">5</label>
		
  	 	</p>
  	 	<p>
        Maîtrise du domaine:

		<input type="radio"  id="maîtriseDomaine" name="maîtriseDomaine" value="1" required="required"><label for="maîtriseDomaine">1</label>
		<input type="radio"  id="maîtriseDomaine" name="maîtriseDomaine" value="2"><label for="maîtriseDomaine">2</label>
		<input type="radio"  id="maîtriseDomaine" name="maîtriseDomaine" value="3"><label for="maîtriseDomaine">3</label>
		<input type="radio"  id="maîtriseDomaine" name="maîtriseDomaine" value="4"><label for="maîtriseDomaine">4</label>
		<input type="radio"  id="maîtriseDomaine" name="maîtriseDomaine" value="5"><label for="maîtriseDomaine">5</label>
		
  	 	</p>
  	 	<p>
  	 	Pédagogie:

		<input type="radio"  id="pedagogie" name="pedagogie" value="1" required="required"><label for="pedagogie">1</label>
		<input type="radio"  id="pedagogie" name="pedagogie" value="2"><label for="pedagogie">2</label>
		<input type="radio"  id="pedagogie" name="pedagogie" value="3"><label for="pedagogie">3</label>
		<input type="radio"  id="pedagogie" name="pedagogie" value="4"><label for="pedagogie">4</label>
		<input type="radio"  id="pedagogie" name="pedagogie" value="5"><label for="pedagogie">5</label>
  	 	</p>
  	 	<p>
  	 	Réponse aux questions:
		<input type="radio"  id="reponsesQuestion" name="reponsesQuestion" value="1" required="required"><label for="reponsesQuestion">1</label>
		<input type="radio"  id="reponsesQuestion" name="reponsesQuestion" value="2"><label for="reponsesQuestion">2</label>
		<input type="radio"  id="reponsesQuestion" name="reponsesQuestion" value="3"><label for="reponsesQuestion">3</label>
		<input type="radio"  id="reponsesQuestion" name="reponsesQuestion" value="4"><label for="reponsesQuestion">4</label>
		<input type="radio"  id="reponsesQuestion" name="reponsesQuestion" value="5"><label for="reponsesQuestion">5</label>
  	 	</p>
  	 	<p>
  	 	Techniques d'animation:
		<input type="radio"  id="animation" name="animation" value="1" required="required"><label for="animation">1</label>
		<input type="radio"  id="animation" name="animation" value="2"><label for="animation">2</label>
		<input type="radio"  id="animation" name="animation" value="3"><label for="animation">3</label>
		<input type="radio"  id="animation" name="animation" value="4"><label for="animation">4</label>
		<input type="radio"  id="animation" name="animation" value="4"><label for="animation">5</label>
  	 	</p>
  	 	
  	 
 		<p> Avez-vous d'autres projets de formations?
		<input type="radio"  id="projetFormation" name="projetFormation" value = true  required="required"/><label for="isProjetFormation"  >oui</label>
		<input type="radio"  id="projetFormation" name="projetFormation" value = false /><label for="isProjetFormation">non</label><br/>
		<textarea rows="3" cols="50" placeholder="Si oui lesquels?" name= "commentaires_projet_formation" required="required"></textarea>
  	 	</p>
  	
  
 	<input class="btn btn-primary" type="submit" value="Envoyer" />
   
</form>

</div>
</div>

<%@ include file="footer.jspf" %>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
 
</body>

</html>