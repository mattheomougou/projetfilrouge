<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inscription</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>

	<%@ include file="header.jspf"%>
	<div class="container">
		<div class="m-5">
			<h2>Inscription</h2>
			<div class="form-group col-md-6">
				<a href="${pageContext.request.contextPath}/connexion">J'ai déjà un compte</a>
			</div>
			
			
			<form action="inscription" method="post" class="mt-5">
				<div class="form-group col-md-6">
					<label for="nom">Nom :</label> <input type="text" name="nom"
						class="form-control" id="nom"
						value="${empty erreurs.get('nom') ? param.nom : ''}"
						placeholder="Saisir votre nom" required>
					<div class="text-danger">${erreurs.get('nom')}</div>
				</div>

				<div class="form-group col-md-6">
					<label for="prenom">Prénom :</label> <input type="text"
						name="prenom" class="form-control" id="prenom"
						value="${empty erreurs.get('prenom') ? param.prenom : ''}"
						placeholder="Saisir votre prénom" required>
					<div class="text-danger">${erreurs.get('prenom')}</div>
				</div>


				<div class="form-group col-md-6">
					<label for="email">Mail :</label> <input class="form-control"
						type="email" id="mail" name="mail"
						value="${empty erreurs.get('mail') ? param.mail : ''}"
						placeholder="Saisir votre mail" required>
					<div class="text-danger">${erreurs.get('mail')}</div>
					<br />

				</div>
				<div class="form-group col-md-6">
					<label for="motDePasse">Mot de passe :</label> <input
						class="form-control" type="password" id="motDePasse"
						name="motDePasse" placeholder="Saisir votre mot de passe" required>
					<div class="text-danger">${erreurs.get('motDePasse')}</div>
					<br />
				</div>

				<div class="form-group col-md-6">
					<label for="confirmMdp">Confirmer le mot de passe :</label> <input
						class="form-control" type="password"
						placeholder="Confirmer votre mot de passe" id="confirmMotDePasse"
						name="confirmMotDePasse" required>
					<div class="text-danger">${erreurs.get('confirmMotDePasse')}</div>
					<br />
				</div>

				<div class="form-group col-md-6">
					<label for="telephone">Téléphone :</label> <input type="text"
						name="telephone" class="form-control" id="telephone"
						value="${empty erreurs.get('telephone') ? param.telephone : ''}"
						placeholder="Saisir votre numéro de téléphone" required>
					<div class="text-danger">${erreurs.get('telephone')}</div>
					<br />
				</div>
				<div class="form-group col-md-6">
					<label for="adresse">Adresse :</label> <input type="text"
						name="adresse" class="form-control" id="adresse"
						value="${param.adresse}" placeholder="Saisir votre adresse"><br />
				</div>
				<div class="form-group col-md-6">
					<label for="codePostal">Code postal :</label> <input type="text"
						name="codePostal" class="form-control" id="codePostal"
						value="${param.codePostal}" placeholder="Saisir votre code postal"><br />
				</div>
				<div class="form-group col-md-6">
					<label for="ville">Ville :</label> <input type="text" name="ville"
						class="form-control" id="ville" value="${param.ville}"
						placeholder="Saisir votre ville"><br />
				</div>
				

				<input class="btn btn-primary" type="submit" value="S'inscrire" />
			</form>

		</div>
	</div>
	<%@ include file="footer.jspf"%>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>
</body>
</html>