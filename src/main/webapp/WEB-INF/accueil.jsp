<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT-Training</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png"
	type="image/x-icon">
</head>
<body>
	<%@ include file="header.jspf"%>


	<section data-bs-version="5.1"
		class="header3 pt-5 pb-5 travelm4_header3 cid-sZ1LScXz03 mbr-parallax-background"
		id="header3-3p">



		<div class="mbr-overlay"
			style="opacity: 0.7; background-color: rgb(68, 121, 217);"></div>

		<div class="container">
			<h1
				class="mbr-section-title mbr-bold align-left mbr-white pb-1 mbr-fonts-style display-1">
				Formez votre succès.<br>
			</h1>

			<h3
				class="mbr-section-sub-title pb-4 mbr-normal mbr-fonts-style align-left display-4">Trouver
				la formation informatique qui répond à vos besoins</h3>
			<div class="row justify-content-center align-center">
				<div
					class="media-container-column col-lg-12 col-md-10 col-sm-12 align-center">
					<!---Formbuilder Form--->
					<form action="chercherFormations" method="GET"
						class="mbr-form form-with-styler">
						<div class="row">

							<div hidden="hidden" data-form-alert-danger=""
								class="alert alert-danger col-12"></div>
						</div>
						<div class="dragArea row">
							<div class="col-lg-3 col-md-12 relative form-group mb-3"
								data-for="message">
								<input type="text" name="q" data-form-field="Message"
									class="form-control input display-7"
									placeholder="Chercher une formation" id="message-header3-3p">
								<div class="ico">
									<span class="mbr-iconfont mbri-search"></span>
								</div>
							</div>


							<div class="col-lg-3 col-md-12 input-group-btn selected">
								<button type="submit"
									class="btn btn-md btn-form btn-primary display-4">Recherche</button>
							</div>
						</div>
					</form>
					<!---Formbuilder Form--->
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="slider1 mbr-embla cid-sZ2HFyubPD"
		id="slider1-45">


		<div class="position-relative">
			<div class="container">
				<div class="row">
					<div class="mbr-section-head">
						<div class="section-title-container">
							<h4 class="mbr-section-title mbr-fonts-style mb-0 display-2">
								Actualité</h4>

						</div>
						<h5
							class="mbr-section-subtitle mbr-fonts-style mb-0 mt-2 display-4">Suivez
							toute l'actualité web et high tech. Découvrez nos articles et nos
							points de vues sur les dernières innovations technologiques.</h5>
					</div>
				</div>
			</div>
			<div class="embla" data-skip-snaps="true" data-align="center"
				data-contain-scroll="trimSnaps" data-loop="true"
				data-auto-play="true" data-auto-play-interval="5"
				data-draggable="true">
				<div class="embla__viewport container">
					<div class="embla__container">
						<div class="embla__slide slider-image item"
							style="margin-left: 1rem; margin-right: 1rem;">
							<div class="slide-content">
								<div class="item-wrapper">
									<div class="item-img">
										<img
											src="${pageContext.request.contextPath}/assets//images/features5.jpg"
											alt="" title="">
										<div class="img-filter"></div>
									</div>
								</div>
								<div class="item-content">
									<div class="item-title-container">
										<p class="item-title mbr-fonts-style mb-0 display-7">
											Toutes nos formations<br>
										</p>
									</div>
									<p class="item-subtitle mbr-fonts-style mb-0 display-4">Retrouvez
										notre offre de formations dédiées aux technologies et métiers
										du numérique</p>

								</div>

							</div>
						</div>
						<div class="embla__slide slider-image item"
							style="margin-left: 1rem; margin-right: 1rem;">
							<div class="slide-content">
								<div class="item-wrapper">
									<div class="item-img">
										<img
											src="${pageContext.request.contextPath}/assets//images/features6.jpg"
											alt="" title="">
										<div class="img-filter"></div>
									</div>
								</div>
								<div class="item-content">
									<div class="item-title-container">
										<p class="item-title mbr-fonts-style mb-0 display-7">
											Formations à distance<br>
										</p>
									</div>
									<p class="item-subtitle mbr-fonts-style mb-0 display-4">
										Tout savoir sur les formations à distance IT, une alternative
										efficace à la formation en salle.<br> <br>
									</p>

								</div>

							</div>
						</div>
						<div class="embla__slide slider-image item"
							style="margin-left: 1rem; margin-right: 1rem;">
							<div class="slide-content">
								<div class="item-wrapper">
									<div class="item-img">
										<img
											src="${pageContext.request.contextPath}/assets//images/features7.jpg"
											alt="" title="">
										<div class="img-filter"></div>
									</div>
								</div>
								<div class="item-content">
									<div class="item-title-container">
										<p class="item-title mbr-fonts-style mb-0 display-7">
											Formations mixtes<br>
										</p>
									</div>
									<p class="item-subtitle mbr-fonts-style mb-0 display-4">
										Vivez une nouvelle expérience d'apprentissage avec nos
										formations mixtes <br> <br>
									</p>

								</div>

							</div>
						</div>
						<div class="embla__slide slider-image item"
							style="margin-left: 1rem; margin-right: 1rem;">
							<div class="slide-content">
								<div class="item-wrapper">
									<div class="item-img">
										<img
											src="${pageContext.request.contextPath}/assets//images/features8.jpg"
											alt="" title="">
										<div class="img-filter"></div>
									</div>
								</div>
								<div class="item-content">
									<div class="item-title-container">
										<p class="item-title mbr-fonts-style mb-0 display-7">
											Sécurité informatique et télétravail<br>
										</p>
									</div>
									<p class="item-subtitle mbr-fonts-style mb-0 display-4">les
										bonnes pratiques.</p>

								</div>

							</div>
						</div>
						<div class="embla__slide slider-image item"
							style="margin-left: 1rem; margin-right: 1rem;">
							<div class="slide-content">
								<div class="item-wrapper">
									<div class="item-img">
										<img
											src="${pageContext.request.contextPath}/assets//images/features9.jpg"
											alt="" title="">
										<div class="img-filter"></div>
									</div>
								</div>
								<div class="item-content">
									<div class="item-title-container">
										<p class="item-title mbr-fonts-style mb-0 display-7">Reconversion
											informatique</p>
									</div>
									<p class="item-subtitle mbr-fonts-style mb-0 display-4">Les
										10 métiers qui recrutent.</p>

								</div>

							</div>
						</div>
						<div class="embla__slide slider-image item"
							style="margin-left: 1rem; margin-right: 1rem;">
							<div class="slide-content">
								<div class="item-wrapper">
									<div class="item-img">
										<img
											src="${pageContext.request.contextPath}/assets//images/features10.jpg"
											alt="" title="">
										<div class="img-filter"></div>
									</div>
								</div>
								<div class="item-content">
									<div class="item-title-container">
										<p class="item-title mbr-fonts-style mb-0 display-7">
											Toutes nos formations<br>
										</p>
									</div>
									<p class="item-subtitle mbr-fonts-style mb-0 display-4">Retrouvez
										notre offre de formations dédiées aux technologies et métiers
										du numérique</p>

								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="embla__button-container">
						<button class="embla__button embla__button--prev">
							<span class="mobi-mbri mobi-mbri-arrow-prev mbr-iconfont"
								aria-hidden="true"></span> <span
								class="sr-only visually-hidden visually-hidden">Previous</span>
						</button>
						<button class="embla__button embla__button--next">
							<span class="mobi-mbri mobi-mbri-arrow-next mbr-iconfont"
								aria-hidden="true"></span> <span
								class="sr-only visually-hidden visually-hidden">Next</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="slider1 cid-sZ2169g7bW"
		id="gallery2-3r">


		<div class="container">
			<div class="row">
				<div class="mbr-section-head">
					<div class="section-title-container">
						<h4 class="mbr-section-title mbr-fonts-style mb-0 display-2">
							Solutions</h4>
					</div>

				</div>
			</div>
		</div>
		<div class="embla__viewport container">
			<div class="embla__container">
				<div class="embla__slide slider-image item col-12 col-md-6 col-lg-4">
					<div class="sl-content">
						<div class="item-wrap">
							<div class="item-img">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-810x539.jpg"
									alt="" title="">
								<div class="img-filter"></div>
							</div>
						</div>
						<div class="item-content">
							<div class="item-title-container">
								<p class="item-title mbr-fonts-style mb-0 display-7">
									Toutes nos formations<br>
								</p>
							</div>
							<p class="item-subtitle mbr-fonts-style mb-0 display-4">
								Retrouvez notre offre de formations dédiées aux technologies et
								métiers du numérique <br> <br>
							</p>

						</div>

					</div>
				</div>
				<div
					class="embla__slide slider-image item  col-12 col-md-6 col-lg-4">
					<div class="sl-content">
						<div class="item-wrap">
							<div class="item-img">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-810x540.jpg"
									alt="" title="">
								<div class="img-filter"></div>
							</div>
						</div>
						<div class="item-content">
							<div class="item-title-container">
								<p class="item-title mbr-fonts-style mb-0 display-7">
									Formations à distance<br>
								</p>
							</div>
							<p class="item-subtitle mbr-fonts-style mb-0 display-4">
								Tout savoir sur les formations à distance IT, une alternative
								effica à la formation en salle.<br> <br>
							</p>

						</div>

					</div>
				</div>
				<div
					class="embla__slide slider-image item  col-12 col-md-6 col-lg-4">
					<div class="sl-content">
						<div class="item-wrap">
							<div class="item-img">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-810x510.jpg"
									alt="" title="">
								<div class="img-filter"></div>
							</div>
						</div>
						<div class="item-content">
							<div class="item-title-container">
								<p class="item-title mbr-fonts-style mb-0 display-7">
									Formations mixtes<br>
								</p>
							</div>
							<p class="item-subtitle mbr-fonts-style mb-0 display-4">
								Vivez une nouvelle expérience d'apprentissage avec nos
								formations mixtes<br> <br>
							</p>

						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="slider1 cid-sZ28G3yYPB"
		id="gallery2-3u">


		<div class="container">
			<div class="row">
				<div class="mbr-section-head">
					<div class="section-title-container">
						<h4 class="mbr-section-title mbr-fonts-style mb-0 display-2">
							Financements</h4>
					</div>
					<h5
						class="mbr-section-subtitle mbr-fonts-style mb-0 mt-2 display-4">Lorem
						ipsum dolor sit amet, consectetur adipiscing elit</h5>
				</div>
			</div>
		</div>
		<div class="embla__viewport container">
			<div class="embla__container">
				<div class="embla__slide slider-image item col-12 col-md-6 col-lg-4">
					<div class="sl-content">
						<div class="item-wrap">
							<div class="item-img">
								<img
									src="${pageContext.request.contextPath}/assets//images/features5.jpg"
									alt="" title="">
								<div class="img-filter"></div>
							</div>
						</div>
						<div class="item-content">

							<p class="item-subtitle mbr-fonts-style mb-0 display-4">
								CPF <br>Inscrivez-vous à nos formations éligibles au CPF
								pour développer vos compétences
							</p>

						</div>

					</div>
				</div>
				<div
					class="embla__slide slider-image item  col-12 col-md-6 col-lg-4">
					<div class="sl-content">
						<div class="item-wrap">
							<div class="item-img">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-5-810x540.jpg"
									alt="" title="">
								<div class="img-filter"></div>
							</div>
						</div>
						<div class="item-content">

							<p class="item-subtitle mbr-fonts-style mb-0 display-4">
								Actions Co et Offres Clé en main <br>Découvrez comment
								bénéficier de formations financées par les OPCO
							</p>

						</div>

					</div>
				</div>
				<div
					class="embla__slide slider-image item  col-12 col-md-6 col-lg-4">
					<div class="sl-content">
						<div class="item-wrap">
							<div class="item-img">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-4-810x540.jpg"
									alt="" title="">
								<div class="img-filter"></div>
							</div>
						</div>
						<div class="item-content">

							<p class="item-subtitle mbr-fonts-style mb-0 display-4">
								FNE Formation <br>Tout sur le dispositif pour développer
								des compétences et améliorer la compétitivité
							</p>

						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="features10 cid-sZ2B2hsdfv"
		id="features11-3z">
		<!---->


		<div class="container">
			<div class="title">
				<h3 class="mbr-section-title mbr-fonts-style mb-4 display-2">
					<strong>Nos formations du moment</strong>
				</h3>

			</div>
			<div class="card">
				<div class="card-wrapper">
					<div class="row align-items-center">
						<div class="col-12 col-md-3">
							<div class="image-wrapper">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-596x354.jpg"
									alt="" title="">
							</div>
						</div>
						<div class="col-12 col-md">
							<div class="card-box">
								<div class="row">
									<div class="col-12">
										<div class="top-line">
											<h4 class="card-title mbr-fonts-style display-5">
												<strong>Formations intra et sur mesure</strong>
											</h4>
											<p class="cost mbr-fonts-style display-5"></p>
										</div>
									</div>
									<div class="col-12">
										<div class="bottom-line">
											<p class="mbr-text mbr-fonts-style m-0 display-7">
												Retrouvez notre offre de formations dédiées aux technologies
												et métiers du numérique <br> <br>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-wrapper">
					<div class="row align-items-center">
						<div class="col-12 col-md-3">
							<div class="image-wrapper">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-596x397.jpg"
									alt="" title="">
							</div>
						</div>
						<div class="col-12 col-md">
							<div class="card-box">
								<div class="row">
									<div class="col-12">
										<div class="top-line">
											<h4 class="card-title mbr-fonts-style display-5">
												<strong>CPF</strong>
											</h4>
											<p class="cost mbr-fonts-style display-5"></p>
										</div>
									</div>
									<div class="col-12">
										<div class="bottom-line">
											<p class="mbr-text mbr-fonts-style m-0 display-7">
												Inscrivez-vous à nos formations éligibles au CPF pour
												développer vos compétences</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-wrapper">
					<div class="row align-items-center">
						<div class="col-12 col-md-3">
							<div class="image-wrapper">
								<img
									src="${pageContext.request.contextPath}/assets//images/mbr-596x278.jpg"
									alt="" title="">
							</div>
						</div>
						<div class="col-12 col-md">
							<div class="card-box">
								<div class="row">
									<div class="col-12">
										<div class="top-line">
											<h4 class="card-title mbr-fonts-style display-5">
												<strong>Big Data</strong>
											</h4>
											<p class="cost mbr-fonts-style display-5"></p>
										</div>
									</div>
									<div class="col-12">
										<div class="bottom-line">
											<p class="mbr-text mbr-fonts-style m-0 display-7">
												Les outils clés pour l'analyse de données. <br>Devenir
												professionnel de la Data <br> <br>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</section>

	<section data-bs-version="5.1" class="map3 cid-sZ2vPcyvyi" id="map3-3x">





		<div class="container">
			<div class="row">
				<div class="col-12">
					<h3
						class="mbr-section-title mb-4 align-center mbr-fonts-style display-2">
						<strong>Nos agences</strong>
					</h3>
				</div>
				<div class="col-12 col-lg-6">
					<h4
						class="mbr-section-subtitle mb-3 align-center mbr-fonts-style display-5">Rennes</h4>
					<div class="google-map">
						<iframe frameborder="0" style="border: 0"
							src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDk89J4FSunMF33ruMVWJaJht_Ro0kvoXs&amp;q=12 rue du Patis Tatelin, 35700 Rennes"
							allowfullscreen=""></iframe>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<h4
						class="mbr-section-subtitle mb-3 mt-5 mt-lg-0 align-center mbr-fonts-style display-5">Bordeaux</h4>
					<div class="google-map">
						<iframe frameborder="0" style="border: 0"
							src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCZI5F_k6S1k46ujh0SNrapM89f7mJxd30&amp;q=Tour Atlantique, 1 Pl. de la Pyramide,92800 Puteaux"
							allowfullscreen=""></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1"
		class="form1 cid-sZ2YpYvxBZ mbr-parallax-background" id="form1-4a">



		<div class="mbr-overlay"
			style="opacity: 0.5; background-color: rgb(255, 255, 255);"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 mx-auto mbr-form" data-form-type="formoid">
					<form action="https://mobirise.eu/" method="POST"
						class="mbr-form form-with-styler" data-form-title="Form Name">
						<input type="hidden" name="email" data-form-email="true"
							value="mvLBM8xecIkTF2CQvIdM3BRq9ZdkUMXNsvWqJnbst6E17wujt9DbU3yCzOPrJ/Dv4y3QnjcyI/HreauNtrJtFUbuXbp9Fj0xfAHQj4770+rRoe/Ds/QjtkDFZe+JR4IZ.AStCiJREU69V12vSdwCsp0quQoF11JSKj8E2Vhf8DawvwSizWXLlaLsh3Z72OHPX3zxqQ+ZWFXSXkL9ObQSe2rDYn7mNQ42XRuIoXL+/xKTw049yu9ogFqou8+3HtS+Q">
						<div class="row">
							<div hidden="hidden" data-form-alert=""
								class="alert alert-success col-12">Thanks for filling out
								the form!</div>
							<div hidden="hidden" data-form-alert-danger=""
								class="alert alert-danger col-12">Oops...! some problem!</div>
						</div>
						<div class="dragArea row">
							<div class="col-12">
								<h1
									class="mbr-section-title mb-4 mbr-fonts-style align-center display-2">
									<strong>Suivez toute l'actualité IT-Training!</strong>
								</h1>
							</div>
							<div class="col-12">
								<p class="mbr-text mbr-fonts-style mb-5 align-center display-7">Inscrivez-vous
									à notre news-letters et ne manquez rien de l'actualité sur nos
									formations</p>
							</div>
							<div class="col-md col-12 form-group mb-3" data-for="name">
								<input type="text" name="name" placeholder="Nom"
									data-form-field="Name" class="form-control" id="name-form1-4a">
							</div>
							<div class="col-md col-12 form-group mb-3" data-for="email">
								<input type="email" name="email" placeholder="Email"
									data-form-field="Email" class="form-control"
									id="email-form1-4a">
							</div>
							<div class="mbr-section-btn col-12 col-md-auto">
								<button type="submit" class="btn btn-primary display-4">Subscribe</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>







	<%@ include file="footer.jspf"%>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>
</body>
</html>