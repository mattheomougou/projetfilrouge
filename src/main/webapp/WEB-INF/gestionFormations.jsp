<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Gestion formation</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script src="https://kit.fontawesome.com/25ea7e5f39.js"
	crossorigin="anonymous"></script>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png"
	type="image/x-icon">
<link href="general.css" rel="stylesheet">
<link href="gestionFormation.css" rel="stylesheet">
</head>
<body>


	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1">
			<%@ include file="header.jspf"%>





			<div class="container main-content p-5">

				<div class="d-flex mt-3 mb-3 align-items-center">
					<div class="flex-grow-1">
						<div>
							<h1>Gestion des Formations</h1>
						</div>
						<div>
							<%@ include file="messages.jspf"%>
						</div>
					</div>
					<div class="">
						<div>
							<a class="btn btn-primary" href="ajouterFormation"> <span
								class="mobi-mbri mobi-mbri-edit mbr-iconfont mbr-iconfont-btn"></span>
								Ajouter une formation
							</a>
						</div>
					</div>
				</div>

				<c:forEach items="${formations}" var="formation">

					<div class="mt-3">
						<div class="card bg-light">
							<div class="card-body">

								<div class="p-3">

									<div class="d-flex align-items-center mb-1 mt-1">
										<div class="flex-grow-1 d-flex align-items-center">
											<h4 class="card-title text-primary">
												<span
													class="fs-5  mobi-mbri mobi-mbri-bookmark mbr-iconfont"></span>
												<span class="ms-1">${formation.titre}</span>
											</h4>
										</div>

									</div>
									<div class="row">
										<div class="col-8">

											<div class="mt-3">
												<h6 class="card-title d-flex align-items-center">
													<span class="mobi-mbri mobi-mbri-features mbr-iconfont"></span>
													<strong class="ms-1">Modules</strong>
												</h6>
											</div>
											<ul class="mt-3 mb-0">
												<c:forEach items="${formation.modules}" var="module">
													<li class="mb-2" name="choixModule" value="${module.id}">${module.titre}</li>
												</c:forEach>
											</ul>
										</div>
										<div class="col-4">

											<div class="mt-3">
												<c:if test="${not empty formation.sessions}">

													<div class="">
														<p class="card-text fw-bold d-flex align-items-center">
															<span class="mobi-mbri mobi-mbri-calendar mbr-iconfont"></span>
															<strong class="ms-1"> Sessions � venir</strong>
														</p>
														<ul class="list-group">
															<c:forEach items="${formation.sessions}" var="session">
																<li class="list-group-item">Session : <span
																	class="text-primary">${session.dateDebut}</span> - <span
																	class="text-primary">${session.dateFin}</span> 
																	<c:if
																		test="${not empty nombreInscritSession.get(session.id)}">

																		<span class="text-info">
																			${nombreInscritSession.get(session.id)} inscrits</span>
																	</c:if>
																</li>
															</c:forEach>
														</ul>
													</div>



												</c:if>
												<c:if test="${empty formation.sessions}">
													<div class="text-center">

														<p class=" mb-4 text-center fw-bold">Aucune session</p>
														<div>
															<span
																class="fs-1 mobi-mbri mobi-mbri-calendar mbr-iconfont"></span>
														</div>
													</div>
												</c:if>
											</div>


										</div>
									</div>
								</div>

								<div class="d-flex mt-2 p-2 justify-content-end">
									<div class="me-2 d-flex">
										<form method="post" action="gestionFormation">
											<div class="d-flex">
												<input type="hidden" value="${formation.id}"
													name="idFormation" />
												<button class="btn btn-secondary btn-sm flex-grow-1 btn-sm"
													type="submit">

													<span
														class="mobi-mbri mobi-mbri-trash mbr-iconfont mbr-iconfont-btn"></span>
													Supprimer la formation
												</button>


											</div>
										</form>
									</div>
									<div class="me-2 d-flex">
										<a target="_blank"
											class="btn btn-dark  btn-sm m-0 btn-sm flex-grow-1"
											href="pageFormation?id=${formation.id}"> <span
											class="mobi-mbri mobi-mbri-info mbr-iconfont mbr-iconfont-btn"></span>
											Information sur le formation
										</a>
									</div>
									<div class="me-2 d-flex">

										<a class="btn btn-dark  btn-sm flex-grow-1 btn-sm m-0"
											href="${pageContext.request.contextPath}/ajouterSession?formationId=${formation.id}">
											<span
											class="mobi-mbri mobi-mbri-calendar mbr-iconfont mbr-iconfont-btn"></span>
											Planifier une Session
										</a>
									</div>

								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>


		</div>

	</div>
	<!-- <!%@ include file="footer.jspf"%> -->



</body>
</html>