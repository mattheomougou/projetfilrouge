<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>S'inscription a une session</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>

	<%@ include file="header.jspf"%>

	<div class="container p-4 mt-4">

		<div class="row">
			<div class="col-md-8 col-sm-12">


				<div class="mt-5 mb-5">
					<h1>Inscription à une formation</h1>
					<h4>${formation.titre}</h4>

					<%@ include file="messages.jspf"%>
				</div>

			</div>

		</div>




		<form action="inscriptionStagiaireSession" method="post">
		
			<div class="row">
				<div class="col-md-8 col-sm-12 mb-3">

					<div class="card ">
						<div class="card-body p-4">
							<div class="form-group">
								<label for="nom">Nom</label> <input class="form-control"
									type="text" id="nom" name="nom" value="${utilisateur.nom}">
							</div>

							<div class="form-group">
								<label for="prenom">Prenom</label> <input class="form-control"
									type="text" id="prenom" name="prenom"
									value="${utilisateur.prenom}"><br />
							</div>
							<div class="form-group">
								<label for="entreprise">Entreprise</label> <input
									class="form-control" type="text" id="entreprise"
									name="entreprise" value="${param.entreprise}">
							</div>


							<div>
								<label for="departement">Departement</label> <select
									class="form-select" id="departement" name="departement">
									<option value="1">1 - Ain</option>
									<option value="2">2 - Aisne</option>
									<option value="3">3 - Allier</option>
									<option value="4">4 - Alpes-de-Haute-Provence</option>
									<option value="5">5 - Hautes-Alpes</option>
									<option value="6">6 - Alpes-Maritimes</option>
									<option value="7">7 - Ardèche</option>
									<option value="8">8 - Ardennes</option>
									<option value="9">9 - Ariège</option>
									<option value="10">10 - Aube</option>
									<option value="11">11 - Aude</option>
									<option value="12">12 - Aveyron</option>
									<option value="13">13 - Bouches-du-Rhône</option>
									<option value="14">14 - Calvados</option>
									<option value="15">15 - Cantal</option>
									<option value="16">16 - Charente</option>
									<option value="17">17 - Charente-Maritime</option>
									<option value="18">18 - Cher</option>
									<option value="19">19 - Corrèze</option>
									<option value="21">21 - Côte-d'Or</option>
									<option value="22">22 - Côtes-d'Armor</option>
									<option value="23">23 - Creuse</option>
									<option value="24">24 - Dordogne</option>
									<option value="25">25 - Doubs</option>
									<option value="26">26 - Drôme</option>
									<option value="27">27 - Eure</option>
									<option value="28">28 - Eure-et-Loir</option>
									<option value="29">29 - Finistère</option>
									<option value="2A">2A - Corse-du-Sud</option>
									<option value="2B">2B - Haute-Corse</option>
									<option value="30">30 - Gard</option>
									<option value="31">31 - Haute-Garonne</option>
									<option value="32">32 - Gers</option>
									<option value="33">33 - Gironde</option>
									<option value="34">34 - Hérault</option>
									<option value="35" selected="selected">35 -Ille-et-Vilaine</option>
									<option value="36">36 - Indre</option>
									<option value="37">37 - Indre-et-Loire</option>
									<option value="38">38 - Isère</option>
									<option value="39">39 - Jura</option>
									<option value="40">40 - Landes</option>
									<option value="41">41 - Loir-et-Cher</option>
									<option value="42">42 - Loire</option>
									<option value="43">43 - Haute-Loire</option>
									<option value="44">44 - Loire-Atlantique</option>
									<option value="45">45 - Loiret</option>
									<option value="46">46 - Lot</option>
									<option value="47">47 - Lot-et-Garonne</option>
									<option value="48">48 - Lozère</option>
									<option value="49">49 - Maine-et-Loire</option>
									<option value="50">50 - Manche</option>
									<option value="51">51 - Marne</option>
									<option value="52">52 - Haute-Marne</option>
									<option value="53">53 - Mayenne</option>
									<option value="54">54 - Meurthe-et-Moselle</option>
									<option value="55">55 - Meuse</option>
									<option value="56">56 - Morbihan</option>
									<option value="57">57 - Moselle</option>
									<option value="58">58 - Nièvre</option>
									<option value="59">59 - Nord</option>
									<option value="60">60 - Oise</option>
									<option value="61">61 - Orne</option>
									<option value="62">62 - Pas-de-Calais</option>
									<option value="63">63 - Puy-de-Dôme</option>
									<option value="64">64 - Pyrénées-Atlantiques</option>
									<option value="65">65 - Hautes-Pyrénées</option>
									<option value="66">66 - Pyrénées-Orientales</option>
									<option value="67">67 - Bas-Rhin</option>
									<option value="68">68 - Haut-Rhin</option>
									<option value="69">69 - Rhône</option>
									<option value="70">70 - Haute-Saône</option>
									<option value="71">71 - Saône-et-Loire</option>
									<option value="72">72 - Sarthe</option>
									<option value="73">73 - Savoie</option>
									<option value="74">74 - Haute-Savoie</option>
									<option value="75">75 - Paris</option>
									<option value="76">76 - Seine-Maritime</option>
									<option value="77">77 - Seine-et-Marne</option>
									<option value="78">78 - Yvelines</option>
									<option value="79">79 - Deux-Sèvres</option>
									<option value="80">80 - Somme</option>
									<option value="81">81 - Tarn</option>
									<option value="82">82 - Tarn-et-Garonne</option>
									<option value="83">83 - Var</option>
									<option value="84">84 - Vaucluse</option>
									<option value="85">85 - Vendée</option>
									<option value="86">86 - Vienne</option>
									<option value="87">87 - Haute-Vienne</option>
									<option value="88">88 - Vosges</option>
									<option value="89">89 - Yonne</option>
									<option value="90">90 - Territoire de Belfort</option>
									<option value="91">91 - Essonne</option>
									<option value="92">92 - Hauts-de-Seine</option>
									<option value="93">93 - Seine-Saint-Denis</option>
									<option value="94">94 - Val-de-Marne</option>
									<option value="95">95 - Val-d'Oise</option>
									<option value="971">971 - Guadeloupe</option>
									<option value="972">972 - Martinique</option>
									<option value="973">973 - Guyane</option>
									<option value="974">974 - La Réunion</option>
									<option value="976">976 - Mayotte</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">

					<div class="card">
						<div class="card-body p-4">
							<h5 class="card-title">Session</h5>
							
							<p class="card-text">Sélectionner la session à laquelle vous voudriez vous inscrire</p>

							<c:forEach items="${formation.prochainesSessions}" var="session">
								<ul class="list-group">
									<li class="list-group-item p-2">
										<div class="form-check">
											<input required name="sessionId" class="form-check-input"
												type="radio" name="choixSession" value="${session.id}"
												id="session-${session.id}"> <label
												class="form-check-label" for="session-${session.id}">
												${session.dateDebut} - ${session.dateFin} </label>
										</div>
									</li>
								</ul>
							</c:forEach>
							<div class="mt-5">
							<c:if test="${sessionScope.test.equals('testValide')}">
								<p class="text-danger"> Vous avez réussi le test et vous pouvez valider votre inscription à cette formation </p>
							</c:if>
							<c:if test="${sessionScope.test.equals('testNonPasse') || sessionScope.test.equals('testRate')}">
								<p class="text-danger"> Vous devez d'abord réaliser le test ci dessous pour pouvoir valider votre inscription</p>
							</c:if>
							
					<input class="btn btn-primary" type="submit"
						value="Valider l'inscription" <c:if test="${sessionScope.test.equals('testNonPasse') || sessionScope.test.equals('testRate')}"> disabled </c:if>/>
					
					</div>
						</div>
					</div>

				</div>

			</div>
		
			</div>
		</form>
		
		<div class="row">
				<div class="col-md-8 col-sm-12"></div>
				<div class="col-md-4 col-sm-12">
		
		<c:if test="${sessionScope.test.equals('testRate')}">
			<p style="color:#FF0000">Vous avez déjà réalisé le test le ${dateDuTest} et vous n'êtes pas admissible pour cette formation, vous pourrez le repasser 2mois après votre tentative </p>
		</c:if>
		<div class="row">
				<div class="col-md-8 col-sm-12"></div>
		<form method="get" action="test">
					<input type="hidden" value="${formation.id}" name="formationID">
					<input class="btn btn-primary" type="submit"
						value="Passer le test" <c:if test="${sessionScope.test.equals('testValide')|| sessionScope.test.equals('testRate')}"> disabled </c:if>/>
		</form>
		</div>
		</div>
		
		
	</div>

	<div class="container">
		<div class="m-5"></div>
	</div>
	<%@ include file="footer.jspf"%>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>
</body>
</html>
