<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ajouter Formation</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script src="https://kit.fontawesome.com/25ea7e5f39.js"
	crossorigin="anonymous"></script>
<link href="ajouterFormation.css" rel="stylesheet">
<link href="general.css" rel="stylesheet">


<title>Ajouter une formation</title>
</head>
<body>

	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1">
			<%@ include file="header.jspf"%>
			<div class="container main-content p-4">
				<div class="m-5">
					<%@ include file="messages.jspf"%>
				</div>

				<div class="m-5">
					<h1>Nouvelle formation</h1>
				</div>

				<div class="container">
					<div class="m-5 col-md-6">
						<form action="ajouterFormation" method="post">

							<fieldset>
								<legend>Informations de la formation</legend>


								<div class="mb-3">
									<label for="titre">Titre de la formation</label>
									<!-- 						** -->
									<input name="titre" class="form-control" id="titre"
										placeholder="Saisir le titre de la formation">
								</div>
								<div class="mb-3">
									<div>
										<label for="domaine">Domaine de la formation</label>
									</div>
									<select class="form-select" name="domaine" id="domaine">

										<option value="" disabled selected>Sélectionner un
											domaine</option>
										<c:forEach items="${domaines}" var="domaine">
											<option name="domaine" value="${domaine.id}">${domaine.nom}</option>
										</c:forEach>
									</select>
								</div>
								<div class="mb-3">
									<label for="description">Description</label>
									<textarea rows="6" cols="80" class="form-control"
										id="description" name="description"></textarea>
								</div>

								<div class="mb-3">
									<label for="objectifs">Objectifs</label>
									<textarea rows="4" cols="80" class="form-control"
										id="objectifs" name="objectifs"></textarea>
								</div>

								<div class="mb-3">
									<label for="prerequis">Pré-requis</label>
									<textarea rows="2" cols="80" class="form-control"
										id="prerequis" name="prerequis"></textarea>
								</div>

								<div class="mb-3">
									<label for="niveauExige">Niveau Exigé pour suivre cette
										formation</label>

									<!-- 							** -->
									<input type="number" class="form-control" required
										id="niveauExige"
										placeholder="Saisir le niveau exigé pour suivre la formation"
										name="niveauExige">
								</div>
								<div class="mb-3">
									<label for="prix">Prix de la formation</label> <input
										type="number" required class="form-control" id="prix" min="0"
										placeholder="Saisir le prix de la formation" name="prix">
								</div>
							</fieldset>
							<fieldset class="mb-4">
								<legend>Modules</legend>


								<div class="mb-3">
									<div>
										<label for="l-m-d">Sélectionner les modules de la
											formation</label>
									</div>
									<select id="l-m-d" class="form-select" multiple name="modules"
										aria-label="multiple select example">
										<option selected>Sélectionner des modules</option>
										<c:forEach items="${modules}" var="module">
											<option value="${module.id}">${module.titre}</option>
										</c:forEach>
									</select>
								</div>

							</fieldset>
							<div class="">
								<div class="">
									<button type="submit" class="btn btn-primary btn-lg btn-block"">Ajouter</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- <!%@ include file="footer.jspf"%> -->
</body>

</html>