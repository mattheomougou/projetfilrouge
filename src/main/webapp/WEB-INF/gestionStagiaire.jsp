<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Gestion Stagiaires</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>


	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1">
			<%@ include file="header.jspf"%>

			<div class="container main-content p-5">

				<div class="d-flex mt-3 mb-3 align-items-center">
					<div class="flex-grow-1">
						<div>
							<h1>Gestion des Stagiaires</h1>
						</div>
						<div>
							<%@ include file="messages.jspf"%>
						</div>
					</div>
					<!-- 					<div class=""> -->
					<!-- 						<div> -->
					<!-- 							<a class="btn btn-primary" href="ajouterFormation"> <span -->
					<!-- 								class="mobi-mbri mobi-mbri-edit mbr-iconfont mbr-iconfont-btn"></span> -->
					<!-- 								Ajouter une formation -->
					<!-- 							</a> -->
					<!-- 						</div> -->
					<!-- 					</div> -->
				</div>

				<c:forEach items="${stagiaires}" var="stagiaire">

					<div class="mt-3">
						<div class="card bg-light">
							<div class="card-body">

								<div class="p-3">

									<div class="d-flex align-items-center mb-1 mt-1">
										<div class="flex-grow-1 d-flex align-items-center">
											<h5 class="card-title text-primary">
												<span class="fs-6  mobi-mbri mobi-mbri-user mbr-iconfont"></span>
												<span class="ms-1">${stagiaire.prenom}
													${stagiaire.nom}</span>
											</h5>
										</div>

									</div>
									<div class="row">
										<div class="col-6">

											<div class="mt-3 mb-1">
												<h6 class="card-title d-flex align-items-center">
													<span class="mobi-mbri mobi-mbri-info mbr-iconfont"></span>
													<strong class="ms-1">Coordonées</strong>
												</h6>
											</div>
											<div class="mt-3">
												<span class="mobi-mbri mobi-mbri-letter mbr-iconfont"></span>
												<span class="ms-1">${stagiaire.mail}</span>
											</div>
											<div class="mt-2">
												<span class="mobi-mbri mobi-mbri-mobile mbr-iconfont"></span>
												<span class="ms-1">${stagiaire.telephone}</span>
											</div>


										</div>
										<div class="col-4">

											<div class="mt-3">
												<h6 class="card-title d-flex align-items-center">
													<span class="mobi-mbri mobi-mbri-features mbr-iconfont"></span>
													<strong class="ms-1">Adresse</strong>
												</h6>
											</div>
											<div class="mt-3">
												
												<p>${stagiaire.adresse}</p>
												<p>${stagiaire.codePostal} ${stagiaire.ville}</p>
											</div>
								
										</div>
									</div>
								</div>

								<div class="d-flex mt-2 p-2 justify-content-end">
									<div class="me-2 d-flex">
										<form method="post" action="gestionStagiaire">
											<div class="d-flex">
											
												<input type="hidden" value="${stagiaire.id}"
													name="idStagiaire" />
												<button class="btn btn-secondary btn-sm flex-grow-1 btn-sm"
													type="submit">

													<span
														class="mobi-mbri mobi-mbri-trash mbr-iconfont mbr-iconfont-btn"></span>
													Supprimer
												</button>


											</div>
										</form>
									</div>
									<!-- 									<div class="me-2 d-flex"> -->
									<!-- 										<a target="_blank" -->
									<!-- 											class="btn btn-dark  btn-sm m-0 btn-sm flex-grow-1" -->
									<%-- 											href="pageFormation?id=${formation.id}"> <span --%>
									<!-- 											class="mobi-mbri mobi-mbri-info mbr-iconfont mbr-iconfont-btn"></span> -->
									<!-- 											Information sur le formation -->
									<!-- 										</a> -->
									<!-- 									</div> -->
									<!-- 									<div class="me-2 d-flex"> -->

									<!-- 										<a class="btn btn-dark  btn-sm flex-grow-1 btn-sm m-0" -->
									<%-- 											href="${pageContext.request.contextPath}/ajouterSession?formationId=${formation.id}"> --%>
									<!-- 											<span -->
									<!-- 											class="mobi-mbri mobi-mbri-calendar mbr-iconfont mbr-iconfont-btn"></span> -->
									<!-- 											Planifier une Session -->
									<!-- 										</a> -->
									<!-- 									</div> -->

								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>


		</div>

	</div>
</body>
</html>