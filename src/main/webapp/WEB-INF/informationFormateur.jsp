<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Information employ�</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jspf" %>


<div class ="container">
<div class = "m-5">
<div class="card w-50">
  <div class="card-body">
    <h5 class="card-title">Information sur : ${formateur.nom} ${formateur.prenom}</h5>
    <p class="card-text">Email : ${formateur.mail}</p>
    <p class="card-text">Tel : ${formateur.telephone}</p>
    <p class="card-text">Adresse: ${formateur.adresse} ${formateur.codePostal} ${formateur.ville}</p>
    <p class="card-text">Date d'embauche : ${formateur.dateEmbauche}</p>
     <p class="card-text">Date d'inscription : ${formateur.dateInscription}</p>
    <a class="btn btn-primary" href="${pageContext.request.contextPath}/gestionFormateur">Retour</a>
   </div>
   </div>
  </div>
</div>

<%@ include file="footer.jspf" %>
</body>
</html> 