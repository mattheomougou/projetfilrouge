<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Information</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>
	<%@ include file="header.jspf"%>
	<link href="general.css" rel="stylesheet">


	<div class="container mt-5">
		<c:if test="${!empty sessions}">

			<div class="mb-3">
				<h5 class="card-title">Information sur les formations sur
					lesquelles vous �tes inscrit :</h5>
			</div>
			<c:forEach items="${sessions}" var="session">
				<div class="mb-2">
					<div class="text-center ">
						<div class="pt-2 pb-2 bg-light">
							<div class="">
								<div class="card-body">

									<p class="card-text">${session.titre}</p>
									<p class="card-text">Date de d�but : ${session.dateDebut}</p>
									<p class="card-text">Date de fin: ${session.dateFin}</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</c:forEach>
		</c:if>

		<c:if test="${empty sessions}">
			<div class="mt-5 mb-5">
				<h5>Mes Formations</h5>
			</div>
			<div class="col-sm-12 text-center ">
				<div class="pt-5 pb-5 bg-light">
					<div class="mb-5 mt-5">


						<h5 class="card-title">Vous �tes inscrit � aucune formation</h5>


					</div>
				</div>
			</div>
		</c:if>
		<div class="mb-2 mt-2"></div>
		<a class="m-0 btn btn-primary"
			href="${pageContext.request.contextPath}/accueil">Retour</a>
		<div class="mb-2 mt-2"></div>
	</div>

	<div class="mb-5 mt-5"></div>
	<div class="mb-5 mt-5"></div>
	<%@ include file="footer.jspf"%>
</body>
</html>
