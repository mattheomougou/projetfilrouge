<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Gestion formateur</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>
	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1 vh-100">
			<%@ include file="header.jspf"%>
			<div class="container main-content p-4">

				<div class="m-5">
					<h1>Gestion des Formateurs</h1>
				</div>
				
				<p class="m-5">${messageSucces}</p>

				<div class="container m-5">
					<c:forEach items="${formateurs}" var="formateur">

						<div class="mb-2">
							<div class="card w-75">
								<div class="card-body">
									<div class="row no-gutters">
										<div class="col-sm-7">
											<h4 class="card-title">${formateur.nom.toUpperCase()}
												${formateur.prenom}</h4>
											<p class="card-text">Email : ${formateur.mail}</p>
											<p class="card-text">Tel: ${formateur.telephone}</p>
										</div>
										<div class="col-sm-5">
										<form class="align-items-center" method="post"
												action="informationFormateur">
												<input type="hidden" value="${formateur.id}"
													name="idFormateur" /> <input class="btn btn-primary "
													type="submit" value="Information sur le formateur" />
											</form>
											</br>
											<form method="post" action="gestionFormateur">
												<input type="hidden" value="${formateur.id}"
													name="idFormateur" /> <input class="btn btn-primary"
													type="submit" value="Supprimer le formateur" />
											</form>
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>

			</div>
		</div>
	</div>
</body>
</html>