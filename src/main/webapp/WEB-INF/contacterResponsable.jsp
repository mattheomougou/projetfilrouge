<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Contacter responsable</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
<link href="test.css" rel="stylesheet">

</head>


<body>
	<%@ include file="header.jspf"%>
	<section data-bs-version="5.1" class="contact2 cid-sZ8LcN5g4R"
		id="contact2-4d">





		<div class="container">
			<div class="row content-wrapper justify-content-center">
				<div class="col-lg-3 offset-lg-1">
					<div class="col-lg-12 col-md-12 col-sm-12 left-side">
						<h3 class="mbr-section-title mbr-fonts-style display-2">
							<strong>Contact</strong>
						</h3>
						<p class="mbr-fonts-style description display-4">Une question,
							un projet de formation ? N'hésitez pas à nous contacter.</p>
						<p class="mbr-fonts-style contacts-name display-4">EMAIL:</p>
						<p class="mbr-fonts-style contacts-info display-4">it.training@gmail.com</p>
						<p class="mbr-fonts-style contacts-name display-4">TELEPHONE:</p>
						<p class="mbr-fonts-style contacts-info margin display-4">02
							23 45 69 60</p>
						<div class="col-12 col-md-auto mbr-section-btn">
							<a type="submit" class="btn gray-button display-4"> <span
								class="mobi-mbri mobi-mbri-map-pin mbr-iconfont mbr-iconfont-btn"></span>&nbsp;Adresse
							</a>
						</div>
					</div>
				</div>

				<div class="col-lg-7 offset-lg-1 col-12" data-form-type="formoid">
					<form action="essalhi.khaoula.mii@gmail.com" method="POST"
						class="mbr-form form-with-styler" data-form-title="Form Name">
						<input type="hidden" name="email" data-form-email="true"
							value="JNS5RltxD95SXl1mPtGxVZJhO0sSsnXxW9f08WMU6Gap8ZFvYMcwtTP/YUEMBoS7yM6NNTm6UPthY/zsTDilzmX3GnnCd5F2pDpEylRcRM4J4MBrP7JR+Ru2TLYcw838.br8011t7mNf+sOQXqchyWYsvWFXsHr/i8wVEIdGRb80wF8V3QtS5LI0WrzJ2aRfrtg4wOjenZ2lknPLYGft/FVaOaSVudiMNXSKtQQCuWsXhJp9peZpTrhs7gn2a68pK">
						<div class="row">
							<div hidden="hidden" data-form-alert=""
								class="alert alert-success col-12">Merci d'avoir rempli
								notre formulaire! Nous vous répondrons sous 24h.</div>
							<div hidden="hidden" data-form-alert-danger=""
								class="alert alert-danger col-12">Oops...! some problem!</div>
						</div>

						<div class="dragArea row">
							<div class="col-lg-12 col-md-12 col-sm-12 form-heading">
								<h2 class="title mbr-section-title display-5">
									<strong>Nos équipes sont mobilisées pour répondre au
										mieux à vos attentes.&nbsp;&nbsp;<a href=""
										class="text-primary">Créons ensemble!</a>
									</strong>
								</h2>
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 form-group mb-3"
								data-for="name">
								<label for="name-contact2-4d"
									class="form-control-label mbr-fonts-style display-4">Votre
									Nom (requis)</label> <input type="text" name="name" placeholder="Name"
									data-form-field="name" required="required"
									class="form-control display-7" value="" id="name-contact2-4d">
							</div>
							<div data-for="email"
								class="col-lg-6 col-md-12 col-sm-12 form-group mb-3">
								<label for="email-contact2-4d"
									class="form-control-label mbr-fonts-style display-4">Votre
									Email (requis)</label> <input type="email" name="email"
									placeholder="Email" data-form-field="email"
									class="form-control display-7" required="required" value=""
									id="email-contact2-4d">
							</div>
							<div data-for="phone"
								class="col-lg-6 col-md-12 col-sm-12 form-group mb-3">
								<label for="phone-contact2-4d"
									class="form-control-label mbr-fonts-style display-4">Numéro
									de Télephone</label> <input type="tel" name="phone"
									placeholder="Telephone" data-form-field="phone"
									class="form-control display-7" value="" id="phone-contact2-4d">
							</div>
							<div data-for="number"
								class="col-lg-6 col-md-12 col-sm-12 form-group mb-3">
								<label for="number-contact2-4d"
									class="form-control-label mbr-fonts-style display-4">Choisir
									un Budget&nbsp; formation (EUR)</label> <input type="text"
									name="number" placeholder="Votre Budget"
									data-form-field="number" class="form-control display-7"
									value="" id="number-contact2-4d">
							</div>
							<div data-for="message"
								class="col-lg-12 col-md-12 col-sm-12 form-group mb-3" style="">
								<label for="message-contact2-4d"
									class="form-control-label mbr-fonts-style display-4">Votre
									message</label>
								<textarea name="message" placeholder="Message"
									data-form-field="message" class="form-control display-7"
									id="message-contact2-4d" required></textarea>
							</div>
							<div class="col-auto" style="">
								<button type="submit" class="btn btn-primary display-4">Contactez-nous!</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>




	<%@ include file="footer.jspf"%>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>

</body>
</html>