<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>connectEvaluation</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
<%@ include file="header.jspf" %>
</head>

<body>
<div class="container">
<div class = "m-5">
<h1>Evaluation de fin de formation</h1>

<form action="connexionEvaluation" method="post" class = "mt-5">
	<div class="form-group col-md-4">
			<label for="nom">N° de session :</label>
			<input type ="text" name="nom" class="form-control" id="nom" value="${empty erreurs.get('nom') ? param.nom : ''}" placeholder="Saisir votre numéreo de session" required><div class="text-danger">${erreurs.get('nom')}</div></br>
	</div>
	
	<div class="form-group col-md-4">
			<label for="prenom">Date de début de session :</label>
			<input type ="date" name="dateSession" class="form-control" id="dateSession" value="${empty erreurs.get('prenom') ? param.prenom : ''}"  required><div class="text-danger">${erreurs.get('prenom')}</div></br>
	</div>
<input class="btn btn-primary" type="submit" value="Entrer" />
</form>
</div>
</div>
<%@ include file="footer.jspf" %>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</html>