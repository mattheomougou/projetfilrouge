<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connexion</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>

	<%@ include file="header.jspf"%>
	<div class="container">
		<div class="m-5">

			<h2 class="text-center">Conditions Générales d'utilisation</h2>


			<div class="mt-5">
				<h4>1. Généralités ; champ d'application</h4>
				a. Les présentes Conditions d'utilisation de IT-Training,12 Rue du
				Patis Tatelin, 35700 Rennes, FRANCE (ci-après « IT-Training » ou «
				nous ») s'appliquent à l'accord contractuel (ci-après dénommé l'«
				Accord ») conclu entre IT-Training d'une part et les utilisateurs
				(ci-après dénommés les « Utilisateurs » ou « vous ») d'autre part,
				qui sont intéressés par ou utilisent tout service de développement
				de carrière proposé sur www.it-training.fr ou sur d'autres sites
				web qui intègrent les présentes Conditions d'utilisation (ci-après
				dénommés collectivement les « Services »). Ces sites web sont
				collectivement dénommés les « Plateformes IT-Training » dans les
				présentes. Si vous êtes un organisme, les présentes Conditions
				d'utilisation ne sont pas applicables mais vous êtes soumis à nos
				Conditions Générales de Ventes. Des conditions supplémentaires, dont
				nous vous informerons séparément, peuvent s'appliquer à certains
				Services. Veuillez lire attentivement les présentes Conditions
				d'utilisation. Si vous n'acceptez pas nos Conditions d'utilisation,
				vous n'êtes pas autorisé à utiliser nos Services.<br/> b. Les Services
				couverts par les présentes Conditions d'utilisation ne sont destinés
				qu'à la recherche de prestation individuelle par des personnes
				physiques ou morales recherchant une formation ou un accompagnement
				individuel en tant qu'employé, demandeur d'emploi ou travailleur
				indépendant. Il n'est pas permis d'utiliser les Services que nous
				proposons à des fins commerciales, par exemple pour promouvoir les
				services d'une entreprise. c. Les présentes Conditions générales
				d'utilisation peuvent être téléchargées, enregistrées et imprimées
				via http://www.it-Training.fr/protection-de-la-vie-privee.html <br/><br>
				<h4>2. Objet de l'Accord et champ d'application des Services</h4>
				IT-Training propose divers services de développement de carrière
				par l'intermédiaire des Plateformes. IT-Training vise à vous
				assister tout au long de votre carrière. Si votre perspective de vie
				change, si vous développez ou atteignez de nouveaux objectifs, IT-Trainingn vous apportera une aide adaptée. Notre objectif est
				d'établir une relation durable, en proposant un Service, disponible
				7 jours sur 7, 24 heures sur 24, vous permettant de prendre votre
				vie et votre avenir en main. Vous pouvez utiliser certains Services
				sans vous inscrire. Pour vous fournir les Services spécifiques
				auxquels votre inscription vous donne droit ou pour en calculer la
				durée, nous conserverons la date de votre dernière activité sur les
				Plateformes ICI Formation (« Date de dernière activité »). Il peut
				s'agir de la date de votre inscription et de la dernière fois que
				vous avez utilisé des Services IT-Training, par exemple en
				cliquant sur des liens figurant dans la lettre d'information IT-Trainingn ou par toute autre action indiquant que vous êtes toujours
				intéressé par l'utilisation de nos Services. a. Services disponibles
				sans inscription Vous pouvez utiliser tous les services qui sont
				disponibles sur notre site Web sans inscription à l'exception d'une
				demande de mise en relation directe avec un client de la plateforme.
				Vous pouvez notamment consulter les offres de formation publiées sur
				notre site web. b. Mise en relation Nous demandons aux personnes qui
				désirent se mettre en relation avec les Organismes référencés par
				ICI Formation de s'inscrire en ligne en nous fournissant certaines
				données par l'intermédiaire d'un formulaire. Les données que
				l'utilisateur complète dans les champs correspondant aux nom,
				prénom, adresse email, téléphone et statut sont appelées
				"coordonnées d'inscription" et sont nécessaires à l'inscription (les
				autres champs sont facultatifs). En complétant ces données en ligne
				et en validant, l'utilisateur autorise ICI Formation à conserver ces
				informations dans une base de données. A défaut, l'utilisateur ne
				pourra pas recevoir de communications de la part d'ICI Formation ni
				être mis en relation avec l'Organisme choisi. L'utilisateur peut
				demander à tout moment que ses coordonnées d'inscription soient
				rectifiées ou supprimées en écrivant à l'adresse suivante:
				dataprotection@iciformation.fr.<br> 2.b.1 Si vous souhaitez rentrer en
				contact avec un organisme, vous devez nous fournir une adresse de
				courrier électronique valide, que nous stockerons.<br> 2.b.2 Après avoir
				reçu votre demande de mise en relation, nous confirmerons la
				réception de cette demande par un message envoyé à l'adresse de
				courrier électronique que vous nous avez fournie. L'accord entre
				vous et nous concernant votre mise en relation prend effet lorsque
				nous activons votre mise en relation. Le fait que vous procédiez à
				l'inscription ne constitue qu'une offre de conclure un accord. ICI
				Formation se réserve le droit d'accepter ou de refuser la conclusion
				de l'accord à sa propre discrétion. Il n'existe aucun droit à la
				conclusion d'un accord. <br>2.b.3 Vous devez fournir les informations et
				les données demandées de façon complète et honnête. Vous êtes
				responsable de tenir vos coordonnées (en particulier votre adresse
				de courrier électronique) à jour et de vous assurer que vous pouvez
				être contacté au moyen de l'adresse fournie et que l'adresse de
				courrier électronique n'est pas transférée à une autre personne. En
				cas de modification des données communiquées, vous devez
				immédiatement les corriger. Si vous nous fournissez des informations
				incomplètes ou fausses, ICI Formation a le droit de bloquer votre
				demande de mise en relation et l'accès à notre plateforme
				temporairement ou définitivement et/ou de résilier le présent
				Accord, conformément à la clause 9des présentes.<br> 2.b.4 Chaque
				Utilisateur est autorisé à réaliser plusieurs mises en relation,
				sous réserve que le nombre de demandes envoyées est raisonnable. c.
				Lettre d'information ICI Formation Vous pouvez vous abonner aux
				diverses lettres d'information que nous vous proposons sur
				différents sujets. Vous pouvez à tout moment vous opposer
				gratuitement à toutes les lettres d'information de ICI Formation.
				Nous vous informerons de votre droit d'opposition lors de la
				collecte de votre adresse de courrier électronique et dans la lettre
				d'information concernée. Si vous ne souhaitez plus recevoir
				d'e-mails de notre part, veuillez cliquer sur le lien de
				désabonnement situé dans chaque message et/ou nous contacter à
				dataprotection@iciformation.fr.<br>
				<h4>3. Obligations et responsabilités de l'Utilisateur</h4>
				a. Les Utilisateurs assument l'entière responsabilité de la
				fourniture du matériel et de la connexion Internet nécessaires à
				l'utilisation de nos Services. b. Veuillez vous assurer que vous
				seul utilisez les Services associés à vos mises en relation ICI
				Formation. Vous êtes responsable de toutes les actions qui se
				produisent dans le contexte de l'utilisation du service ICI
				Formation. c. Vous ne devez pas agir illégalement dans le cadre de
				l'utilisation de nos Services ni enfreindre une loi applicable, plus
				précisément vous ne devez pas : nous communiquer un contenu, quel
				qu'il soit, qui - est diffamatoire, offensant, susceptible de
				corrompre les jeunes ou illégal de quelque façon que ce soit ; -
				enfreint les droits de tiers, en particulier les lois sur le droit
				d'auteur, les droits de la personnalité ou d'autres lois sur la
				propriété ; - est confidentiel, p. ex. comprend des secrets
				d'affaires ; vous faire passer pour une autre personne, par exemple
				un agent public, représentant de notre société, ou prétendre
				l'existence d'une relation avec de telles personnes qui n'existe
				pas, falsifier des noms d'utilisateur ou manipuler des marques
				distinctives d'une autre façon et/ou déguiser l'origine du contenu,
				qui est transmis dans le contexte du Service ; enregistrer, publier
				et/ou transmettre des e-mails publicitaires, indésirables ou de
				masse, des chaînes de lettres, des systèmes boules de neige ou
				d'autres communications commerciales ; utiliser des techniques de
				prélèvement ou similaires, pour collecter du contenu pour l'utiliser
				à d'autres fins, le publier à nouveau ou l'utiliser d'une manière
				différente de celle à laquelle les Services sont destinés ; utiliser
				des techniques ou d'autres Services automatisés qui visent à
				afficher ou ont pour conséquence l'affichage incorrect de l'activité
				des utilisateurs, par exemple en utilisant des robots de recherche,
				réseaux de zombies, scripts, applications, plugins, extensions ou
				autres instruments automatisés pour enregistrer des comptes, lire du
				contenu, envoyer des messages, poster des commentaires ou autres
				actes ; harceler, menacer, insulter, diffamer une personne physique
				ou une entreprise, dénigrer ou discriminer en raison du sexe, de la
				race, de la religion ou des croyances, ou énoncer ou diffuser des
				faits inexacts au sujet d'une personne physique ou morale ;
				collecter, sauvegarder ou transmettre les données personnelles d'un
				autre utilisateur, dans la mesure où la personne concernée n'a pas
				donné son accord ; afficher des liens vers du contenu de tiers qui
				enfreint les règles des présentes Conditions d'utilisation ou
				d'autres règlements. d. Toute information ou contenu que vous nous
				communiquez (« Contenu ») doit être vrai. Cela signifie que vous
				n'êtes pas autorisé à créer des demandes de mise en relation pour
				des tiers sans leur consentement ou pour des personnes inexistantes.

				e. Le Contenu que vous nous communiquez ne doit pas contenir de
				virus, de vers ou tout autre code nuisible. Les données personnelles
				que vous envoyez ou fournissez à ICI Formation ne doivent inclure,
				et vous garantissez qu'elles ne comprennent, aucun des éléments
				suivants : origine raciale ou ethnique, opinions politiques,
				croyances religieuses ou philosophiques, appartenance syndicale,
				données génétiques, données biométriques aux fins de
				l'identification unique d'une personne physique, données relatives à
				la santé données concernant la vie sexuelle ou l'orientation
				sexuelle d'une personne physique f. Si vous transmettez des données
				personnelles à d'autres utilisateurs, cette transmission se fait à
				vos propres risques. ICI Formation ne peut pas contrôler
				l'utilisation réelle de vos données personnelles par les
				destinataires. Par conséquent, veuillez vérifier l'identité des
				destinataires de vos données avant de les envoyer. Par exemple, vous
				ne devez pas envoyer vos données personnelles à une adresse de
				courrier électronique privée.<br>
				<h4>4. Licence relative au contenu de l'Utilisateur</h4>
				a. Vous pouvez utiliser nos Services et nous transmettre du Contenu
				conformément aux présentes Conditions d'utilisation. Nous
				n'examinons pas ce Contenu et n'en revendiquons pas la propriété. En
				fournissant du Contenu à ICI Formation, vous nous confiez
				l'enregistrement, l'hébergement et, le cas échéant, la mise à la
				disposition du public de ce Contenu. Nous utilisons votre Contenu
				conformément à l'Accord entre vous et nous en vertu des présentes
				Conditions d'utilisation. En fournissant votre Contenu à ICI
				Formation, vous accordez une licence non exclusive, sans restriction
				territoriale pour la durée de l'Accord, d'utiliser le Contenu aux
				fins de la fourniture des Services dans le cadre de votre Accord
				avec nous, ce qui inclut le droit d'enregistrer, de reproduire, de
				formater, de reformater, d'éditer techniquement, de transmettre, de
				rendre disponible et d'analyser le Contenu pour qu'il soit analysé
				et/ou évalué statistiquement par nous-mêmes ou par des tiers. b.
				Vous certifiez posséder tous les droits ou la licence relative au
				Contenu qui sont nécessaires pour accorder les droits conformément à
				la section 5 a à ICI Formation, et que le Contenu et son utilisation
				par ICI Formation en vertu de l'Accord conclu avec vous n'enfreint
				pas nos Conditions d'utilisation ou les réglementations légales
				applicables et n'enfreint pas les droits de propriété intellectuelle
				ou d'autres droits de tiers.<br>
				<h4>5. Tarifs</h4>
				Les Services de ICI Formation couverts par les présentes Conditions
				d'utilisation sont gratuits.<br>
				<h4>6.Absence de garantie ; Copies de sauvegarde</h4>
				a. Nous ne donnons aucune garantie quant à l'actualité,
				l'exactitude, l'exhaustivité, la facilité d'utilisation ou
				l'adéquation à un certain but du contenu sur nos Plateformes. Nous
				ne donnons non plus aucune garantie quant à la signature d'un
				contrat de formation ou accompagnement entre les Utilisateurs de nos
				Plateformes en tant que stagiaire et un organisme. b. ICI Formation
				ne garantit pas que les Services que nous offrons sont disponibles à
				certains moments ou de façon permanente. Des perturbations, des
				interruptions ou des temps d'arrêt éventuels du Service (en ligne)
				ne peuvent pas être exclus. Pour des raisons techniques ou
				opérationnelles, une limitation temporelle de la disponibilité est
				possible. C'est notamment possible en ce qui concerne les limites de
				capacité, la sécurité ou l'intégrité des systèmes de traitement des
				données ou la mise en œuvre de mesures techniques, qui sont
				nécessaires à une performance régulière ou améliorée. Par
				conséquent, vous acceptez que les Plateformes et Services ICI
				Formation et leur contenu vous soient fournis « tels quels », sans
				aucune garantie d'aucune sorte. c. Les serveurs de ICI Formation
				sont sauvegardés régulièrement et soigneusement. Néanmoins, des
				pertes de données ne sont pas exclues. Dans la mesure où vous
				transmettez ou téléchargez des données, sous quelque forme que ce
				soit, nous vous conseillons de faire des copies de sauvegarde. Cette
				recommandation s'applique également au cas où vous résiliez votre
				accord avec nous, parce que nous ne conservons pas de copie de vos
				données après la suppression, sous réserve des seules obligations
				légales de stockage.<br>
				<h4>7. Responsabilité</h4>
				a. ICI Formation ne peut être tenue responsable des dommages
				indirects, accidentels ou consécutifs résultant de l'utilisation des
				Plateformes et Services de ICI Formation. b. Nous sommes
				responsables selon la loi des seuls dommages prévisibles (sauf (iii)
				ci-dessous) (i) résultant d'une atteinte à la vie, au corps ou à la
				santé découlant d'un manquement à une obligation de la part de ICI
				Formation, d'un représentant légal ou d'un agent de ICI Formation,
				(ii) résultant de l'absence d'une condition garantie par ICI
				Formation, (iii) causés par intention ou négligence grave, également
				par nos représentants légaux et agents, ainsi que (iii) par nos
				représentants légaux et agents, ainsi que (iv) causés par une
				conduite malveillante de ICI Formation. c. En cas de dommages
				matériels ou pécuniaires causés par un comportement légèrement
				négligent de ICI Formation, d'un représentant légal ou d'un agent de
				ICI Formation, ICI Formation ne sera responsable qu'en cas de
				violation substantielle d'une obligation contractuelle de base, et
				seulement à hauteur du dommage typique prévisible en cas de
				conclusion de l'Accord. Les obligations contractuelles de base sont
				les obligations dont l'accomplissement permet la bonne exécution
				d'un accord et sur le respect desquelles les parties contractantes
				peuvent régulièrement s'appuyer. d. Une éventuelle responsabilité en
				vertu de la loi sur la responsabilité du fait des produits n'est pas
				affectée. e. Toute autre responsabilité de ICI Formation est exclue.
				f. ICI Formation n'est pas responsable selon les dispositions
				légales en cas de négligence légère. g. Dans la mesure où la
				responsabilité de ICI Formation est exclue ou limitée, cette
				exclusion/limitation s'applique également à la responsabilité
				personnelle des représentants légaux, cadres et agents de ICI
				Formation.<br>
				<h4>8. Résiliation</h4>
				a. L'Accord conclu avec vous en ce qui concerne les Mises en
				relation et les Services fournis en vertu de celles-ci ainsi que
				pour la Lettre d'information est conclu pour une durée illimitée.
				L'une ou l'autre partie peut résilier le présent Accord par écrit
				avec effet immédiat. b. Lorsque l'Accord est résilié, nous
				supprimons toutes vos données personnelles que nous avons stockées
				ou stockons sous une forme non personnelle, à moins que leur
				traitement ne soit légal ou nécessaire à d'autres fins.<br>
				<h4>9. Droits de propriété intellectuelle relatifs à nos
					Plateformes</h4>
				a. Nos Plateformes et tous les logiciels, bases de données,
				graphiques, interfaces utilisateur, dessins et modèles et contenus
				supplémentaires, noms et marques déposées connexes sont protégés par
				le droit d'auteur, le droit des marques et d'autres droits de
				propriété. Comme convenu entre vous et ICI Formation, ICI Formation
				sera réputée être le seul titulaire de tous les droits de propriété
				intellectuelle. b. Sous réserve que vous vous conformiez aux
				présentes Conditions d'utilisation et à toute autre disposition
				applicable, ICI Formation accorde un droit non exclusif et non
				transférable d'utilisation des Plateformes et du Service. Vous ne
				devez pas (i) utiliser les Plateformes ou le Service pour le
				développement d'autres Services ; (ii) activer ou utiliser les
				fonctionnalités des Plateformes ou du Service pour lesquelles vous
				n'avez aucun droit d'utilisation ; (iiii) modifier, traduire,
				reproduire, décompiler le code source, pour examiner ses fonctions,
				sauf lorsque cela est autorisé par la loi.
				<h4>10. Confidentialité et protection des données</h4>
				Nous traitons vos données personnelles de manière confidentielle à
				tout moment. Nous ne les utilisons ou les publions que lorsque cela
				est nécessaire en vertu des présentes Conditions d'utilisation.
				Chaque collecte et traitement des données personnelles de
				l'utilisateur sont effectués en tenant compte de la législation
				applicable en matière de protection des données. Vous pouvez obtenir
				de plus amples informations sur l'utilisation des données
				personnelles sur nos Plateformes dans notre politique de
				confidentialité.<br>
				<h4>11. Modification des présentes Conditions d'utilisation</h4>
				a. Modification des Services nécessitant une inscription Nous nous
				réservons le droit de modifier ou d'ajuster les Services qui
				nécessitent une inscription (Mise en relation ou Lettre
				d'information), dans la mesure où (i) ces modifications ou
				ajustements sont uniquement bénéfiques pour les utilisateurs
				enregistrés, ou (ii) ces modifications ou ajustements sont
				nécessaires pour se conformer à la loi applicable, en particulier si
				la situation juridique actuelle change ou pour se conformer à une
				décision judiciaire ou officielle ; ou (iii) ces modifications ou
				ajustements sont sans influence significative sur les fonctions ou
				les Services ou uniquement d'ordre technique ou organisationnel. b.
				Modification des Conditions d'utilisation des Services nécessitant
				une inscription (i) Nous nous réservons le droit de modifier nos
				Conditions d'utilisation des Services qui nécessitent une
				inscription (Mise en relation ou Lettre d'information) à tout
				moment, sauf si cela n'est pas raisonnable pour les utilisateurs.
				Dans le cas d'une telle modification des Conditions d'utilisation,
				nous vous en informerons au moins 4 semaines à l'avance, soit a)
				lors de votre prochaine mise en relation sur un masque d'écran
				correspondant, soit b) par e-mail. Si vous ne nous faites pas part
				de votre désaccord au cours de ce délai de 4 semaines ou si vous
				vous connectez à nouveau, les Conditions modifiées entrent en
				vigueur pour l'avenir et sont intégrées à l'Accord à leur date
				d'entrée en vigueur. Nous vous informerons dans notre notification
				concernant les conditions modifiées de l'effet du silence et la
				signification du délai de 4 semaines. (ii) Dans la mesure où la
				modification des Conditions d'utilisation constitue également une
				modification de la finalité du traitement des données, par laquelle
				les données sont traitées pour une autre finalité que celle pour
				laquelle elles ont été collectées à l'origine, ladite modification
				n'est possible que si elle est légale, conformément aux dispositions
				applicables (y compris article 6, paragraphe 4 du RGPD). En outre,
				nous vous informons à l'avance du respect des dispositions légales.

				(iii) Les modifications concernant l'objet de l'accord et les
				obligations majeures sont exemptées de l'autorisation de
				modification visée à la clause 12b(i). Dans ces cas, ICI Formation
				vous informera des modifications prévues et vous proposera la
				poursuite de la relation contractuelle conformément aux conditions
				modifiées. (iv) En outre, nous nous réservons le droit d'ajuster ou
				de modifier les Conditions d'utilisation des Services qui
				nécessitent une inscription, dans la mesure où la modification ou
				l'ajustement est uniquement bénéfique pour l'utilisateur enregistré,
				ou la modification ou l'ajustement est nécessaire pour établir la
				conformité avec la loi applicable, en particulier lorsque la
				situation juridique actuelle change ou pour se conformer à une
				décision judiciaire ou officielle ; ou les modifications ou
				ajustements sont sans influence significative sur les fonctions ou
				les Services ou uniquement d'ordre technique ou organisationnel. ICI
				Formation introduit des Services ou des éléments de Services
				complètement nouveaux, qui nécessitent une spécification de
				performance dans les Conditions d'utilisation, à moins que cela ne
				soit contraire à la présente relation avec l'utilisateur.<br>
				<h4>12. Droit applicable ; Lieu de juridiction et autres
					stipulations</h4>
				a. Le présent Accord et son interprétation sont régis par le droit
				français. Si vous êtes un consommateur, conformément à l'article 6
				paragraphe 2 du règlement (CE) 593/2008, ce choix du droit
				applicable ne peut cependant pas avoir pour conséquence de vous
				priver de la protection que vous offrent les dispositions
				impératives du droit qui aurait été applicable (en l'absence de
				choix). b. Si une stipulation des présentes Conditions d'utilisation
				est jugée invalide ou inapplicable, en tout ou en partie, cette
				stipulation est réputée dissociable des présentes Conditions
				d'utilisation, et la validité et le caractère exécutoire de toutes
				les autres stipulations des présentes Conditions d'utilisation n'en
				sont pas affectés. Dans ce cas, la disposition invalide ou
				inapplicable sera remplacée par une stipulation légale. c. Avis
				concernant le règlement des litiges : Nous ne sommes ni obligés ni
				disposés à participer aux procédures de règlement des litiges par un
				quelconque conseil d'arbitrage des consommateurs. La Commission
				européenne a mis en place une plateforme de résolution des litiges
				afin de recueillir les plaintes des consommateurs et de les
				transmettre aux médiateurs nationaux concernés. Cette plateforme est
				accessible à l'adresse URL suivante :
				http://ec.europa.eu/consumers/odr. d. En cas de litige, les
				tribunaux de Paris sont seuls compétents.<br /> Date : 26/04/2018
			</div>

		</div>
	</div>
	<%@ include file="footer.jspf"%>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>
</body>
</html>