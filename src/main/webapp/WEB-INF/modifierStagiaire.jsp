<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ajout d'un stagiaire</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>
	<div class="d-flex">
		<div class="flex-grow-1 vh-100">
			<%@ include file="header.jspf"%>
			<div class="container p-4">

				<div class="m-5">
					<h2>Modifications des informations</h2>
				</div>

				<div>
					<%@ include file="messages.jspf"%>
				</div>
				<div class="container">
					<div class="m-5">

						<form action="modifierStagiaire" method="post" class="mt-5">
							<div class="form-group col-md-6">
								<label for="nom">Nom :</label> <input type="text" name="nom"
									class="form-control" id="nom"
									value="${empty erreurs.get(utilisateur.nom) ? utilisateur.nom : ''}"
									placeholder="Saisir votre nom" required>
								<div class="text-danger">${erreurs.get('nom')}</div>

							</div>

							<div class="form-group col-md-6">
								<label for="prenom">Pr�nom :</label> <input type="text"
									name="prenom" class="form-control" id="prenom"
									value="${empty erreurs.get(utilisateur.prenom) ? utilisateur.prenom : ''}"
									placeholder="Saisir votre pr�nom" required>
								<div class="text-danger">${erreurs.get('prenom')}</div>

							</div>


							<div class="form-group col-md-6">
								<label for="mail">Mail :</label> <input class="form-control"
									type="email" id="mail" name="mail"
									value="${empty erreurs.get(utilisateur.mail) ? utilisateur.mail : ''}"
									placeholder="Saisir votre mail" required disabled>
								<div class="text-danger">${erreurs.get('mail')}</div>


							</div>
							<div class="form-group col-md-6">
								<label for="motDePasse">Mot de passe :</label> <input
									class="form-control" type="password" id="motDePasse"
									name="motDePasse" disabled
									
									value="${empty erreurs.get(utilisateur.motDePasse) ? utilisateur.motDePasse : ''}"
									
									 placeholder="Saisir votre mot de passe"
									required>
								<div class="text-danger">${erreurs.get(utilisateur.motDePasse)}</div>

							</div>


							<div class="form-group col-md-6">
								<label for="telephone">T�l�phone :</label> <input type="text"
									name="telephone" class="form-control" id="telephone"
									value="${empty erreurs.get('utilisateur.telephone') ? utilisateur.telephone : ''}"
									placeholder="Saisir votre num�ro de t�l�phone" required>
								<div class="text-danger">${erreurs.get('utilisateur.telephone')}</div>

							</div>
							<div class="form-group col-md-6">
								<label for="adresse">Adresse :</label> <input type="text"
									name="adresse" class="form-control" id="adresse"
									value="${utilisateur.adresse}" placeholder="Saisir votre adresse"><br />
							</div>
							<div class="form-group col-md-6">
								<label for="codePostal">Code postal :</label> <input type="text"
									name="codePostal" class="form-control" id="codePostal"
									value="${utilisateur.codePostal}"
									placeholder="Saisir votre code postal"><br />
							</div>
							<div class="form-group col-md-6">
								<label for="ville">Ville :</label> <input type="text"
									name="ville" class="form-control" id="ville"
									value="${utilisateur.ville}" placeholder="Saisir votre ville"><br />
							</div>

							<!-- <div class="form-group col-md-6">
								<label for="entreprise">Entreprise:</label> <input type="test"
									name="entreprise" class="form-control" id="entreprise"
									value="${sessionScope.entreprise}" ><br />
							</div>
							 -->

							<input class="btn btn-primary" type="submit"
								value="Enregistrer les modifiactions"
								 />
						</form>
						 
						<form action="panier" method="post" class="mt-5">
						
								<input class="btn btn-primary" type="submit"
								value="Retour au panier" />
						</form>
						
						 
						
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>