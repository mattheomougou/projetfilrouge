<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Modifier Formation</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/fontawesome.min.css">


<link href="ajouterFormation.css" rel="stylesheet">


<title>Modifier la formation</title>
</head>
<body>
	<br />
	<br />
		<%@ include file="header.jspf"%>

	<div class="container">
		<div>
			<form action="modifierFormation" method="post">

				<fieldset>
					<legend>Informations de la formation</legend>


					<div class="form-group">
						<label for="titre">Titre de la formation</label> <input
							type="titre" class="form-control" id="titre"
							 value="${param.formation.nom}">
					</div>
					<div class="form-group">
						<label for="domaine">Domaine de la formation</label> 
						<select class="form-select" aria-label="Default select example" >
							<option selected disabled="disabled">Sélectionner le domaine</option>
							<c:forEach items="${domaines}" var="domaine">
								<option value="${param.formation.domaine.nom}" name="domaine" value="1">${domaine.nom}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea rows="6" cols="80" name="description"
							class="form-control" id="description" value="${param.formation.description}"></textarea>
					</div>

					<div class="form-group">
						<label for="objectifs">Objectifs</label>
						<textarea rows="4" cols="80" name="objectifs" class="form-control"
							id="objectifs" value="${param.formation.objectifs}"></textarea>
					</div>

					<div class="form-group">
						<label for="prerequis">Pré-requis</label>
						<textarea rows="2" cols="80" name="prerequis" class="form-control"
							id="prerequis" value="${param.formation.prerequis}"></textarea>
					</div>

					<div class="form-group">
						<label for="niveauExige">Niveau Exigé pour suivre cette
							formation</label> <input type="number" class="form-control"
							id="niveauExige"
							placeholder="Saisir le niveau exigé pour suivre la formation"
							value="${param.formation.niveauExige}">
					</div>
					<div class="form-group">
						<label for="prix">Prix de la formation</label> <input
							type="number" class="form-control" id="prix"
							value="${param.formation.prix}">
					</div>
				</fieldset>
				<fieldset class="mb-4">
					<legend>Modules</legend>


					<div class="form-group">
						<label for="l-m-d">Sélectionner les modules de la formation</label> 
						<select multiple class="form-control" id="l-m-d">
							<c:forEach items="${themes}" var="theme">
								<option name="module" value="1">${theme.nom}</option>
							</c:forEach>
						</select>
					</div>

				</fieldset>
				<div class="row justify-content-center">
					<div class="col-4">
						<button type="submit" class="btn btn-primary btn-lg btn-block"">Modifier</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- Leave those next 4 lines if you care about users using IE8 -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<%@ include file="footer.jspf" %>
</body>

</html>