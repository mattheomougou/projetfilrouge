<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<meta charset="UTF-8">
<title>Panier</title>
</head>
<body>
	<%@ include file="header.jspf"%>

	<div class="row">
		<div class="col-sm-12 pt-5">

			<div class="pt-5 pb-5 bg-light">
				<div class="mb-5 mt-5">
					<div class="mt-5 mb-5 text-center text-success">
						<h1>Votre Inscription a été effectuée avec succès</h1>

					</div>
				</div>
			</div>
			<!--  	
			<div class="mt-5 mb-5 text-center text-success">
				<h1>Votre Inscription a été effectuée avec succès</h1>

			</div>
			-->

			<div class="mt-5 mb-5 text-center">
				<a class="btn btn-primary" href="connect/espaceClient"> Aller à
					mon espace personnel </a>
			</div>
		</div>

	</div>


	<div class="mt-5 mb-5"></div>



	<%@ include file="footer.jspf"%>
</body>
</html>