<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ajout d'un formateur</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>
	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1 vh-100">
			<%@ include file="header.jspf"%>
			<div class="container main-content p-4">

				<div class="m-5">
					<h2>Ajouter un formateur</h2>
				</div>

				<div class="container">
					<div class="m-5">

						<form action="ajouterFormateur" method="post" class="mt-5">
							<div class="form-group col-md-6">
								<label for="nom">Nom :</label> <input type="text" name="nom"
									class="form-control" id="nom"
									value="${empty erreurs.get('nom') ? param.nom : ''}"
									placeholder="Saisir votre nom" required>
								<div class="text-danger">${erreurs.get('nom')}</div>
								</br>
							</div>

							<div class="form-group col-md-6">
								<label for="prenom">Pr�nom :</label> <input type="text"
									name="prenom" class="form-control" id="prenom"
									value="${empty erreurs.get('prenom') ? param.prenom : ''}"
									placeholder="Saisir votre pr�nom" required>
								<div class="text-danger">${erreurs.get('prenom')}</div>
								</br>
							</div>


							<div class="form-group col-md-6">
								<label for="email">Mail :</label> <input class="form-control"
									type="email" id="mail" name="mail"
									value="${empty erreurs.get('mail') ? param.mail : ''}"
									placeholder="Saisir votre mail" required>
								<div class="text-danger">${erreurs.get('mail')}</div>
								<br />

							</div>
							<div class="form-group col-md-6">
								<label for="motDePasse">Mot de passe :</label> <input
									class="form-control" type="password" id="motDePasse"
									name="motDePasse" placeholder="Saisir votre mot de passe"
									required>
								<div class="text-danger">${erreurs.get('motDePasse')}</div>
								<br />
							</div>

							<div class="form-group col-md-6">
								<label for="confirmMdp">Confirmer le mot de passe :</label> <input
									class="form-control" type="password"
									placeholder="Confirmer votre mot de passe"
									id="confirmMotDePasse" name="confirmMotDePasse" required>
								<div class="text-danger">${erreurs.get('confirmMotDePasse')}</div>
								<br />
							</div>

							<div class="form-group col-md-6">
								<label for="telephone">T�l�phone :</label> <input type="text"
									name="telephone" class="form-control" id="telephone"
									value="${empty erreurs.get('telephone') ? param.telephone : ''}"
									placeholder="Saisir votre num�ro de t�l�phone" required>
								<div class="text-danger">${erreurs.get('telephone')}</div>
								<br />
							</div>
							<div class="form-group col-md-6">
								<label for="adresse">Adresse :</label> <input type="text"
									name="adresse" class="form-control" id="adresse"
									value="${param.adresse}" placeholder="Saisir votre adresse"><br />
							</div>
							<div class="form-group col-md-6">
								<label for="codePostal">Code postal :</label> <input type="text"
									name="codePostal" class="form-control" id="codePostal"
									value="${param.codePostal}"
									placeholder="Saisir votre code postal"><br />
							</div>
							<div class="form-group col-md-6">
								<label for="ville">Ville :</label> <input type="text"
									name="ville" class="form-control" id="ville"
									value="${param.ville}" placeholder="Saisir votre ville"><br />
							</div>

							<div class="form-group col-md-6">
								<label for="dateEmbauche">Date d'embauche :</label> <input
									type="date" name="dateEmbauche" class="form-control"
									id="dateEmbauche"><br />
							</div>

							<input class="btn btn-primary" type="submit"
								value="Ajouter le formateur" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>