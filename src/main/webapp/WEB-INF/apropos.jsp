<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT-Training</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png"
	type="image/x-icon">
</head>
<body>


	<%@ include file="header.jspf"%>


	<section data-bs-version="5.1" class="header18 cid-sZeoEjzLuJ"
		id="header18-4z">





		<div class="align-center container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10">
					<h1
						class="mbr-section-title mbr-fonts-style mbr-white mb-3 display-1">
						<strong>Notre Histoire!</strong>
					</h1>

					<p class="mbr-text mbr-fonts-style mbr-white display-7">Dès sa
						création à Paris, en 1985, IT-training s'est positionnée sur le
						marché de la formation informatique en proposant une offre de
						formations informatiques à forte valeur ajoutée. En 1999, IT
						devient filiale du Groupe Cegos, leader européen du management par
						les compétences.&nbsp;Aujourd'hui&nbsp; implantée dans 11 villes
						en France, IT-Training accompagne les entreprises dans la
						définition et la mise en oeuvre de leurs projets informatiques.</p>
					<div class="mbr-section-btn mt-3">
						<a class="btn btn-primary display-4" href="contact">Nous contacter</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1"
		class="counters1 agencym4_counters1 counters cid-sZejAWlwLx"
		id="counters1-4w">



		<div class="mbr-overlay"
			style="opacity: 0.7; background-color: rgb(4, 54, 124);"></div>

		<div class="container mbr-white">



			<div class="container pt-4 mt-2">
				<div class="media-container-row">
					<div class="card p-3 align-center col-12 col-md-6 col-lg-4">
						<div class="panel-item p-3">


							<div class="card-text">
								<h3
									class="count pt-3 pb-3 mbr-semibold mbr-fonts-style text-white display-1">
									166</h3>
								<h4
									class="mbr-content-title mbr-semibold pb-3 mbr-fonts-style display-5">
									Clients</h4>
								<p class="mbr-content-text mbr-fonts-style display-7">
									Partenaires des plus grands éditeurs (Microsoft, VMware, SAP,
									IBM,…) mais aussi d’organismes tels que Axel iOS (ITIL,
									PRINCE2)</p>
							</div>
						</div>
					</div>

					<div class="card p-3 align-center col-12 col-md-6 col-lg-4">
						<div class="panel-item p-3">

							<div class="card-text">
								<h3
									class="count pt-3 pb-3 mbr-semibold mbr-fonts-style text-white display-1">
									210</h3>
								<h4
									class="mbr-content-title mbr-semibold pb-3 mbr-fonts-style display-5">
									Projets</h4>
								<p class="display-7 mbr-content-text mbr-fonts-style">A
									travers son offre de 1 150 séminaires et formations , IT assure
									chaque année la montée en compétences de plus de 30 000
									spécialistes.</p>
							</div>
						</div>
					</div>

					<div class="card p-3 align-center col-12 col-md-6 col-lg-4">
						<div class="panel-item p-3">

							<div class="card-text">
								<h3
									class="count pt-3 pb-3 mbr-semibold mbr-fonts-style text-white display-1">
									357</h3>
								<h4
									class="mbr-content-title mbr-semibold pb-3 mbr-fonts-style display-5">
									Témoignages</h4>
								<p class="display-7 mbr-content-text mbr-fonts-style">Les
									synergies entretenues avec nos partenaires,&nbsp; font de nous
									un leader européen de la formation professionnelle.&nbsp;</p>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="testimonials2 cid-sZediZlkJb"
		id="testimonials2-4p">


		<div class="container">
			<h3
				class="mbr-section-title mbr-fonts-style align-center mb-4 display-2">
				<strong>Témoignages</strong><br>
			</h3>
			<div class="row justify-content-center">
				<div class="card col-12 col-md-6">
					<p class="mbr-text mbr-fonts-style mb-4 display-7">
						<em>Les formateurs et la direction IT-Training Lyon ont été à
							la hauteur sur toutes nos interrogations. Une belle expérience!</em>
					</p>
					<div class="d-flex mb-md-0 mb-4">
						<div class="image-wrapper">
							<img
								src="${pageContext.request.contextPath}/assets/images/team1.jpg"
								alt="">
						</div>
						<div class="text-wrapper">
							<p class="name mbr-fonts-style mb-1 display-4">
								<strong>Martin Dufour</strong>
							</p>
							<p class="position mbr-fonts-style display-4">
								<strong>Client</strong>
							</p>
						</div>
					</div>
				</div>
				<div class="card col-12 col-md-6">
					<p class="mbr-text mbr-fonts-style mb-4 display-7">
						<em>Très bonne formation, ça va me permettre de démarrer plus
							sereinement la transformation devops au sein de mon équipe .</em>
					</p>
					<div class="d-flex mb-md-0 mb-4">
						<div class="image-wrapper">
							<img
								src="${pageContext.request.contextPath}/assets/images/team2.jpg"
								alt="">
						</div>
						<div class="text-wrapper">
							<p class="name mbr-fonts-style mb-1 display-4">
								<strong>Jessica Pichon</strong>
							</p>
							<p class="position mbr-fonts-style display-4">
								<strong>Client</strong>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="clients2 cid-sZedlPNxiW"
		id="clients2-4q">




		<div class="container">
			<div class="row justify-content-center">
				<div class="card col-12 col-md-6 col-lg-4">
					<div class="card-wrapper">
						<div class="img-wrapper">
							<img
								src="${pageContext.request.contextPath}/assets/images/mbr-280x280.png"
								alt="">
						</div>
						<div class="card-box align-center">

							<h5 class="card-title mbr-fonts-style mb-3 display-5">linkedIn</h5>
							<p class="mbr-text mbr-fonts-style mb-4 display-4">IT compte
								capitaliser sur LinkedIn pour attirer les meilleurs talents et
								permettre aux recruteurs de créer une relation de proximité avec
								nos stagiaires sur ce réseau</p>
							<div class="mbr-section-btn mt-3">
								<a class="btn btn-primary-outline display-4" href="#">Lire</a>
							</div>
						</div>
					</div>
				</div>
				<div class="card col-12 col-md-6 col-lg-4">
					<div class="card-wrapper">
						<div class="img-wrapper">
							<img
								src="${pageContext.request.contextPath}/assets/images/mbr-280x140.png"
								alt="">
						</div>
						<div class="card-box align-center">

							<h5 class="card-title mbr-fonts-style mb-3 display-5">
								<strong>Partenaires</strong>
							</h5>
							<p class="mbr-text mbr-fonts-style mb-4 display-4">Avec
								IT-Training, membre du programme Google Partners, vous
								bénéficierez de formations,&nbsp; qui permettront à votre
								entreprise à se développer et se démarquer de la concurrence</p>
							<div class="mbr-section-btn mt-3">
								<a class="btn btn-primary-outline display-4" href="#"><p>
										En savoir plus&nbsp;<br>
									</p></a>
							</div>
						</div>
					</div>
				</div>
				<div class="card col-12 col-md-6 col-lg-4">
					<div class="card-wrapper">
						<div class="img-wrapper">
							<img
								src="${pageContext.request.contextPath}/assets/images/mbr-2-280x280.png"
								alt="">
						</div>
						<div class="card-box align-center">

							<h5 class="card-title mbr-fonts-style mb-3 display-5">Apple</h5>
							<p class="mbr-text mbr-fonts-style mb-4 display-4">
								Apple et IT s'associent pour développer des solutions de
								sécurité, de réseau et de collaboration qui offrent aux
								utilisateurs Apple une expérience sécurisée<br> <br>
							</p>
							<div class="mbr-section-btn mt-3">
								<a class="btn btn-primary-outline display-4" href="#">Lire</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="footer3 cid-sZ2F0g5zo6"
		once="footers" id="footer3-4o">





		<%@ include file="footer.jspf"%>
		<script
			src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
			crossorigin="anonymous"></script>


		<script
			src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/smoothscroll/smooth-scroll.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/ytplayer/index.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/dropdown/js/navbar-dropdown.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/viewportchecker/viewportchecker.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/theme/js/script.js"></script>
</body>
</html>