<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Liste Formations</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/fontawesome.min.css">


<link href="ajouterFormation.css" rel="stylesheet">

<link href="general.css" rel="stylesheet">

<title>Lister toutes les formations</title>
</head>
<body>
	<div>
		<%@ include file="header.jspf"%>
	</div>

	<div class="p-3"></div>

	<!--%@ include file="header.jspf" %-->
	<div class="container mt-5">
		<div class="mt-5">
			<h2>Résultat de Recherche</h2>
		</div>
		<div class="mt-1">
			<p>mot clé : ${param.q}</p>
		</div>
		<div class="mt-4 mb-5">

			<%@ include file="formations.jspf"%>


		</div>
	</div>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- Leave those next 4 lines if you care about users using IE8 -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
	<%@ include file="footer.jspf"%>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>
</body>
</html>