<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ajouter Module</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">

<link href="general.css" rel="stylesheet">


<title>Ajouter une Module</title>
</head>
<body>




	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1">
			<%@ include file="header.jspf"%>
			<div class="container main-content p-4">
				<div class="m-5">
					<%@ include file="messages.jspf"%>
				</div>
				<div class="m-5">
					<h1>Nouveau Module</h1>
				</div>

				<div>

					<div class="container">
						<div class="m-5 col-md-6">
							<form action="ajouterModule" method="post">


								<c:if test="${ success }" var="variable">
									<div class="card green darken-1">Le module a été créé
										avec succès</div>
								</c:if>
								<div class="mb-3">
									<label for="titre">Titre </label>
									<!-- 						** -->
									<input name="titre" class="form-control" id="titre"
										placeholder="Saisir le titre de la formation">
								</div>

								<div>
									<div class="mb-3">
										<label for="theme">Thème </label> <select id="theme"
											name="theme" required class="form-select">
											<option value="" disabled selected>Choisie le thème</option>
											<c:forEach items="${themes}" var="theme">
												<option name="choixTheme" value="${theme.id}">${theme.nom}</option>
											</c:forEach>

										</select>
									</div>

									<div class="mb-3">
										<label for="duree">Durée (en jours)</label> <input id="duree"
											class="form-control" name="duree" required min="1"
											type="number" class="validate">

									</div>

							

									<div class="mb-3">
										<label for="description">Description</label>
										<textarea rows="6" cols="80" class="form-control"
											id="description" name="description"></textarea>
									</div>
								</div>

								<div class="card-action">
									<button type="submit" class="btn btn-primary btn-lg btn-block"">Ajouter</button>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
</body>

</html>