<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png"
	type="image/x-icon">
<meta name="description" content="">


<title>Formations intra-entreprises</title>



</head>
<body>

	<%@ include file="header.jspf"%>

	<section data-bs-version="5.1"
		class="header01 digitalm4_header01 container cid-sYXjeUT07w mbr-parallax-background"
		id="header01-2v">



		<div class="mbr-overlay"
			style="opacity: 0.4; background-color: rgb(60, 113, 211);"></div>
		<svg xmlns="http://www.w3.org/2000/svg"
			xmlns:xlink="http://www.w3.org/1999/xlink" width="1380px"
			height="760px" viewBox="0 0 1380 760"
			preserveAspectRatio="xMidYMid meet">
        <defs id="svgEditorDefs">
            <polygon id="svgEditorShapeDefs"
				style="fill:khaki;stroke:black;vector-effect:non-scaling-stroke;stroke-width:0px;"></polygon>
        </defs>
        <rect id="svgEditorBackground" x="0" y="0" width="1380"
				height="760" style="fill: none; stroke: none;"></rect>
        <path
				d="M0.3577131120350206,0.819491525482845h-1.5000000000000355ZM0.3577131120350206,-3.1805084745172603h-1.5000000000000355ZM-0.14228688796500222,-4.180508474517258h5.000000000000002a5,5,0,0,1,0,6.00000000000003h-5.000000000000025a5,5,0,0,0,0,-6.00000000000003ZM5.8577131120349835,-1.1805084745172634h1.0000000000000249Z"
				style="fill:khaki; stroke:black; vector-effect:non-scaling-stroke;stroke-width:0px;"
				id="e2_shape"
				transform="matrix(1.01506 82.3743 -245.478 0.34062 392.311 526.125)"></path>
    </svg>
		<div class="container align-center">
			<div class="row justify-content-center">
				<div class="mbr-white col-md-12 col-lg-9">
					<h1
						class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
						Formations intra &amp; <br>sur mesure
					</h1>
					<p class="mbr-text pb-3 mbr-semibold mbr-fonts-style display-5">Optez
						pour des approches multimodales de la formation, et propose � vos
						�quipes des dispositifs plus agiles</p>
					<div class="mbr-section-btn">
						<a class="btn btn-lg btn-white display-4" href="contact">CONTACTEZ NOUS</a>
					</div>
				</div>
				<div class="pt-5 align-center">
					<img src="${pageContext.request.contextPath}/assets/images/11.png"
						alt="" style="width: 35%;">
				</div>
			</div>
		</div>
	</section>

	<section data-bs-version="5.1" class="content15 cid-sYWdEc2YXj"
		id="content15-2q">




		<div class="container">
			<div class="row justify-content-center">
				<div class="card col-md-12 col-lg-12">
					<div class="card-wrapper">
						<div class="card-box align-left">
							<h4 class="card-title mbr-fonts-style mbr-white mb-3 display-5">LARGE
								CATALOGUE&nbsp; DE FORMATIONS</h4>
							<p class="mbr-text mbr-fonts-style display-7">Avec 3 000
								sessions intra et sur-mesure r�alis�es chaque ann�e, IT-Trainig
								dispose d'un savoir-faire unique dans la conception,
								l'organisation et la r�alisation de formations intra-entreprise,
								que celles-ci reposent sur des contenus standards, l�g�rement
								adapt�s ou enti�rement sur-mesure. De la prestation reposant un
								programme catalogue � la mise en place de dispositifs riches
								associant diff�rentes modalit�s d'apprentissage, nous vous
								proposons un large �ventail de solutions.</p>
							<div class="mbr-section-btn mt-3">
								<a class="btn btn-primary display-4" href="formationsInter">DECOUVRIR</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<%@ include file="footer.jspf" %>

	



</body>
</html>