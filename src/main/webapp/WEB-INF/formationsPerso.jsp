<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png"
	type="image/x-icon">
<meta name="description" content="">



<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png" type="image/x-icon">
  <meta name="description" content="">
  
  
  <title>Formations personnalisées</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/web/assets/mobirise-icons2/mobirise2.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/dropdown/css/style.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/socicon/css/styles.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/theme/css/style.css">
  <link rel="preload" href="https://fonts.googleapis.com/css?family=Jost:100,200,300,400,500,600,700,800,900,100i,200i,300i,400i,500i,600i,700i,800i,900i&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Jost:100,200,300,400,500,600,700,800,900,100i,200i,300i,400i,500i,600i,700i,800i,900i&display=swap"></noscript>
  <link rel="preload" href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600,700,800,900,300i,400i,500i,600i,700i,800i,900i&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600,700,800,900,300i,400i,500i,600i,700i,800i,900i&display=swap"></noscript>
  <link rel="preload" href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&display=swap"></noscript>
  <link rel="preload" as="style" href="${pageContext.request.contextPath}/assets/mobirise/css/mbr-additional.css"><link rel="stylesheet" href="${pageContext.request.contextPath}/assets/mobirise/css/mbr-additional.css" type="text/css">
  
 

</head>
<body>

<!--   <section data-bs-version="5.1" class="menu menu2 cid-sYKMtdErxO" once="menu" id="menu2-54" data-sortbtn="btn-primary"> -->
    
<!--     <nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg"> -->
<!--         <div class="container-fluid"> -->
<!--             <div class="navbar-brand"> -->
<!--                 <span class="navbar-logo"> -->
<!--                     <a href="#"> -->
<!--                         <img src="assets/images/mbr-2-89x95.png" alt="" style="height: 3rem;"> -->
<!--                     </a> -->
<!--                 </span> -->
<!--                 <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-7" href="#">IT-Training</a></span> -->
<!--             </div> -->
<!--             <button class="navbar-toggler" type="button" data-toggle="collapse" data-bs-toggle="collapse" data-target="#navbarSupportedContent" data-bs-target="#navbarSupportedContent" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"> -->
<!--                 <div class="hamburger"> -->
<!--                     <span></span> -->
<!--                     <span></span> -->
<!--                     <span></span> -->
<!--                     <span></span> -->
<!--                 </div> -->
<!--             </button> -->
<!--             <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
<!--                 <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item"><a class="nav-link link text-black display-4" href="#">Home</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#" aria-expanded="false">Inscription</a></li><li class="nav-item dropdown"><a class="nav-link link text-black dropdown-toggle display-4" href="#" data-toggle="dropdown-submenu" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="false">Formations</a><div class="dropdown-menu" aria-labelledby="dropdown-239"><a class="text-black dropdown-item display-4" href="#">Formations Inter</a><a class="text-black show dropdown-item display-4" href="#">Formations Intra</a><a class="text-black show dropdown-item display-4" href="#">Formations Personnalisées</a></div> <div class="col-lg-3 col-md-12 input-group-btn selected"></div> -->
<!-- 					</li><li class="nav-item"><a class="nav-link link text-black display-4" href="#">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; A propos&nbsp; &nbsp;</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#"><span class="mobi-mbri mobi-mbri-letter mbr-iconfont mbr-iconfont-btn"></span>Contact</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#"><span class="mobi-mbri mobi-mbri-cart-add mbr-iconfont mbr-iconfont-btn"></span>Panier<br></a> -->
<!--                     </li></ul> -->
                
<!--                 <div class="navbar-buttons mbr-section-btn"><a class="btn btn-primary display-4" href="#"><span class="mobi-mbri mobi-mbri-user-2 mbr-iconfont mbr-iconfont-btn"></span>Connexion<br></a></div> -->
<!--             </div> -->
<!--         </div> -->
<!--     </nav> -->
<!-- </section> -->

<%@ include file="header.jspf"%>

<section data-bs-version="5.1" class="header6 modernm4_header6 carousel slide testimonials-slider cid-sZimtlrECK" data-interval="false" data-bs-interval="false" id="header6-57" data-sortbtn="btn-primary">
    
    

    <div class="">
        
        <div class="carousel slide" role="listbox" data-pause="true" data-keyboard="false" data-ride="false" data-bs-ride="false" data-interval="false" data-bs-interval="false">
            <div class="carousel-inner">
                
                
            <div class="carousel-item">
                    <div class="user">
                        <div class="mode"></div>
                        <div class="title__block align-left">
                            <h3 class="mbr-section-subtitle mbr-regular mbr-fonts-style display-7">LES ACTIVITES DIGITALES</h3>
            <h1 class="mbr-section-title mbr-fonts-style mbr-bold display-2">Formez vous<br>où que vous soyez .</h1>
                            <div class="arrow__btn">
                            <a href="index.html"><span class="mbr-iconfont mobi-mbri-right mobi-mbri"></span></a>
                        </div>
                    </div>
                            
                        <div class="user_image">
                            <img src="${pageContext.request.contextPath}/assets/images/mbr-1920x1276.jpg" alt="" title="">
                        </div>
                    </div>
                </div><div class="carousel-item">
                    <div class="user">
                        <div class="mode"></div>
                        <div class="title__block align-left">
                            <h3 class="mbr-section-subtitle mbr-regular mbr-fonts-style display-7">FORMATIONS MIXTES</h3>
            <h1 class="mbr-section-title mbr-fonts-style mbr-bold display-2">Un compromis <br>avec votre quotidien</h1>
                            <div class="arrow__btn">
                            <a href="index.html"><span class="mbr-iconfont mobi-mbri-right mobi-mbri"></span></a>
                        </div>
                    </div>
                            
                        <div class="user_image">
                            <img src="${pageContext.request.contextPath}/assets/images/mbr-1-1920x1271.jpg" alt="" title="">
                        </div>
                    </div>
                </div><div class="carousel-item">
                    <div class="user">
                        <div class="mode"></div>
                        <div class="title__block align-left">
                            <h3 class="mbr-section-subtitle mbr-regular mbr-fonts-style display-7">LEARNING HUB</h3>
            <h1 class="mbr-section-title mbr-fonts-style mbr-bold display-2">Optimisez votre<br>apprentissage</h1>
                            <div class="arrow__btn">
                            <a href="index.html"><span class="mbr-iconfont mobi-mbri-right mobi-mbri"></span></a>
                        </div>
                    </div>
                            
                        <div class="user_image">
                            <img src="${pageContext.request.contextPath}/assets/images/mbr-5-1920x1280.jpg" alt="" title="">
                        </div>
                    </div>
                </div></div>
             <div class="box">
                <div class="box__content">
                    <div class="box__item">
                    
                    <p class="box__text mbr-fonts-style display-4">01<br><strong>Les activités digitales<br></strong>Quiz, vidéos, e-learning</p>
                    </div>
                    <div class="box__item">
                    <p class="box__text mbr-fonts-style display-4">02<br><strong>Les formations mixtes<br></strong>3000 formations disponibles</p>
                    </div>
                    <div class="box__item">
                    <p class="box__text mbr-fonts-style display-4">03<br><strong>Learning Hub<br></strong>Dernière génération</p>
                    </div>
                    
                    
                </div>
            </div>
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-bs-target="#carouselExampleIndicators" data-slide-to="0" data-bs-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-bs-target="#carouselExampleIndicators" data-slide-to="1" data-bs-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-bs-target="#carouselExampleIndicators" data-slide-to="2" data-bs-slide-to="2"></li>
                
                                 
              </ol>
             <div class="carousel-controls">
                <a class="carousel-control-prev" role="button" data-slide="prev" data-bs-slide="prev">
                  <span aria-hidden="true" class="mobi-mbri-left mobi-mbri"></span>
                  <span class="sr-only visually-hidden">Previous</span>
                </a>
                
                <a class="carousel-control-next" role="button" data-slide="next" data-bs-slide="next">
                  <span aria-hidden="true" class="mobi-mbri-right mobi-mbri"></span>
                  <span class="sr-only visually-hidden">Next</span>
                </a>
            </div>

        </div>
    </div>
</section>

<section data-bs-version="5.1" class="features10 cid-sZj4jHImvY" id="features11-58" data-sortbtn="btn-primary">
    <!---->
    
    
    <div class="container">
        <div class="title">
            <h3 class="mbr-section-title mbr-fonts-style mb-4 display-2"><strong>Nos solutions s'adaptent à votre disponibilité</strong></h3>
            
        </div>
        <div class="card">
            <div class="card-wrapper">
                <div class="row align-items-center">
                    <div class="col-12 col-md-3">
                        <div class="image-wrapper">
                            <img src="${pageContext.request.contextPath}/assets/images/mbr-1-596x397.jpg" alt="" title="">
                        </div>
                    </div>
                    <div class="col-12 col-md">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-12">
                                    <div class="top-line">
                                        <h4 class="card-title mbr-fonts-style display-5"><strong>Les activités digitales</strong></h4>
                                        <p class="cost mbr-fonts-style display-5"></p>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="bottom-line">
                                        <p class="mbr-text mbr-fonts-style m-0 display-7">
                                            Découvrez les différents types d'activités digitales conçues par nos experts (quiz, vidéos, e-learning), ainsi que leur apport dans le cycle d'apprentissage.&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-wrapper">
                <div class="row align-items-center">
                    <div class="col-12 col-md-3">
                        <div class="image-wrapper">
                            <img src="${pageContext.request.contextPath}/assets/images/mbr-596x398.jpg" alt="" title="">
                        </div>
                    </div>
                    <div class="col-12 col-md">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-12">
                                    <div class="top-line">
                                        <h4 class="card-title mbr-fonts-style display-5"><strong>Les formations mixtes</strong></h4>
                                        <p class="cost mbr-fonts-style display-5"></p>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="bottom-line">
                                        <p class="mbr-text mbr-fonts-style m-0 display-7">
                                            Nous vous proposons près de 3000 formations dans un format mixte. Retrouvez sur cette page l'ensemble de nos références réparties par gamme.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-wrapper">
                <div class="row align-items-center">
                    <div class="col-12 col-md-3">
                        <div class="image-wrapper">
                            <img src="${pageContext.request.contextPath}/assets/images/mbr-596x369.jpg" alt="" title="">
                        </div>
                    </div>
                    <div class="col-12 col-md">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-12">
                                    <div class="top-line">
                                        <h4 class="card-title mbr-fonts-style display-5"><strong>Learning Hub IT-Training</strong></h4>
                                        <p class="cost mbr-fonts-style display-5"></p>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="bottom-line">
                                        <p class="mbr-text mbr-fonts-style m-0 display-7">
                                            Conçu pour optimiser l’expérience d’apprentissage, le Learning Hub est la plate-forme unique utilisée par IT pour la mise à disposition de tous ses contenus digitaux.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
    </div>
</section>



<%@ include file="footer.jspf"%>


</body>
</html>