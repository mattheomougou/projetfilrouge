<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Information employ�</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>
	<%@ include file="header.jspf"%>

	<div class="container">
		<div class="col-sm-12 text-center ">
			<div class="pt-5 pb-5 bg-light">
				<div class="mb-5 mt-5">
					<div class="card-body">
						<h5 class="card-title">Vos informations</h5>
						<h5 class="card-title">${utilisateur.nom.toUpperCase()}
							${utilisateur.prenom}</h5>
						<p class="card-text">Email : ${utilisateur.mail}</p>
						<p class="card-text">Tel : ${utilisateur.telephone}</p>
						<p class="card-text">Adresse: ${utilisateur.adresse}
							${utilisateur.codePostal} ${utilisateur.ville}</p>
						<p class="card-text">Date d'inscription :
							${utilisateur.dateInscription}</p>

						<a class="btn btn-primary"
							href="${pageContext.request.contextPath}/accueil">Retour</a> <a
							class="btn btn-primary"
							href="${pageContext.request.contextPath}/modifierStagiaire">Modifier</a>
					</div>
				</div>
			</div>

		</div>


	</div>




	<%@ include file="footer.jspf"%>
</body>
</html>
