<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/assets/images/mbr-2-89x95.png"
	type="image/x-icon">
<meta name="description" content="">


<title>Formations intra-entreprises</title>

<link href="general.css" rel="stylesheet">

</head>
<body>

	<%@ include file="header.jspf"%>


	<section data-bs-version="5.1"
		class="header3 travelm4_header3 cid-sZ1LScXz03 mbr-parallax-background"
		id="header3-3p">



		<div class="mbr-overlay"
			style="opacity: 0.7; background-color: rgb(68, 121, 217);"></div>

		<div class="container">
			<h1
				class="mbr-section-title mbr-bold align-left mbr-white pb-1 mbr-fonts-style display-1">
				Formez votre succ�s.<br>
			</h1>

			<h3
				class="mbr-section-sub-title pb-4 mbr-normal mbr-fonts-style align-left display-4">Trouver
				la formation informatique qui r�pond � vos besoins</h3>
			<div class="row justify-content-center align-center">
				<div
					class="media-container-column col-lg-12 col-md-10 col-sm-12 align-center">
					<!---Formbuilder Form--->
					<form action="chercherFormations" method="GET"
						class="mbr-form form-with-styler">
						<div class="row">

							<div hidden="hidden" data-form-alert-danger=""
								class="alert alert-danger col-12"></div>
						</div>
						<div class="dragArea row">
							<div class="col-lg-3 col-md-12 relative form-group mb-3"
								data-for="message">
								<input type="text" name="q" data-form-field="Message"
									class="form-control input display-7"
									placeholder="Chercher une formation" id="message-header3-3p">
								<div class="ico">
									<span class="mbr-iconfont mbri-search"></span>
								</div>
							</div>


							<div class="col-lg-3 col-md-12 input-group-btn selected">
								<button type="submit"
									class="btn btn-md btn-form btn-primary display-4">Recherche</button>
							</div>
						</div>
					</form>
					<!---Formbuilder Form--->
				</div>
			</div>
		</div>
	</section>


	<div class="container">
		<div class="mt-5">
			<h1>Liste de toutes les formations</h1>
		</div>
		<div class="mb-5">

			<%@ include file="formations.jspf"%>



		</div>
	</div>

	<section data-bs-version="5.1"
		class="header1 cid-sYXGhh5QqX mbr-fullscreen" id="header1-2z">





		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-lg-6">
					<h1 class="mbr-section-title mbr-fonts-style mb-3 display-1">
						<strong> Besoin d'une Formation sur Mesure ?</strong>
					</h1>

					<p class="mbr-text mbr-fonts-style display-7">Proposez � vos
						�quipes des dispositifs de formations multimodales sur mesure</p>
					<div class="mbr-section-btn mt-3">
						<a class="btn btn-secondary display-4" href="formationsIntra">D�couvrir</a>
					</div>
				</div>
			</div>
		</div>
	</section>




	<%@ include file="footer.jspf"%>





</body>
</html>