<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Administration</title>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1">
<link rel="shortcut icon" href="assets/images/mbr-2-89x95.png"
	type="image/x-icon">
<meta name="description" content="">


<title>Admin</title>



<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">



</head>
<body>


	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1 vh-100">
			<%@ include file="header.jspf"%>


			<section data-bs-version="5.1"
				class="counters1 main-content agencym4_counters1 counters cid-sZvhvUXYDj"
				id="counters1-5s" data-sortbtn="btn-primary">





				<div class="container mbr-white">



					<div class="container pt-4 mt-2">
						<div class="media-container-row">
							<div class="card p-3 align-center col-12 col-md-6 col-lg-3">
								<div class="panel-item p-3">
									<div class="card-img pb-3">
										<span class="mbr-iconfont mobi-mbri-user mobi-mbri"></span>
									</div>

									<div class="card-text">
										<h3
											class="count pt-3 pb-3 mbr-semibold mbr-fonts-style text-white display-1">
											${nombreEmploye}</h3>
										<h4
											class="mbr-content-title mbr-semibold pb-3 mbr-fonts-style display-5">Employ�s</h4>

									</div>
								</div>
							</div>

							<div class="card p-3 align-center col-12 col-md-6 col-lg-3">
								<div class="panel-item p-3">
									<div class="card-img pb-3">
										<span class="mbr-iconfont mobi-mbri-users mobi-mbri"></span>
									</div>
									<div class="card-text">
										<h3
											class="count pt-3 pb-3 mbr-semibold mbr-fonts-style text-white display-1">
											${nombreStagiaire}</h3>
										<h4
											class="mbr-content-title mbr-semibold pb-3 mbr-fonts-style display-5">
											Stagiaires</h4>

									</div>
								</div>
							</div>

							<div class="card p-3 align-center col-12 col-md-6 col-lg-3">
								<div class="panel-item p-3">
									<div class="card-img pb-3">
										<span class="mbr-iconfont mobi-mbri-user mobi-mbri"></span>
									</div>
									<div class="card-text">
										<h3
											class="count pt-3 pb-3 mbr-semibold mbr-fonts-style text-white display-1">
											${nombreFormateur}</h3>
										<h4
											class="mbr-content-title mbr-semibold pb-3 mbr-fonts-style display-5">Formateurs</h4>

									</div>
								</div>
							</div>

							<div class="card p-3 align-center col-12 col-md-6 col-lg-3">
								<div class="panel-item p-3">
									<div class="card-img pb-3">
										<span class="mbr-iconfont mbri-bookmark"></span>
									</div>

									<div class="card-texts">
										<h3
											class="count pt-3 pb-3 mbr-semibold mbr-fonts-style text-white display-1">
											${nombreFormation}</h3>
										<h4
											class="mbr-content-title mbr-semibold pb-3 mbr-fonts-style display-5">
											Formations</h4>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


			<script
				src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
			<script
				src="${pageContext.request.contextPath}/assets/smoothscroll/smooth-scroll.js"></script>
			<script
				src="${pageContext.request.contextPath}/assets/ytplayer/index.js"></script>
			<script
				src="${pageContext.request.contextPath}/assets/dropdown/js/navbar-dropdown.js"></script>
			<script
				src="${pageContext.request.contextPath}/assets/viewportchecker/viewportchecker.js"></script>
			<script
				src="${pageContext.request.contextPath}/assets/theme/js/script.js"></script>
</body>
</html>