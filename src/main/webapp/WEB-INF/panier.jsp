<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
	<link href="general.css" rel="stylesheet">
<meta charset="UTF-8">
<title>Panier</title>
</head>
<body>
	<%@ include file="header.jspf"%>

	<c:set var="nbInscriptions"
		value="${sessionScope.panier != null ? sessionScope.panier.sessions.size() : 0}" />
	<div class="container p-4 mt-4">

		<div class="row">
			<div class="col-md-8 col-sm-12">


				<div class="mt-5 mb-5">
					<h1>Votre panier</h1>
					<h5>
						<c:if test="${ nbInscriptions > 0}">
					 ${nbInscriptions} Inscription(s)
					 </c:if>
					</h5>
				</div>

			</div>

		</div>





		<div class="row">
			<c:if test="${ nbInscriptions > 0}">
				<div class="col-md-8 col-sm-12">


					<c:forEach items="${sessionScope.panier.sessions}" var="session">
						<div class="mb-2">

							<div style="border-width: 1px !important; border-style: solid;"
								class="card formation-card border-light p-2">
								<div class="card-body">
									<div class="d-flex p-1">
										<div class="flex-grow-1">

											<div class="d-flex align-items-center mb-1 mt-1">
												<div class="flex-grow-1 d-flex align-items-center">
													<a href="pageFormation?id=${session.formation.id}"
														class="text-body">
														<h5 class="card-title text-body">
															<span
																class="fs-5  mobi-mbri mobi-mbri-bookmark mbr-iconfont"></span>
															<span class="ms-1">${session.formation.titre}</span>
														</h5>
													</a>
												</div>

											</div>
											<div class="d-flex align-items-center mb-1 mt-1">
												<div class="flex-grow-1 d-flex align-items-center">
													<h6 class="card-title text-primary">
														<span
															class="fs-5  mobi-mbri mobi-mbri-calendar mbr-iconfont"></span>
														<span class="ms-1">Session : </span> <span
															class="text-primary">${session.dateDebut}</span> - <span
															class="text-primary">${session.dateFin}</span>
													</h6>
												</div>

											</div>

										</div>

										<div class="ms-2 d-flex flex-column">

											<div class="pt-2">
												<h5 class="fw-bold text-primary">${session.formation.prix}€</h5>
											</div>
											<div class="d-flex">
												<form method="post" class="flex-grow-1 d-flex"
													action="panier">
													<input type="hidden" value="SUPPRIMER" name="action" /> <input
														type="hidden" name="sessionId" value="${session.id}" />
													<button
														class="btn m-0 d-flex justify-center btn-light flex-grow-1">
														<span
															class="fs-6 m-0  mobi-mbri mobi-mbri-trash mbr-iconfont"></span>
													</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>


						</div>
					</c:forEach>

				</div>


				<div class="col-md-4 col-sm-12">

					<div class="p-4 bg-dark text-light d-flex flex-column">
						<div>
							<h3>SUMMARY</h3>
						</div>

						<div class="mt-4 fs-6">
							<span class="fs-6"> Sous-Total : </span> <span class="fw-bold">
								${ sessionScope.panier.total} € </span>
						</div>

						<div class="mt-5 fs-4">
							<span> Total : </span> <span class="fw-bold"> ${ sessionScope.panier.total}
								€ </span>
						</div>
					</div>


					<div class="mt-3  d-flex">
						<form method="post" class="flex-grow-1 d-flex" action="panier">
							<input type="hidden" value="VALIDER" name="action" />
							<button class="btn btn-primary btn-lg m-0 flex-grow-1"
								type="submit">Payer</button>
						</form>
					</div>

				</div>
			</c:if>
			<c:if test="${ nbInscriptions <= 0}">
				<div class="col-sm-12 text-center ">
					<div class="pt-5 pb-5 bg-light">
						<div class="mb-5 mt-5">
							<p class="fs-5">Vous n'avez ajouté aucune formation à votre
								panier</p>
						</div>
					</div>

				</div>
			</c:if>
		</div>



	</div>






	<%@ include file="footer.jspf"%>
</body>
</html>