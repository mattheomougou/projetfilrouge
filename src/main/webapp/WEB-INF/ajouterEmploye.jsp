<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ajout d'un employ�</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link href="general.css" rel="stylesheet">
</head>
<body>
	<div class="d-flex">
		<%@ include file="adminsidebar.jspf"%>
		<div class="flex-grow-1 vh-100">
			<%@ include file="header.jspf"%>
			<div class="container main-content p-4">

				<div class="m-5">
					<h2>Ajouter un employ�</h2>

					<form action="ajouterEmploye" method="post" class="mt-5">
						<div class="form-group col-md-6">
							<label for="nom">Nom :</label> <input class="form-control"
								placeholder="Saisir son nom" type="text" id="nom" name="nom"
								value="${empty erreursUtilisateur.get('nom') ? param.nom : ''}"
								required>${erreursUtilisateur.get('nom')}<br />
						</div>
						<div class="form-group col-md-6">
							<label for="prenom">Pr�nom :</label> <input class="form-control"
								placeholder="Saisir son pr�nom" type="text" id="prenom"
								name="prenom"
								value="${empty erreursUtilisateur.get('prenom') ? param.prenom : ''}"
								required>${erreursUtilisateur.get('prenom')}<br />
						</div>
						<div class="form-group col-md-6">
							<label for="mail">Mail :</label> <input class="form-control"
								placeholder="Saisir son mail" type="email" id="mail" name="mail"
								value="${empty erreursUtilisateur.get('mail') ? param.mail : ''}"
								required>${erreursUtilisateur.get('mail')}<br />
						</div>

						<div class="form-group col-md-6">
							<label for="motDePasse">Mot de passe :</label> <input
								class="form-control" type="password" id="motDePasse"
								name="motDePasse" placeholder="Saisir votre mot de passe"
								required>
							<div class="text-danger">${erreursUtilisateur.get('motDePasse')}</div>
							<br />
						</div>

						<div class="form-group col-md-6">
							<label for="confirmMdp">Confirmer le mot de passe :</label> <input
								class="form-control" type="password"
								placeholder="Confirmer votre mot de passe"
								id="confirmMotDePasse" name="confirmMotDePasse" required>
							<div class="text-danger">${erreursUtilisateur.get('confirmMotDePasse')}</div>
							<br />
						</div>

						<div class="form-group col-md-6">
							<label for="fonction">Fonction :</label> <input
								class="form-control" placeholder="Saisir sa fonction"
								type="text" id="fonction" name="fonction"
								value="${param.fonction}" required><br />
						</div>
						<div class="form-group col-md-6">
							<label for="salaire">Salaire :</label> <input
								class="form-control" placeholder="Saisir son salaire"
								type="number" id="salaire" name="salaire" step="0.01"
								value="${empty erreursEmploye.get('salaire') ? param.salaire : ''}"
								required>${erreursEmploye.get('salaire')}<br />
						</div>
						<div class="form-group col-md-6">
							<label for="telephone">Telephone :</label> <input
								class="form-control" placeholder="Saisir son t�l�phone"
								type="tel" id="telephone" name="telephone"
								value="${empty erreursUtilisateur.get('telephone') ? param.telephone : ''}"
								required>${erreursUtilisateur.get('telephone')}<br />
						</div>
						<div class="form-group col-md-6">
							<label for="adresse">Adresse :</label> <input
								class="form-control" placeholder="Saisir son adresse"
								type="text" id="adresse" name="adresse"
								value="${ param.adresse}"><br />
						</div>
						<div class="form-group col-md-6">
							<label for="codePostal">Code Postal :</label> <input
								class="form-control" placeholder="Saisir son code postal"
								type="text" id="codePostal" name="codePostal"
								value="${ param.codePostal}"><br />
						</div>
						<div class="form-group col-md-6">
							<label for="ville">Ville :</label> <input class="form-control"
								placeholder="Saisir sa ville" type="text" id="ville"
								name="ville" value="${ param.ville}"><br />
						</div>
						<div class="form-group col-md-6">
							<label for="dateEmbauche"> Date d'embauche </label> <input
								class="form-control" type="date" id="dateEmbauche"
								name="dateEmbauche" value="${ param.dateEmbauche}"><br />
						</div>
						<div class="form-group col-md-6">
							<input type="checkbox" id="responsablePlanning"
								name="responsablePlanning" value="true"> <label
								for="responsablePlanning">L'employ� a le r�le de
								responsable planning? </label> <br />
						</div>
						<br /> <input class="btn btn-primary" type="submit"
							value="Ajouter l'employ�" />
					</form>
				</div>




			</div>
		</div>
	</div>

</body>
</html>